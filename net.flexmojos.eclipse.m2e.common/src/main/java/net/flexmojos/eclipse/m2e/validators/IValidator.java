package net.flexmojos.eclipse.m2e.validators;

import org.eclipse.core.runtime.IStatus;

public interface IValidator
    extends org.eclipse.core.databinding.validation.IValidator
{
    public IStatus validate();
}
