package net.flexmojos.eclipse.m2e;

import net.flexmojos.eclipse.m2e.encryption.IClassLoaderProvider;
import net.flexmojos.eclipse.m2e.encryption.internal.NitroClassLoaderException;
import net.flexmojos.eclipse.m2e.encryption.internal.NitroClassLoaderManager;
import net.flexmojos.eclipse.m2e.internal.markers.IMarkerManager;
import net.flexmojos.eclipse.m2e.internal.markers.MarkerManager;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlexMojosPlugin
    extends Plugin
    implements IFlexMojosPlugin, IClassLoaderProvider
{
    private static final Logger log = LoggerFactory.getLogger( FlexMojosPlugin.class );

    private static final String FB_BUNDLE = "com.adobe.flexbuilder.project";

    private static final String FDT_BUNDLE = "com.powerflasher.fdt.core";

    public static final String PLUGIN_ID = "net.flexmojos.eclipse.m2e.core";

    // The shared instance
    private static FlexMojosPlugin plugin;

    private IdeType ideType;

    private NitroClassLoaderManager classLoaderManager;
    
    private IMarkerManager markerManager;

    public synchronized IdeType getIdeType()
    {
        if ( ideType == null )
        {
            if ( null != Platform.getBundle( FB_BUNDLE ) )
            {
                ideType = IdeType.FLASHBUILDER;

                if ( log.isDebugEnabled() )
                    log.debug( "Detected Flashbuilder Plugin." );

                return ideType;
            }
            else if ( null != Platform.getBundle( FDT_BUNDLE ) )
            {
                ideType = IdeType.FDT;

                if ( log.isDebugEnabled() )
                    log.debug( "Detected FDT Plugin." );
            }
            else
            {
                if ( log.isErrorEnabled() )
                    log.error( "Unable to detected a supported IDE plugin." );
            }
        }

        return ideType;
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static FlexMojosPlugin getDefault()
    {
        return plugin;
    }
    
    public IMarkerManager getMarkerManager()
    {
        return markerManager;
    }

    public ClassLoader getClassLoader( IFlexMojosPlugin plugin )
    {
        try
        {
            return classLoaderManager.getClassLoader( plugin );
        }
        catch ( NitroClassLoaderException e )
        {
            log.error( "Unable to get NitroLM class loader for plugin " + plugin.getPluginId(), e );
        }

        return null;
    }

    public ClassLoader getClassLoader()
    {
        return this.getClassLoader( this );
    }

    public void start( BundleContext context )
        throws Exception
    {
        super.start( context );
        plugin = this;

        classLoaderManager = new NitroClassLoaderManager();
        markerManager = new MarkerManager();
    }

    public void stop( BundleContext context )
        throws Exception
    {
        super.stop( context );
        plugin = null;
    }

    public String getPluginId()
    {
        return PLUGIN_ID;
    }
}
