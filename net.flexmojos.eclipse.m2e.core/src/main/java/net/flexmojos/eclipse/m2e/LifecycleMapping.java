package net.flexmojos.eclipse.m2e;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.eclipse.m2e.core.project.configurator.AbstractBuildParticipant;
import org.eclipse.m2e.core.project.configurator.AbstractCustomizableLifecycleMapping;
import org.eclipse.m2e.core.project.configurator.MojoExecutionKey;

public class LifecycleMapping
    extends AbstractCustomizableLifecycleMapping
{
    @Override
    public Map<MojoExecutionKey, List<AbstractBuildParticipant>> getBuildParticipants( IMavenProjectFacade projectFacade,
                                                                                       IProgressMonitor monitor )
        throws CoreException
    {
        return super.getBuildParticipants( projectFacade, monitor );
    }
}
