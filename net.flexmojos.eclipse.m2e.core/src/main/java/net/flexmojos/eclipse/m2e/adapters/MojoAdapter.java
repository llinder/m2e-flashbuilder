package net.flexmojos.eclipse.m2e.adapters;

/**
 * Interface for mojo adapters that primarily serves as a bridge between
 * encrypted and non-encrypted classes.
 * 
 * @author Lance Linder
 */
public interface MojoAdapter
{

}
