package net.flexmojos.eclipse.m2e.common;

public interface FMConstants
{
	static final String PLUGIN_GROUPID = "org.sonatype.flexmojos";
	static final String PLUGIN_ARTIFACTID = "flexmojos-maven-plugin";
	
	static final String GOAL_WRAPPER = "wrapper";
	static final String GOAL_COMPILE_SWF = "compile-swf";
	static final String GOAL_COMPILE_SWC = "compile-swc";
	static final String GOAL_SIGN_AIR = "sign-air";
}
