package net.flexmojos.eclipse.m2e.common;

public interface FMEConstants
{
	static final String PLUGIN_GROUPID = "net.flexmojos";
	static final String PLUGIN_ARTIFACTID = "fme";
	
	static final String GOAL_WRAPPER = "ewrapper";
	static final String GOAL_COMPILE_SWF = "ecompile-swf";
	static final String GOAL_COMPILE_SWC = "ecompile-swc";
	static final String GOAL_SIGN_AIR = "esign-air";
	
	static final String HTML_TEMPLATE_CLIENT_SIDE_DETECTION = 		"embed:client-side-detection";
	static final String HTML_TEMPLATE_CLIENT_SIDE_DETECTION_HIST = 	"embed:client-side-detection-with-history";
	static final String HTML_TEMPLATE_EXPRESS_INSTALL = 			"embed:express-installation";
	static final String HTML_TEMPLATE_EXPRESS_INSTALL_HIST = 		"embed:express-installation-with-history";
	static final String HTML_TEMPLATE_NO_PLAYER_DETECTION = 		"embed:no-player-detection";
	static final String HTML_TEMPLATE_NO_PLAYER_DETECTION_HIST = 	"embed:no-player-detection-with-history";
}
