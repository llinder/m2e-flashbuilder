package net.flexmojos.eclipse.m2e.common;

public interface FlexConstants
{
	public static String SWC = "swc";
	public static String SWF = "swf";
	public static String SWZ = "swz";
	public static String POM = "pom";
	
	public static String COMPILER_GROUPID = "com.adobe.flex";
	public static String COMPILER_ARTIFACTID = "compiler";
	
	public static String FRAMEWORK_GROUPID = "com.adobe.flex.framework";
	
	public static String PLAYERGLOBAL_ARTIFACTID = "playerglobal";
	public static String AIRGLOBAL_ARTIFACTID = "airglobal";
}
