package net.flexmojos.eclipse.m2e.configurator;

import java.lang.reflect.Constructor;
import java.util.List;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.internal.markers.IMarkerManager;
import net.flexmojos.eclipse.m2e.licensing.ILicensing;
import net.flexmojos.eclipse.m2e.licensing.LicensingPlugin;
import net.flexmojos.eclipse.m2e.licensing.markers.ILicenseMarkers;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.AbstractBuildParticipant;
import org.eclipse.m2e.core.project.configurator.AbstractProjectConfigurator;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractEncryptedConfigurator
    extends AbstractProjectConfigurator
{
    private static final Logger log = LoggerFactory.getLogger( AbstractEncryptedConfigurator.class );

    public AbstractEncryptedConfigurator()
    {

    }

    /**
     * Expose protected method for access by internal configurators.
     */
    public List<MojoExecution> getMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return super.getMojoExecutions( request, monitor );
    }

    protected InternalConfigurator getInternalConfigurator( final IProject project, final String className )
    {
        IMarkerManager markerManager = FlexMojosPlugin.getDefault().getMarkerManager();

        if ( validateLicense() )
        {
            try
            {
                // Clear license required problem markers.
                markerManager.deleteMarkers( project, ILicenseMarkers.REQUIRED_ID );

                // Start protected
                @SuppressWarnings( "unchecked" )
                Class<InternalConfigurator> clazz =
                    (Class<InternalConfigurator>) FlexMojosPlugin.getDefault().getClassLoader().loadClass( className );
                Constructor<InternalConfigurator> c =
                    (Constructor<InternalConfigurator>) clazz.getConstructor( AbstractEncryptedConfigurator.class );
                // End protected

                return c.newInstance( this );
            }
            catch ( Exception e )
            {
                if ( log.isErrorEnabled() )
                    log.error( "Error creating configurator '" + className + "'", e );
            }
        }
        else
        {
            markerManager.addMarker( project,
                                     ILicenseMarkers.REQUIRED_ID,
                                     "A valid FlexMojos m2e license is required to perform Maven configuration updates on this project type.",
                                     0, IMarker.SEVERITY_ERROR, true );
        }

        return null;
    }

    protected AbstractBuildParticipant getInternalBuildParticipant( final IProject project, final String className,
                                                                    MojoExecution execution )
    {
        IMarkerManager markerManager = FlexMojosPlugin.getDefault().getMarkerManager();

        if ( validateLicense() )
        {
            try
            {
                // Clear license required problem markers.
                markerManager.deleteMarkers( project, ILicenseMarkers.REQUIRED_ID );

                // Start protected
                @SuppressWarnings( "unchecked" )
                Class<AbstractBuildParticipant> clazz =
                    (Class<AbstractBuildParticipant>) FlexMojosPlugin.getDefault().getClassLoader().loadClass( className );
                Constructor<AbstractBuildParticipant> c =
                    (Constructor<AbstractBuildParticipant>) clazz.getConstructor( MojoExecution.class );
                // End protected

                return c.newInstance( execution );
            }
            catch ( Exception e )
            {
                if ( log.isErrorEnabled() )
                    log.error( "Error creating build participant '" + className + "'", e );
            }
        }
        else
        {
            // Add license required problem marker.
            markerManager.addMarker( project,
                                     ILicenseMarkers.REQUIRED_ID,
                                     "A valid FlexMojos m2e license is required to perform Maven configuration updates on this project type.",
                                     0, IMarker.SEVERITY_ERROR, true );
        }

        return null;
    }

    /**
     * Validates the users license. If license validation fails then a dialog will be presented to allow the user to
     * register.
     * 
     * @return
     */
    private boolean validateLicense()
    {
        final ILicensing licensing = LicensingPlugin.getDefault().getLicensing();

        return licensing.validate();
    }
}
