package net.flexmojos.eclipse.m2e.configurator;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.IdeType;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.lifecyclemapping.model.IPluginExecutionMetadata;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.eclipse.m2e.core.project.MavenProjectChangedEvent;
import org.eclipse.m2e.core.project.configurator.AbstractBuildParticipant;
import org.eclipse.m2e.core.project.configurator.ILifecycleMappingConfiguration;
import org.eclipse.m2e.core.project.configurator.MojoExecutionKey;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompileConfigurator
    extends AbstractEncryptedConfigurator
{
    private static final Logger log = LoggerFactory.getLogger( CompileConfigurator.class );

    private static final String FB_CONFIGURATOR =
        "net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators.CompileConfigurator";

    private static final String FDT_CONFIGURATOR =
        "net.flexmojos.eclipse.m2e.fdt.encrypted.configurators.CompileConfigurator";

    private static final String FB_WRAPPER =
        "net.flexmojos.eclipse.m2e.flashbuilder.encrypted.participants.WrapperBuildParticipant";

    @Override
    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {

        InternalConfigurator configurator = null;
        final IdeType ideType = FlexMojosPlugin.getDefault().getIdeType();
        switch ( ideType )
        {
            case FLASHBUILDER:
                configurator = getInternalConfigurator( request.getProject(), FB_CONFIGURATOR );
                break;
            case FDT:
                configurator = getInternalConfigurator( request.getProject(), FDT_CONFIGURATOR );
                break;
            default:
                if ( log.isErrorEnabled() )
                    log.error( "Compatable IDE now found." );
                break;
        }

        // Run configure
        if ( configurator != null )
            configurator.configure( request, monitor );
    }

    public void mavenProjectChanged( MavenProjectChangedEvent event, IProgressMonitor monitor )
        throws CoreException
    {
        // TODO handle project change (NOTE jdt plugin adds a change listener on the build path manager... maybe we want
        // that too?)
    }

    @Override
    public boolean hasConfigurationChanged( IMavenProjectFacade newFacade,
                                            ILifecycleMappingConfiguration oldProjectConfiguration,
                                            MojoExecutionKey key, IProgressMonitor monitor )
    {
        return super.hasConfigurationChanged( newFacade, oldProjectConfiguration, key, monitor );
    }

    @Override
    public AbstractBuildParticipant getBuildParticipant( IMavenProjectFacade projectFacade, MojoExecution execution,
                                                         IPluginExecutionMetadata executionMetadata )
    {
        if ( "wrapper".equals( execution.getGoal() ) )
        {
            final IdeType ideType = FlexMojosPlugin.getDefault().getIdeType();
            switch ( ideType )
            {
                case FLASHBUILDER:
                    return getInternalBuildParticipant( projectFacade.getProject(), FB_WRAPPER, execution );
            }
        }

        return null;
    }
}
