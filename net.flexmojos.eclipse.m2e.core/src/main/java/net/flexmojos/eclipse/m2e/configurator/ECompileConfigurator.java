package net.flexmojos.eclipse.m2e.configurator;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.lifecyclemapping.model.IPluginExecutionMetadata;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.eclipse.m2e.core.project.configurator.AbstractBuildParticipant;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

public class ECompileConfigurator
    extends AbstractEncryptedConfigurator
{
    private static final String CONFIGURATOR =
        "net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators.ECompileConfigurator";

    private static final String WRAPPER =
        "net.flexmojos.eclipse.m2e.flashbuilder.encrypted.participants.WrapperBuildParticipant";

    @Override
    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        // Create configurator instance
        InternalConfigurator configurator = getInternalConfigurator( request.getProject(), CONFIGURATOR );

        // Run configure
        configurator.configure( request, monitor );
    }

    @Override
    public AbstractBuildParticipant getBuildParticipant( IMavenProjectFacade projectFacade, MojoExecution execution,
                                                         IPluginExecutionMetadata executionMetadata )
    {

        if ( "ewrapper".equals( execution.getGoal() ) )
        {
            return getInternalBuildParticipant( projectFacade.getProject(), WRAPPER, execution );
        }

        return null;
    }
}
