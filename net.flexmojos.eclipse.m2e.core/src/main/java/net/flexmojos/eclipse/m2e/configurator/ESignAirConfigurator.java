package net.flexmojos.eclipse.m2e.configurator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

public class ESignAirConfigurator
    extends AbstractEncryptedConfigurator
{

    private static final String INTERNAL = "net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators.ESignAirConfigurator";
    
    @Override
    public void configure( ProjectConfigurationRequest request,
                           IProgressMonitor monitor )
        throws CoreException
    {
        // Create configurator instance
        InternalConfigurator configurator = getInternalConfigurator( request.getProject(), INTERNAL );
        
        // Run configure
        configurator.configure( request, monitor );
    }

}
