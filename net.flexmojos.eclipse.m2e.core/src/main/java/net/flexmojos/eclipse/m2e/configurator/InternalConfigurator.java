package net.flexmojos.eclipse.m2e.configurator;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.adapters.MojoAdapter;
import net.flexmojos.eclipse.m2e.common.FMConstants;
import net.flexmojos.eclipse.m2e.common.FMEConstants;
import net.flexmojos.eclipse.m2e.encrypted.adapters.AbstractMojoAdapter;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.MavenProjectChangedEvent;
import org.eclipse.m2e.core.project.configurator.AbstractProjectConfigurator;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.compiler.extra.IHtmlWrapperConfiguration;

/**
 * Re-expose parts of {@link AbstractProjectConfigurator} so the internal encrypted configurators have access to
 * required public methods.
 * 
 * @author Lance Linder
 */
public abstract class InternalConfigurator
{
    private static final Logger log = LoggerFactory.getLogger( InternalConfigurator.class );
    
    private final Map<String, MojoExecution> executions;

    private final AbstractEncryptedConfigurator fbConfigurator;

    public InternalConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        executions = new HashMap<String, MojoExecution>();

        this.fbConfigurator = fbConfigurator;
    }

    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        List<MojoExecution> mojoExecs = getFlexMojoExecutions( request, monitor );
        for( MojoExecution exec : mojoExecs )
            executions.put( exec.getGoal(), exec );
    }

    public void unconfigure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        fbConfigurator.unconfigure( request, monitor );
    }

    public void mavenProjectChanged( MavenProjectChangedEvent event, IProgressMonitor monitor )
        throws CoreException
    {
        fbConfigurator.mavenProjectChanged( event, monitor );
    }

    protected abstract List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException;

    protected MojoExecution getExecution( String goal )
    {
        return executions.get( goal );
    }

    @SuppressWarnings( "unchecked" )
    protected <T> T getMojoConfiguration( Class<T> clazz, IProgressMonitor monitor, MavenSession session )
    {
        if ( clazz == IHtmlWrapperConfiguration.class )
        {
            // Prefer the wrapper goal configuration. If not available then use the compile swf goal configuration.
            MojoExecution exec = getExecution( isEnterprise() ? FMEConstants.GOAL_WRAPPER : FMConstants.GOAL_WRAPPER );
            if( exec == null )
                exec = getExecution( isEnterprise() ? FMEConstants.GOAL_COMPILE_SWF : FMConstants.GOAL_COMPILE_SWF );
                 
            if( exec != null )
                return (T)getAdapter( "net.flexmojos.eclipse.m2e.encrypted.adapters.WrapperMojoAdapter", exec, monitor, session );
        }

        return null;
    }
    
    /**
     * Nitro LM helper to load encrypted Mojo Adapters.
     * 
     * @param className
     * @param exec
     * @param monitor
     * @param session
     * @return
     */
    protected MojoAdapter getAdapter( String className, MojoExecution exec, IProgressMonitor monitor, MavenSession session )
    {
        try
        {
            // Start protected
            @SuppressWarnings( "unchecked" )
            Class<AbstractMojoAdapter> clazz =
                (Class<AbstractMojoAdapter>) FlexMojosPlugin.getDefault().getClassLoader().loadClass( className );
            Constructor<AbstractMojoAdapter> c =
                (Constructor<AbstractMojoAdapter>) clazz.getConstructor( IProgressMonitor.class, MavenSession.class, MojoExecution.class );
            // End protected
            
            return c.newInstance( monitor, session, exec );
        }
        catch ( Exception e )
        {
            if( log.isErrorEnabled() )
                log.error( "Error creating mojo adapter '" + className + "'", e );
        }

        return null;
    }
    
    protected abstract boolean isEnterprise();
}
