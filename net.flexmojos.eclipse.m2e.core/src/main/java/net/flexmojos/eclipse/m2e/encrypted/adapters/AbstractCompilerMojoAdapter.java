package net.flexmojos.eclipse.m2e.encrypted.adapters;

import static ch.lambdaj.Lambda.filter;
import static ch.lambdaj.Lambda.selectFirst;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.artifactId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.groupId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.scope;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.type;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.CSS;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.RB_SWC;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.SWC;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.CACHING;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.COMPILE;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.EXTERNAL;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.INTERNAL;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.MERGED;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.RSL;
import static org.sonatype.flexmojos.util.PathUtil.files;
import static org.sonatype.flexmojos.util.PathUtil.pathsList;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.embedder.ArtifactKey;
import org.eclipse.m2e.core.embedder.IMaven;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.hamcrest.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.compatibilitykit.FlexCompatibility;
import org.sonatype.flexmojos.compiler.ICompilerConfiguration;
import org.sonatype.flexmojos.compiler.IDefine;
import org.sonatype.flexmojos.compiler.IExtension;
import org.sonatype.flexmojos.compiler.IExtensionsConfiguration;
import org.sonatype.flexmojos.compiler.IFontsConfiguration;
import org.sonatype.flexmojos.compiler.IMxmlConfiguration;
import org.sonatype.flexmojos.compiler.INamespacesConfiguration;
import org.sonatype.flexmojos.plugin.common.FlexExtension;
import org.sonatype.flexmojos.plugin.common.FlexScopes;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenArtifact;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenExtension;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenFontsConfiguration;
import org.sonatype.flexmojos.plugin.compiler.attributes.Module;
import org.sonatype.flexmojos.plugin.utilities.MavenUtils;
import org.sonatype.flexmojos.plugin.utilities.SourceFileResolver;
import org.sonatype.flexmojos.util.PathUtil;

public abstract class AbstractCompilerMojoAdapter
    extends AbstractMojoAdapter
    implements ICompilerConfiguration, IExtensionsConfiguration, IMojoAdapter,
    // Defer implementation of these to sub classes.
    IMxmlConfiguration, INamespacesConfiguration
{
    private static final Logger log =
        LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.internal.adapters.AbstractCompilerMojoAdapter" );

    public static final String FRAMEWORK_GROUP_ID = "com.adobe.flex.framework";

    private static final String PLAYER_GLOBAL = "playerglobal";

    private static final String AIR_GLOBAL = "airglobal";

    // Flex Mojos Configuration Properties
    public static final String ACCESSIBLE = "accessible";

    public static final String ACTIONSCRIPT_FILE_ENCODING = "actionscriptFileEncoding";

    public static final String ADJUST_OPDEBUGLINE = "adjustOpdebugline";

    public static final String ALLOW_DUPLICATE_DEFAULT_STYLE_DECLARATIONS = "allowDuplicateDefaultStyleDeclarations";

    public static final String ALLOW_SOURCE_PATH_OVERLAP = "allowSourcePathOverlap";

    public static final String ARCHIVE_CLASSES_AND_ASSETS = "archiveClassesAndAssets";

    public static final String AS3 = "as3";

    public static final String AUTO_SORT_RSLS = "autoSortRsls";

    public static final String COMPATIBILITY_VERSION = "compatiblityVersion";

    public static final String COMPILE_SOURCE_ROOTS = "compileSourceRoots";

    public static final String COMPILER_WARNINGS = "compilerWarnings";

    public static final String CONSERVATIVE = "conservative";

    public static final String CONTEXT_ROOT = "contextRoot";

    public static final String DEBUG = "debug";

    public static final String DEFAULTS_CSS_FILES = "defaultsCssFiles";

    public static final String DEFAULTS_CSS_URL = "defaultsCssUrl";

    public static final String DEFINES = "defines";

    public static final String DISABLE_INCREMENTAL_OPTIMIZATIONS = "disableIncrementalOptimizations";

    public static final String DOC = "doc";

    public static final String ENABLE_RUNTIME_DESIGN_LAYERS = "enableRuntimeDesignLayers";

    public static final String ENABLE_SWC_VERSION_FILTERING = "enableSwcVersionFiltering";

    public static final String ES = "es";

    public static final String EXTENSIONS = "extensions";

    public static final String FONTS = "fonts";

    public static final String GENERATE_ABSTRACT_SYNTAX_TREE = "generateAbstractSyntaxTree";

    public static final String HEADLESS_SERVER = "headlessServer";

    public static final String INCREMENTAL = "incremental";

    public static final String ISOLATE_STYLES = "isolateStyles";

    public static final String JAVA_PROFILER_CLASS = "javaProfilerClass";

    public static final String KEEP_ALL_TYPE_SELECTORS = "keepAllTypeSelectors";

    public static final String KEEP_AS3_METADATAS = "keepAs3Metadatas";

    public static final String KEEP_GENERATED_ACTIONSCRIPT = "keepGeneratedActionscript";

    public static final String KEEP_GENERATED_SIGNATURES = "keepGeneratedSignatures";

    public static final String LOCALES_COMPILED = "localesCompiled";

    public static final String LOCALES_RUNTIME = "localesRuntime";

    public static final String LOCALES_SOURCE_PATH = "localesSourcePath";

    public static final String MEMORY_USAGE_FACTOR = "memoryUsageFactor";

    public static final String MINIMUM_SUPPORTED_VERSION = "minimumSupportedVersion";

    public static final String MOBILE = "mobile";

    public static final String MODULES = "modules";

    public static final String NAMESPACES = "namespaces";

    public static final String OMIT_TRACE_STATEMENTS = "omitTraceStatements";

    public static final String OPTIMIZE = "optimize";

    public static final String PRELOADER = "preloader";

    public static final String REPORT_MISSING_REQUIRED_SKIN_PARTS_AS_WARNINGS =
        "reportMissingRequiredSkinPartsAsWarnings";

    public static final String RESOURCE_HACK = "resourceHack";

    public static final String SERVICES = "services";

    public static final String SIGNATURE_DIRECTORY = "signature_directory";

    public static final String SOURCE_PATHS = "sourcePaths";

    public static final String STRICT = "strict";

    public static final String THEMES = "themes";

    public static final String TRANSLATION_FORMAT = "translationFormat";

    public static final String QUALIFIED_TYPE_SELECTORS = "getQualifiedTypeSelectors";

    public static final String USE_RESOURCE_BUNDLE_METADATA = "useResourceBundleMetadata";

    public static final String VERBOSE_STACKTRACES = "verboseStacktraces";

    protected static final Matcher<? extends Artifact> GLOBAL_MATCHER = initGlobalMatcher();

    @SuppressWarnings( "unchecked" )
    private static Matcher<? extends Artifact> initGlobalMatcher()
    {
        return allOf( groupId( FRAMEWORK_GROUP_ID ), type( SWC ),//
                      anyOf( artifactId( PLAYER_GLOBAL ), artifactId( AIR_GLOBAL ) ) );
    }

    protected Map<String, Boolean> convertedWarnings;

    protected Set<Artifact> dependencies;

    private Collection<Artifact> globalArtifact;

    private List<Method> consumedProperties;

    private List<Method> properties;

    public AbstractCompilerMojoAdapter( final IProgressMonitor monitor, final MavenSession session,
                                        final MojoExecution execution )
    {
        super( monitor, session, execution );

        consumedProperties = new ArrayList<Method>();

        properties = new ArrayList<Method>();
        addProperties( ICompilerConfiguration.class );
        addProperties( IExtensionsConfiguration.class );
        addProperties( IMxmlConfiguration.class );
        addProperties( INamespacesConfiguration.class );

    }

    /**
     * Generate an accessible SWF
     * <p>
     * Equivalent to -compiler.accessible
     * </p>
     * Default: false
     */
    public Boolean getAccessible()
    {
        final Boolean accessible = getParameterValue( ACCESSIBLE, Boolean.class );
        return ( accessible == null ) ? false : accessible;
    }

    /**
     * Specifies actionscript file encoding. If there is no BOM in the AS3 source files, the compiler will use this file
     * encoding.
     * <p>
     * Equivalent to -compiler.actionscript-file-encoding
     * </p>
     * Default: (default OS encoding)
     */
    public String getActionscriptFileEncoding()
    {
        final String encoding = getParameterValue( ACTIONSCRIPT_FILE_ENCODING, String.class );
        return encoding;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.adjust-opdebugline
     * </p>
     * Default: false
     */
    public Boolean getAdjustOpdebugline()
    {
        final Boolean adjust = getParameterValue( ADJUST_OPDEBUGLINE, Boolean.class );

        return ( adjust == null ) ? false : adjust;
    }

    /**
     * If true, a style manager will add style declarations to the local style manager without checking to see if the
     * parent already has the same style selector with the same properties. If false, a style manager will check the
     * parent to make sure a style with the same properties does not already exist before adding one locally.<BR>
     * If there is no local style manager created for this application, then don't check for duplicates. Just use the
     * old "selector exists" test.
     * <p>
     * Equivalent to -compiler.allow-duplicate-style-declaration
     * </p>
     * Default: false
     */
    public Boolean getAllowDuplicateDefaultStyleDeclarations()
    {
        final Boolean allow = getParameterValue( ALLOW_DUPLICATE_DEFAULT_STYLE_DECLARATIONS, Boolean.class );

        return ( allow == null ) ? false : allow;
    }

    /**
     * checks if a source-path entry is a subdirectory of another source-path entry. It helps make the package names of
     * MXML components unambiguous.
     * <p>
     * Equivalent to -compiler.allow-source-path-overlap
     * </p>
     * Default: false
     */
    public Boolean getAllowSourcePathOverlap()
    {
        final Boolean allow = getParameterValue( ALLOW_SOURCE_PATH_OVERLAP, Boolean.class );

        return ( allow == null ) ? false : allow;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.archive-classes-and-assets
     * </p>
     * Default: false
     */
    public Boolean getArchiveClassesAndAssets()
    {
        final Boolean archive = getParameterValue( ARCHIVE_CLASSES_AND_ASSETS, Boolean.class );

        return ( archive == null ) ? false : archive;
    }

    /**
     * Use the ActionScript 3 class based object model for greater performance and better error reporting. In the class
     * based object model most built-in functions are implemented as fixed methods of classes
     * <p>
     * Equivalent to -compiler.as3
     * </p>
     * Default: true
     */
    public Boolean getAs3()
    {
        final Boolean as3 = getParameterValue( AS3, Boolean.class );

        return ( as3 == null ) ? true : as3;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.conservative
     * </p>
     * compiler algorithm settings Default: false
     */
    public Boolean getConservative()
    {
        final Boolean con = getParameterValue( CONSERVATIVE, Boolean.class );

        return ( con == null ) ? false : con;
    }

    /**
     * Path to replace {context.root} tokens for service channel endpoints
     * <p>
     * Equivalent to -compiler.context-root
     * </p>
     * Default: null
     */
    public String getContextRoot()
    {
        final String value = getParameterValue( CONTEXT_ROOT, String.class );

        return value;
    }

    /**
     * Path to replace {context.root} tokens for service channel endpoints
     * <p>
     * Equivalent to -compiler.context-root
     * </p>
     * Default: false
     */
    public Boolean getDebug()
    {
        final Boolean value = getParameterValue( DEBUG, Boolean.class );

        return ( value == null ) ? false : value;
    }

    /**
     * Location of defaults style stylesheets
     * <p>
     * Equivalent to -compiler.defaults-css-url
     * </p>
     * Default: null
     */
    public List<File> getDefaultsCssFiles()
    {
        final File[] files = getParameterValue( DEFAULTS_CSS_FILES, File[].class );

        return ( files == null ) ? null : Arrays.asList( files );
    }

    /**
     * Defines the location of the default style sheet. Setting this option overrides the implicit use of the
     * defaults.css style sheet in the framework.swc file
     * <p>
     * Equivalent to -compiler.defaults-css-url
     * </p>
     * Default: null
     */
    public String getDefaultsCssUrl()
    {
        final String value = getParameterValue( DEFAULTS_CSS_URL, String.class );

        return value;
    }

    /**
     * Define a global AS3 conditional compilation definition, e.g. -define=CONFIG::debugging,true or
     * -define+=CONFIG::debugging,true (to append to existing definitions in flex-config.xml)
     * <p>
     * Equivalent to -compiler.define
     * </p>
     * Default: null
     */
    public IDefine[] getDefine()
    {
        final Properties defines = getParameterValue( DEFINES, Properties.class );

        if ( defines == null )
        {
            return null;
        }

        List<IDefine> keys = new ArrayList<IDefine>();
        Set<Entry<Object, Object>> entries = defines.entrySet();
        for ( final Entry<Object, Object> entry : entries )
        {
            keys.add( new IDefine()
            {
                public String name()
                {
                    return entry.getKey().toString();
                }

                public String value()
                {
                    return entry.getValue().toString();
                }
            } );
        }

        return keys.toArray( new IDefine[keys.size()] );
    }

    /**
     * Back-door to disable optimizations in case they are causing problems
     * <p>
     * Equivalent to -compiler.disable-incremental-optimizations
     * </p>
     * Default: false
     */
    public Boolean getDisableIncrementalOptimizations()
    {
        final Boolean value = getParameterValue( DISABLE_INCREMENTAL_OPTIMIZATIONS, Boolean.class );

        return ( value == null ) ? false : value;
    }

    /**
     * DOCME undocumented
     * <p>
     * Equivalent to -compiler.doc
     * </p>
     * Default: null
     */
    public Boolean getDoc()
    {
        final Boolean value = getParameterValue( DOC, Boolean.class );

        return value;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.enable-runtime-design-layers
     * </p>
     * Default: null
     */
    public Boolean getEnableRuntimeDesignLayers()
    {
        final Boolean value = getParameterValue( ENABLE_RUNTIME_DESIGN_LAYERS, Boolean.class );

        return value;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.enable-swc-version-filtering
     * </p>
     * Default: null
     */
    public Boolean getEnableSwcVersionFiltering()
    {
        final Boolean value = getParameterValue( ENABLE_SWC_VERSION_FILTERING, Boolean.class );

        return value;
    }

    /**
     * Use the ECMAScript edition 3 prototype based object model to allow dynamic overriding of prototype properties. In
     * the prototype based object model built-in functions are implemented as dynamic properties of prototype objects
     * <p>
     * Equivalent to -compiler.es
     * </p>
     * Default: null
     */
    public Boolean getEs()
    {
        final Boolean value = getParameterValue( ES, Boolean.class );

        return value;
    }

    /**
     * Configure extensions to flex compiler
     * <p>
     * Equivalent to -compiler.extensions.extension
     * </p>
     * Default:
     */
    public IExtensionsConfiguration getExtensionsConfiguration()
    {
        return this;
    }

    public IExtension[] getExtension()
    {
        final IExtension[] extensions = getParameterValue( EXTENSIONS, IExtension[].class );

        if ( extensions == null )
            return null;

        for ( int i = 0; i < extensions.length; i++ )
        {
            final MavenExtension extension = (MavenExtension) extensions[i];

            if ( extension.getExtensionArtifact() == null )
            {
                throw new IllegalArgumentException( "Extension artifact is required!" );
            }

            extensions[i] = new IExtension()
            {
                public File extension()
                {
                    final IMaven maven = MavenPlugin.getMaven();

                    MavenArtifact a = extension.getExtensionArtifact();
                    Artifact resolvedArtifact = null;
                    try
                    {
                        resolvedArtifact =
                            maven.resolve( a.getGroupId(), a.getArtifactId(), a.getVersion(), a.getClassifier(),
                                           a.getType(), maven.getArtifactRepositories(), monitor );
                        return resolvedArtifact.getFile();
                    }
                    catch ( CoreException e )
                    {
                        if ( log.isErrorEnabled() )
                            log.error( "Unable to resolve compiler extension.", e );
                    }

                    return null;
                }

                public String[] parameters()
                {
                    return extension.getParameters();
                }
            };
        }

        return extensions;
    }

    @SuppressWarnings( "unchecked" )
    public File[] getExternalLibraryPath()
    {
        if ( SWC.equals( getProjectType() ) )
        {
            Matcher<? extends Artifact> swcs =
                allOf( type( SWC ), //
                       anyOf( scope( EXTERNAL ), scope( CACHING ), scope( RSL ), scope( COMPILE ),
                              scope( nullValue( String.class ) ) )//
                );
            return MavenUtils.getFiles( getDependencies( swcs, not( GLOBAL_MATCHER ) ), getGlobalArtifact() );
        }
        else
        {
            return MavenUtils.getFiles( getDependencies( not( GLOBAL_MATCHER ),//
                                                         allOf( type( SWC ),//
                                                                anyOf( scope( EXTERNAL ), scope( CACHING ), scope( RSL ) ) ) ),
                                        getGlobalArtifact() );
        }
    }

    /**
     * Fonts configurations to be used on SWF compilation
     * <p>
     * Equivalent to -compiler.fonts.*
     * </p>
     * Default: null
     */
    public IFontsConfiguration getFontsConfiguration()
    {
        MavenFontsConfiguration fonts = getParameterValue( FONTS, MavenFontsConfiguration.class );

        if ( fonts == null )
            fonts = new MavenFontsConfiguration();

        return fonts.toFontsConfiguration( getOutputDirectory() );
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.generate-abstract-syntax-tree
     * </p>
     * Default: null
     */
    public Boolean getGenerateAbstractSyntaxTree()
    {
        final Boolean value = getParameterValue( GENERATE_ABSTRACT_SYNTAX_TREE, Boolean.class );

        return value;
    }

    /**
     * A flag to set when Flex is running on a server without a display
     * <p>
     * Equivalent to -compiler.headless-server
     * </p>
     * Default: null
     */
    public Boolean getHeadlessServer()
    {
        final Boolean value = getParameterValue( HEADLESS_SERVER, Boolean.class );

        return value;
    }

    @SuppressWarnings( "unchecked" )
    public File[] getIncludeLibraries()
    {
        return MavenUtils.getFiles( getDependencies( type( SWC ), scope( INTERNAL ), not( GLOBAL_MATCHER ) ) );
    }

    /**
     * Enables incremental compilation
     * <p>
     * Equivalent to -compiler.incremental
     * </p>
     * Default: null
     */
    public Boolean getIncremental()
    {
        final Boolean value = getParameterValue( INCREMENTAL, Boolean.class );

        return value;
    }

    /**
     * Enables the compiled application or module to set styles that only affect itself and its children.<BR>
     * Allow the user to decide if the compiled application/module should have its own style manager
     * <p>
     * Equivalent to -compiler.isolate-styles
     * </p>
     * Default: null
     */
    public Boolean getIsolateStyles()
    {
        final Boolean value = getParameterValue( ISOLATE_STYLES, Boolean.class );

        return value;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.java-profiler-class
     * </p>
     * Default: null
     */
    public String getJavaProfilerClass()
    {
        final String value = getParameterValue( JAVA_PROFILER_CLASS, String.class );

        return value;
    }

    /**
     * Disables the pruning of unused CSS type selectors
     * <p>
     * Equivalent to -compiler.keep-all-type-selectors
     * </p>
     * Default: null
     */
    public Boolean getKeepAllTypeSelectors()
    {
        final Boolean value = getParameterValue( KEEP_ALL_TYPE_SELECTORS, Boolean.class );

        return value;
    }

    /**
     * Keep the specified metadata in the SWF
     * <p>
     * Equivalent to -compiler.keep-as3-metadata
     * </p>
     * Usage: Default: null
     */
    public String[] getKeepAs3Metadata()
    {
        final String[] value = getParameterValue( KEEP_AS3_METADATAS, String[].class );

        return value;
    }

    /**
     * Keep the specified metadata in the SWF
     * <p>
     * Equivalent to -compiler.keep-generated-actionscript
     * </p>
     * Default: null
     */
    public Boolean getKeepGeneratedActionscript()
    {
        return getParameterValue( KEEP_GENERATED_ACTIONSCRIPT, Boolean.class );
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.keep-generated-signatures
     * </p>
     * Default: null
     */
    public Boolean getKeepGeneratedSignatures()
    {
        return getParameterValue( KEEP_GENERATED_SIGNATURES, Boolean.class );
    }

    public File[] getLibraryPath()
    {
        return getLibraryPath( true );
    }

    @SuppressWarnings( "unchecked" )
    private File[] getLibraryPath( boolean includeResourceBundle )
    {
        Collection<Artifact> resourceBundle =
            includeResourceBundle ? getCompiledResouceBundles() : Collections.EMPTY_LIST;
        if ( SWC.equals( getProjectType() ) )
        {
            return MavenUtils.getFiles( getDependencies( type( SWC ), scope( MERGED ), not( GLOBAL_MATCHER ) ),
                                        resourceBundle );
        }
        else
        {
            return MavenUtils.getFiles( getDependencies( type( SWC ),//
                                                         anyOf( scope( MERGED ), scope( COMPILE ),
                                                                scope( nullValue( String.class ) ) ),//
                                                         not( GLOBAL_MATCHER ) ),//
                                        resourceBundle );
        }
    }

    /**
     * Specifies the locale for internationalization
     * <p>
     * Equivalent to -compiler.locale
     * </p>
     * Usage: Default: en_US
     */
    public String[] getLocale()
    {
        // I don't have a way to deal with runtime locales using FlashBuilder right now
        // so instead use the following... 1. compiled locales, 2. runtime locales, 3. en_US

        String[] localesCompiled = getLocalesCompiled();

        if ( localesCompiled == null )
            localesCompiled = getLocalesRuntime();

        if ( localesCompiled == null )
            localesCompiled = new String[] { "en_US" };

        if ( localesCompiled != null )
        {
            String[] locales = new String[localesCompiled.length];
            for ( int i = 0; i < localesCompiled.length; i++ )
            {
                String locale = localesCompiled[i];
                if ( locale.contains( "," ) )
                {
                    locale = locale.split( "," )[0];
                }
                locales[i] = locale;
            }
            return locales;
        }

        return null;
    }

    protected String[] getLocalesCompiled()
    {
        final String[] localesCompiled = getParameterValue( LOCALES_COMPILED, String[].class );

        return localesCompiled;
    }

    /**
     * Specifies the locales for external internationalization bundles
     * <p>
     * No equivalent parameter
     * </p>
     * Usage: Default: null
     */
    protected String[] getLocalesRuntime()
    {
        final String[] localesRuntime = getParameterValue( LOCALES_RUNTIME, String[].class );

        // TODO figure out good way to deal with locale resource bundles.
        // if ( localesRuntime == null )
        // {
        // return null;
        // }
        // try
        // {
        // File rbBeacon = new File( getTargetDirectory(), getFinalName() + "." + RB_SWC );
        // FileUtils.copyURLToFile( getClass().getResource( "/rb.swc" ), rbBeacon );
        // projectHelper.attachArtifact( project, RB_SWC, rbBeacon );
        // }
        // catch ( IOException e )
        // {
        // throw new MavenRuntimeException( "Failed to create beacon resource bundle", e );
        // }

        return localesRuntime;
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.memory-usage-factor
     * </p>
     * Default: null
     */
    public Integer getMemoryUsageFactor()
    {
        return getParameterValue( MEMORY_USAGE_FACTOR, Integer.class );
    }

    /**
     * Specifies the target runtime is a mobile device
     * <p>
     * Equivalent to -compiler.mobile
     * </p>
     * Default: null
     */
    public Boolean getMobile()
    {
        return getParameterValue( MOBILE, Boolean.class );
    }

    public IMxmlConfiguration getMxmlConfiguration()
    {
        return this;
    }

    public INamespacesConfiguration getNamespacesConfiguration()
    {
        return this;
    }

    /**
     * Toggle whether trace statements are omitted
     * <p>
     * Equivalent to -compiler.omit-trace-statements
     * </p>
     * Default: null
     */
    public Boolean getOmitTraceStatements()
    {
        return getParameterValue( OMIT_TRACE_STATEMENTS, Boolean.class );
    }

    /**
     * Enable post-link SWF optimization
     * <p>
     * Equivalent to -compiler.optimize
     * </p>
     * Default: null
     */
    public Boolean getOptimize()
    {
        return getParameterValue( OPTIMIZE, Boolean.class );
    }

    /**
     * Specifies the default value for the Application's preloader attribute. If not specified, the default preloader
     * value will be mx.preloaders.SparkDownloadProgressBar with -compatibility-version >= 4.0 and it will be
     * mx.preloader.DownloadProgressBar with -compatibility-version < 4.0.
     * <p>
     * Equivalent to -compiler.preloader
     * </p>
     * Default: null
     */
    public String getPreloader()
    {
        return getParameterValue( PRELOADER, String.class );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.report-invalid-styles-as-warnings
     * </p>
     * Default: null
     */
    public Boolean getReportInvalidStylesAsWarnings()
    {
        return getCompilerWarnings().get( "report-invalid-styles-as-warnings" );
    }

    /**
     * Use this option to generate a warning instead of an error when a missing required skin part is detected.
     * <p>
     * Equivalent to -compiler.report-missing-required-skin-parts-as-warnings
     * </p>
     * Default: null
     */
    public Boolean getReportMissingRequiredSkinPartsAsWarnings()
    {
        return getParameterValue( REPORT_MISSING_REQUIRED_SKIN_PARTS_AS_WARNINGS, Boolean.class );
    }

    /**
     * This undocumented option is for compiler performance testing. It allows the Flex 3 compiler to compile the Flex 2
     * framework and Flex 2 apps. This is not an officially-supported combination
     * <p>
     * Equivalent to -compiler.resource-hack
     * </p>
     * Default: null
     */
    public Boolean getResourceHack()
    {
        return getParameterValue( RESOURCE_HACK, Boolean.class );
    }

    /**
     * Path to Flex Data Services configuration file
     * <p>
     * Equivalent to -compiler.services
     * </p>
     * Default: null
     */
    public String getServices()
    {
        return getParameterValue( SERVICES, String.class );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-actionscript-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowActionscriptWarnings()
    {
        return getCompilerWarnings().get( "show-actionscript-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-binding-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowBindingWarnings()
    {
        return getCompilerWarnings().get( "show-binding-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-dependency-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowDependencyWarnings()
    {
        return getCompilerWarnings().get( "show-dependency-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-deprecation-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowDeprecationWarnings()
    {
        return getCompilerWarnings().get( "show-deprecation-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-invalid-css-property-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowInvalidCssPropertyWarnings()
    {
        return getCompilerWarnings().get( "show-invalid-css-property-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-shadowed-device-font-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowShadowedDeviceFontWarnings()
    {
        return getCompilerWarnings().get( "show-shadowed-device-font-warnings" );
    }

    /**
     * Warn
     * <p>
     * Equivalent to -compiler.show-unused-type-selector-warnings
     * </p>
     * Default: null
     */
    public Boolean getShowUnusedTypeSelectorWarnings()
    {
        return getCompilerWarnings().get( "show-unused-type-selector-warnings" );
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.signature-directory
     * </p>
     * Default: null
     */
    public File getSignatureDirectory()
    {
        return getParameterValue( SIGNATURE_DIRECTORY, File.class );
    }

    /**
     * The maven compile source roots
     * <p>
     * Equivalent to -compiler.source-path
     * </p>
     * List of path elements that form the roots of ActionScript class Default: null
     */
    public File[] getSourcePath()
    {
        List<File> files = new ArrayList<File>();

        // compiled source roots
        files.addAll( PathUtil.existingFilesList( getCompileSourceRoots() ) );

        // source path
        final File[] sourcePath = getParameterValue( SOURCE_PATHS, File[].class );
        if ( sourcePath != null )
            files.addAll( Arrays.asList( sourcePath ) );

        // test source path
        final String testSourceDirStr = session.getCurrentProject().getBuild().getTestSourceDirectory();
        final File testSourceDir = new File( testSourceDirStr );
        if ( testSourceDir.exists() )
            files.add( testSourceDir );

        // locale path
        if ( getLocale() != null )
        {
            final File localesSourcePath = getLocalesSourcePath();
            if ( localesSourcePath.getParentFile().exists() )
            {
                files.add( localesSourcePath );
            }
        }

        // Add resource output paths
        List<Resource> resources = session.getCurrentProject().getBuild().getResources();
        for ( Resource resource : resources )
        {
            // Ignore if source dir doesn't exist
            if( new File( resource.getDirectory() ).exists() == false )
                continue;
            
            File target = null;
            final String path = resource.getTargetPath();
            if( path != null )
            {
                target = new File( path );
                // Convert to absolute path if necessary.
                if( !target.isAbsolute() )
                    target = PathUtil.file( path, getBuildDirectory() );
            }
            else
            {
                target = getOutputDirectory();
            }

            if( target != null )
                files.add( target );
        }
        List<Resource> testResources = session.getCurrentProject().getBuild().getTestResources();
        for ( Resource testResource : testResources )
        {
            // Ignore if source dir doesn't exist
            if( new File( testResource.getDirectory() ).exists() == false )
                continue;
            
            File target = null;
            final String path = testResource.getTargetPath();
            if( path != null )
            {
                target = new File( path );
                // Convert to absolute path if necessary.
                if( !target.isAbsolute() )
                    target = PathUtil.file( path, getBuildDirectory() );
            }
            else
            {
                target = getTestOutputDirectory();
            }

            if( target != null )
                files.add( target );
        }

        return files.toArray( new File[0] );
    }

    @SuppressWarnings( "unchecked" )
    protected List<String> getCompileSourceRoots()
    {
        return getParameterValue( COMPILE_SOURCE_ROOTS, List.class );
    }

    /**
     * Define the base path to locate resouce bundle files Accept some special tokens:
     * 
     * <pre>
     * {locale}     - replace by locale name
     * </pre>
     * 
     * Default: "${basedir}/src/main/locales/{locale}"
     */
    protected File getLocalesSourcePath()
    {
        File file = getParameterValue( LOCALES_SOURCE_PATH, File.class );
        if ( file == null )
        {
            // Get base directory from POM
            MavenProject project = session.getCurrentProject();
            File basedir = project.getBasedir();
            // Concatenate default path components.
            IPath path =
                new Path( PathUtil.path( basedir ) ).append( "src" ).append( "main" ).append( "locales" ).append( "{locale}" );
            file = path.toFile();
        }

        return file;
    }

    /**
     * Runs the AS3 compiler in strict error checking mode
     * <p>
     * Equivalent to -compiler.strict
     * </p>
     * Defualt: null
     */
    public Boolean getStrict()
    {
        return getParameterValue( STRICT, Boolean.class );
    }

    /**
     * List of CSS or SWC files to apply as a theme
     * <p>
     * Equivalent to -compiler.theme
     * </p>
     * <p>
     * There are three ways a theme can be included when you compile
     * </p>
     * 1 - you explicitly list a <theme> in the config of the pom file<BR>
     * 2 - You include a dependency with scope="theme" and with type="css" or type="swc"<BR>
     * 3 - if you don't do either of the above steps, flexmojos will attempt to automatically include a theme for you
     * based on your dependencies. (if you depend upon mx.swc halo will be included, if you depend upon spark.swc -
     * spark.css theme will be included) Default: blank list
     */
    @SuppressWarnings( "unchecked" )
    public List<String> getTheme()
    {
        List<File> themeList = new ArrayList<File>();

        // Step 1. Add theme dependencies
        final Set<Artifact> themeDependencies = getDependencies( allOf( anyOf( type( SWC ), type( CSS ) ), scope( FlexScopes.THEME ) ) );
        final List<File> themeDepsList = asList( MavenUtils.getFiles( themeDependencies ) );
        themeList.addAll( themeDepsList );
        
        // Step 2. Add local themes
        // if themes are specified in the <themes> configuration
        String[] themes = getParameterValue( THEMES, String[].class );
        if ( themes != null )
        {
            final File[] themeFiles = files( themes, getResourcesTargetDirectories() );
            themeList.addAll( asList( themeFiles ) );
        }
        
        // TODO investigate when and how Flex Mojos adds default spark and halo themes.

        if ( themeList.isEmpty() )
        {

            // console.logMessage(
            // "No themes are explicitly defined in the <theme> section or in any scope=\"theme\" dependencies. "
            // + "Flexmojos is now attempting to figure out which themes to include. (to avoid this warning "
            // + "you should explicitly state your theme dependencies)" );

            // configureThemeSparkCss( themeList );
            // configureThemeHaloSwc( themeList );
        }

        return pathsList( themeList );
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.translation-format
     * </p>
     * Default: null
     */
    public String getTranslationFormat()
    {
        return getParameterValue( TRANSLATION_FORMAT, String.class );
    }

    /**
     * Determines whether resources bundles are included in the application
     * <p>
     * Equivalent to -compiler.use-resource-bundle-metadata
     * </p>
     * Default: null
     */
    public Boolean getUseResourceBundleMetadata()
    {
        return getParameterValue( USE_RESOURCE_BUNDLE_METADATA, Boolean.class );
    }

    /**
     * Save callstack information to the SWF for debugging
     * <p>
     * Equivalent to -compiler.verbose-stacktraces
     * </p>
     * Default: null
     */
    public Boolean getVerboseStacktraces()
    {
        return getParameterValue( VERBOSE_STACKTRACES, Boolean.class );
    }

    public Boolean getWarnArrayTostringChanges()
    {
        return getCompilerWarnings().get( "warn-array-tostring-changes" );
    }

    public Boolean getWarnAssignmentWithinConditional()
    {
        return getCompilerWarnings().get( "warn-assignment-within-conditional" );
    }

    public Boolean getWarnBadArrayCast()
    {
        return getCompilerWarnings().get( "warn-bad-array-cast" );
    }

    public Boolean getWarnBadBoolAssignment()
    {
        return getCompilerWarnings().get( "warn-bad-bool-assignment" );
    }

    public Boolean getWarnBadDateCast()
    {
        return getCompilerWarnings().get( "warn-bad-date-cast" );
    }

    public Boolean getWarnBadEs3TypeMethod()
    {
        return getCompilerWarnings().get( "warn-bad-es3-type-method" );
    }

    public Boolean getWarnBadEs3TypeProp()
    {
        return getCompilerWarnings().get( "warn-bad-es3-type-prop" );
    }

    public Boolean getWarnBadNanComparison()
    {
        return getCompilerWarnings().get( "warn-bad-nan-comparison" );
    }

    public Boolean getWarnBadNullAssignment()
    {
        return getCompilerWarnings().get( "warn-bad-null-assignment" );
    }

    public Boolean getWarnBadNullComparison()
    {
        return getCompilerWarnings().get( "warn-bad-null-comparison" );
    }

    public Boolean getWarnBadUndefinedComparison()
    {
        return getCompilerWarnings().get( "warn-bad-undefined-comparison" );
    }

    public Boolean getWarnBooleanConstructorWithNoArgs()
    {
        return getCompilerWarnings().get( "warn-boolean-constructor-with-no-args" );
    }

    public Boolean getWarnChangesInResolve()
    {
        return getCompilerWarnings().get( "warn-changes-in-resolve" );
    }

    public Boolean getWarnClassIsSealed()
    {
        return getCompilerWarnings().get( "warn-class-is-sealed" );
    }

    public Boolean getWarnConstNotInitialized()
    {
        return getCompilerWarnings().get( "warn-const-not-initialized" );
    }

    public Boolean getWarnConstructorReturnsValue()
    {
        return getCompilerWarnings().get( "warn-constructor-returns-value" );
    }

    public Boolean getWarnDeprecatedEventHandlerError()
    {
        return getCompilerWarnings().get( "warn-deprecated-event-handler-error" );
    }

    public Boolean getWarnDeprecatedFunctionError()
    {
        return getCompilerWarnings().get( "warn-deprecated-function-error" );
    }

    public Boolean getWarnDeprecatedPropertyError()
    {
        return getCompilerWarnings().get( "warn-deprecated-property-error" );
    }

    public Boolean getWarnDuplicateArgumentNames()
    {
        return getCompilerWarnings().get( "warn-duplicate-argument-names" );
    }

    public Boolean getWarnDuplicateVariableDef()
    {
        return getCompilerWarnings().get( "warn-duplicate-variable-def" );
    }

    public Boolean getWarnForVarInChanges()
    {
        return getCompilerWarnings().get( "warn-for-var-in-changes" );
    }

    public Boolean getWarnImportHidesClass()
    {
        return getCompilerWarnings().get( "warn-import-hides-class" );
    }

    public Boolean getWarnInstanceOfChanges()
    {
        return getCompilerWarnings().get( "warn-instance-of-changes" );
    }

    public Boolean getWarnInternalError()
    {
        return getCompilerWarnings().get( "warn-internal-error" );
    }

    public Boolean getWarnLevelNotSupported()
    {
        return getCompilerWarnings().get( "warn-level-not-supported" );
    }

    public Boolean getWarnMissingNamespaceDecl()
    {
        return getCompilerWarnings().get( "warn-missing-namespace-decl" );
    }

    public Boolean getWarnNegativeUintLiteral()
    {
        return getCompilerWarnings().get( "warn-negative-uint-literal" );
    }

    public Boolean getWarnNoConstructor()
    {
        return getCompilerWarnings().get( "warn-no-constructor" );
    }

    public Boolean getWarnNoExplicitSuperCallInConstructor()
    {
        return getCompilerWarnings().get( "warn-no-explicit-super-call-in-constructor" );
    }

    public Boolean getWarnNoTypeDecl()
    {
        return getCompilerWarnings().get( "warn-no-type-decl" );
    }

    public Boolean getWarnNumberFromStringChanges()
    {
        return getCompilerWarnings().get( "warn-number-from-string-changes" );
    }

    public Boolean getWarnScopingChangeInThis()
    {
        return getCompilerWarnings().get( "warn-scoping-change-in-this" );
    }

    public Boolean getWarnSlowTextFieldAddition()
    {
        return getCompilerWarnings().get( "warn-slow-text-field-addition" );
    }

    public Boolean getWarnUnlikelyFunctionValue()
    {
        return getCompilerWarnings().get( "warn-unlikely-function-value" );
    }

    public Boolean getWarnXmlClassHasChanged()
    {
        return getCompilerWarnings().get( "warn-xml-class-has-changed" );
    }

    @Override
    public String getTargetPlayer()
    {
        String version = super.getTargetPlayer();

        // If not found in configuration then look up version of the global artifact
        if ( version == null )
        {
            Collection<Artifact> c = getGlobalArtifact();
            Artifact playerGlobal = c.iterator().next();
            if ( "playerglobal".equals( playerGlobal.getArtifactId() ) )
            {
                version = playerGlobal.getClassifier();
            }
        }

        return version;
    }

    @SuppressWarnings( "unchecked" )
    public Collection<Artifact> getGlobalArtifact()
    {
        if ( globalArtifact != null )
            return globalArtifact;

        Artifact global = getDependency( GLOBAL_MATCHER );
        if ( global == null )
        {
            throw new IllegalArgumentException(
                                                "Global artifact is not available. Make sure to add 'playerglobal' or 'airglobal' to this project." );
        }

        // don't think there is any need to rename this
        // File source = global.getFile();
        // File dest =
        // new File( source.getParentFile(), global.getClassifier() + "/" + global.getArtifactId() + "." + SWC );
        // global.setFile( dest );
        // try
        // {
        // if ( !dest.exists() )
        // {
        // dest.getParentFile().mkdirs();
        // FileUtils.copyFile( source, dest );
        // }
        // }
        // catch ( IOException e )
        // {
        // throw new IllegalStateException( "Error renamming '" + global.getArtifactId() + "'.", e );
        // }

        return Collections.singletonList( global );
    }

    public String getProjectType()
    {
        return session.getCurrentProject().getPackaging();
    }

    public Set<Artifact> getDependencies()
    {
        if ( dependencies != null )
            return dependencies;

        final MavenProject project = session.getCurrentProject();
        final Set<Artifact> deps = project.getArtifacts();

        // Append compiled resource bundles.
        deps.addAll( getCompiledResouceBundles() );

        // NOTE: artifact paths resolve to the Java build directory (target/classes).
        // This is a problem since flex needs a path that resolves to the SWC.
        for ( Artifact a : deps )
        {
            // Occasionally getting null artifacts... not sure why.
            if( a == null )
                continue;
            
            if ( SWC.equals( a.getType() ) )
            {
                final IProject refProject =
                    net.flexmojos.eclipse.m2e.encrypted.util.PathUtil.getWorkspaceProjectForArtifact( new ArtifactKey( a ) );

                if ( refProject != null )
                {

                    IMavenProjectFacade mavenProject = MavenPlugin.getMavenProjectRegistry().getProject( refProject );

                    String buildDir = mavenProject.getMavenProject().getBuild().getDirectory();

                    String swcPath = buildDir += "/" + refProject.getName() + ".swc";

                    // On windows a C: is added to the front... need to remove it
                    final int cColonIndex = swcPath.indexOf( "C:" );
                    if ( cColonIndex > -1 )
                        swcPath = swcPath.substring( cColonIndex + 2, swcPath.length() );

                    a.setFile( new File( swcPath ) );
                }
            }
        }

        dependencies = Collections.unmodifiableSet( deps );

        return dependencies;
    }

    @SuppressWarnings( "unchecked" )
    public Set<Artifact> getDependencies( boolean includeSdkDependencies )
    {
        return ( includeSdkDependencies ) ? getDependencies( anyOf( type( FlexExtension.SWC ),
                                                                    type( FlexExtension.SWZ ),
                                                                    type( FlexExtension.RB_SWC ) ) )
                        : getDependencies( anyOf( type( FlexExtension.SWC ), type( FlexExtension.SWZ ),
                                                  type( FlexExtension.RB_SWC ) ), not( groupId( FRAMEWORK_GROUP_ID ) ) );
    }

    @SuppressWarnings( "unchecked" )
    public Set<Artifact> getSdkDependencies()
    {
        return getDependencies( groupId( FRAMEWORK_GROUP_ID ) );
    }

    public Set<Artifact> getDependencies( Matcher<? extends Artifact>... matchers )
    {
        Set<Artifact> dependencies = getDependencies();

        return new LinkedHashSet<Artifact>( filter( allOf( matchers ), dependencies ) );
    }

    protected Artifact getDependency( Matcher<? extends Artifact>... matchers )
    {
        return selectFirst( getDependencies(), allOf( matchers ) );
    }

    public Boolean getAutoSortRsls()
    {
        return ( getParameterValue( AUTO_SORT_RSLS, Boolean.class ) == null ) ? false : true;
    }

    /**
     * A list of warnings that should be enabled/disabled
     * <p>
     * Equivalent to -compiler.show-actionscript-warnings, -compiler.show-binding-warnings,
     * -compiler.show-shadowed-device-font-warnings, -compiler.show-unused-type-selector-warnings and -compiler.warn-*
     * </p>
     * Usage: Default: empty map
     */
    public Map<String, Boolean> getCompilerWarnings()
    {
        if ( convertedWarnings != null )
            return convertedWarnings;

        convertedWarnings = new LinkedHashMap<String, Boolean>();

        @SuppressWarnings( "unchecked" )
        final Map<String, String> compilerWarnings = getParameterValue( COMPILER_WARNINGS, Map.class );

        if ( compilerWarnings != null )
        {
            // converts the <String, String> map into a <String, Boolean> one
            Set<Entry<String, String>> warns = compilerWarnings.entrySet();
            for ( Entry<String, String> entry : warns )
            {
                convertedWarnings.put( entry.getKey(), Boolean.valueOf( entry.getValue() ) );
            }
        }

        return convertedWarnings;
    }

    public File getTestOutputDirectory()
    {
        final MavenProject project = session.getCurrentProject();
        final File outputDirectory = new File( project.getBuild().getTestOutputDirectory() );
        outputDirectory.mkdirs();
        return PathUtil.file( outputDirectory );
    }

    public File getOutputDirectory()
    {
        final MavenProject project = session.getCurrentProject();
        final File outputDirectory = new File( project.getBuild().getOutputDirectory() );
        outputDirectory.mkdirs();
        return PathUtil.file( outputDirectory );
    }

    public String getBuildDirectory()
    {
        return session.getCurrentProject().getBuild().getDirectory();
    }

    public File getProjectLocation()
    {
        return session.getCurrentProject().getBasedir();
    }

    public boolean isAirProject()
    {
        return ( selectFirst( getGlobalArtifact(), artifactId( AIR_GLOBAL ) ) != null );
    }

    protected List<File> getResourcesTargetDirectories()
    {
        final MavenProject project = session.getCurrentProject();
        List<Resource> resources = project.getResources();

        List<File> directories = new ArrayList<File>();
        for ( Resource resource : resources )
        {
            File directory;
            if ( resource.getTargetPath() != null )
            {
                directory = PathUtil.file( resource.getTargetPath(), project.getBasedir() );
            }
            else
            {
                directory = getOutputDirectory();
            }
            if ( !directory.isDirectory() )
            {
                continue;
            }

            directories.add( directory );
        }
        return directories;
    }

    @FlexCompatibility( minVersion = "4.0.0.11420" )
    private void configureThemeHaloSwc( List<File> themes )
    {
        File haloSwc = resolveThemeFile( "mx", "halo", "swc", "Halo" );
        if ( haloSwc == null )
        {
            return;
        }

        // console.logMessage( "Added the halo.swc theme because mx.swc was included as a dependency" );
        themes.add( haloSwc );
    }

    @FlexCompatibility( minVersion = "4.0.0.11420" )
    private void configureThemeSparkCss( List<File> themes )
    {
        File sparkCss = resolveThemeFile( "spark", "spark", "css", "Spark" );
        if ( sparkCss == null )
        {
            return;
        }

        // console.logMessage( "Added the spark.css theme because spark.swc was included as a dependency" );
        themes.add( sparkCss );
    }

    @SuppressWarnings( "unchecked" )
    private File resolveThemeFile( String artifactName, String themeName, String type, String path )
    {
        Artifact sparkSwc = getDependency( groupId( FRAMEWORK_GROUP_ID ), artifactId( artifactName ), type( "swc" ) );
        if ( sparkSwc == null )
        {
            return null;
        }

        File sparkCss = null;
        try
        {
            // first try to get the artifact from maven local repository for the appropriated flex version
            IMaven maven = MavenPlugin.getMaven();
            sparkCss =
                maven.resolve( FRAMEWORK_GROUP_ID, themeName, getFrameworkVersion(), "theme", type,
                               maven.getArtifactRepositories(), monitor ).getFile();
        }
        catch ( CoreException e )
        {
            if ( log.isErrorEnabled() )
                log.error( "Error resolving theme.", e );
        }
        return sparkCss;
    }

    @SuppressWarnings( "unchecked" )
    public String getFrameworkVersion()
    {
        Artifact dep = null;
        if ( dep == null )
        {
            dep = getDependency( GLOBAL_MATCHER );
        }
        if ( dep == null )
        {
            dep = getDependency( groupId( "com.adobe.flex.framework" ), artifactId( "flex-framework" ), type( "pom" ) );
        }
        if ( dep == null )
        {
            dep = getDependency( groupId( "com.adobe.flex.framework" ), artifactId( "air-framework" ), type( "pom" ) );
        }
        if ( dep == null )
        {
            getDependency( groupId( "com.adobe.flex.framework" ), artifactId( "framework" ), type( "swc" ) );
        }
        if ( dep == null )
        {
            getDependency( groupId( "com.adobe.flex.framework" ), artifactId( "airframework" ), type( "swc" ) );
        }

        if ( dep == null )
        {
            return null;
        }
        return dep.getVersion();
    }

    public Artifact getSourceBundle( Artifact artifact )
    {

        Artifact requestedSourceBundle = null;

        try
        {
            final IMaven maven = MavenPlugin.getMaven();

            requestedSourceBundle =
                maven.resolve( artifact.getGroupId(), artifact.getArtifactId(), artifact.getVersion(), "jar",
                               "sources", maven.getArtifactRepositories(), monitor );
        }
        catch ( CoreException e )
        {
            if ( log.isErrorEnabled() )
                log.error( "Error getting source bundle for " + artifact.toString(), e );
        }

        return requestedSourceBundle;
    }

    protected Collection<Artifact> getCompiledResouceBundles()
    {
        if ( this.getLocale() == null )
        {
            return null;
        }

        final Collection<Artifact> rbsSwc = new LinkedHashSet<Artifact>();

        final MavenProject project = session.getCurrentProject();
        final Set<Artifact> deps = project.getArtifacts();

        final Set<Artifact> beacons = new LinkedHashSet<Artifact>( filter( type( RB_SWC ), deps ) );

        String[] localeChains = getLocale();

        // TODO for for for for if for for, too many nested blocks, improve this
        for ( Artifact beacon : beacons )
        {
            for ( String localeChain : localeChains )
            {
                String[] locales;
                if ( localeChain.contains( "," ) )
                {
                    locales = localeChain.split( "," );
                }
                else
                {
                    locales = new String[] { localeChain };
                }

                String requestedLocale = locales[0];

                Artifact requestedRbSwc = null;
                try
                {
                    final IMaven maven = MavenPlugin.getMaven();

                    requestedRbSwc =
                        maven.resolve( beacon.getGroupId(), beacon.getArtifactId(), beacon.getVersion(),
                                       beacon.getType(), requestedLocale, maven.getArtifactRepositories(), monitor );
                }
                catch ( CoreException e )
                {
                    if ( log.isErrorEnabled() )
                        log.error( "Error resolving resource bundle for beacon " + beacon.toString(), e );
                }

                Artifact resultRbSwc = null;
                if ( requestedRbSwc != null && requestedRbSwc.isResolved() )
                {
                    resultRbSwc = requestedRbSwc;
                }
                else if ( locales.length > 1 )
                {
                    try
                    {
                        resultRbSwc = doLocalizationChain( locales, requestedLocale, beacon, requestedRbSwc );
                    }
                    catch ( CoreException e )
                    {
                        if ( log.isErrorEnabled() )
                            log.error( "Error resolving resource bundle for locales=" + locales
                                + " with requested locale=" + requestedLocale + " and beacon=" + beacon.getId()
                                + " for requested bundle " + requestedRbSwc.getId(), e );
                    }
                }
                else
                {
                    if ( log.isErrorEnabled() )
                        log.error( "Missing resource bundle '" + beacon.getId() + "'" );
                }

                rbsSwc.add( resultRbSwc );
            }
        }

        return rbsSwc;
    }

    private Artifact doLocalizationChain( String[] locales, String requestedLocale, Artifact beacon,
                                          Artifact requestRbSwc )
        throws CoreException
    {
        for ( String locale : locales )
        {
            try
            {
                return resolve( beacon.getGroupId(), beacon.getArtifactId(), beacon.getVersion(), locale,
                                beacon.getType() );
            }
            catch ( CoreException e )
            {
                throw new CoreException( new Status( IStatus.ERROR, IFlexMojosConstants.PLUGIN_ID,
                                                     "Unable to resolve resource bundle '" + beacon + "' for '"
                                                         + requestedLocale + "'" ) );
            }
        }

        return null;
    }

    protected Artifact resolve( final String groupId, final String artifactId, final String version,
                                final String classifier, final String type )
        throws CoreException
    {
        final IMaven maven = MavenPlugin.getMaven();

        return maven.resolve( groupId, artifactId, version, classifier, type, maven.getArtifactRepositories(), monitor );
    }

    public String getMainSourcePath()
    {
        return this.session.getCurrentProject().getBuild().getSourceDirectory();
    }

    public Module[] getModules()
    {
        return getParameterValue( MODULES, Module[].class );
    }

    public File resolveSourceFile( String path )
    {
        return SourceFileResolver.resolveSourceFile( session.getCurrentProject().getCompileSourceRoots(), path );
    }

    public String getFinalName()
    {
        final MavenProject project = session.getCurrentProject();
        return project.getBuild().getFinalName();
    }

    public String getProjectName()
    {
        return session.getCurrentProject().getArtifactId();
    }

    // IMojoAdapter
    public Method[] getConsumedProperties()
    {
        return consumedProperties.toArray( new Method[0] );
    }

    public Method[] getUnconsumedProperties()
    {
        List<Method> up = new ArrayList<Method>();
        for ( Method method : properties )
        {
            if ( !consumedProperties.contains( method ) )
                up.add( method );
        }
        return up.toArray( new Method[0] );
    }

    public void setPropertyConsumed( Method property )
    {
        consumedProperties.add( property );
    }

    protected void addProperties( Class<?> type )
    {
        Method[] methods = type.getMethods();
        for ( int i = 0; i < methods.length; i++ )
        {
            properties.add( methods[i] );
        }
    }

    protected String getPropertyName( Method m )
    {
        return m.getName().substring( 3 );
    }

    public File writeConfig()
    {
        // TODO write compiler config
        return null;
    }
}
