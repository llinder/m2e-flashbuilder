package net.flexmojos.eclipse.m2e.encrypted.adapters;

import net.flexmojos.eclipse.m2e.adapters.MojoAdapter;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.PluginExecution;
import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.embedder.IMaven;

public class AbstractMojoAdapter implements MojoAdapter
{
    private static final String TARGET_PLAYER = "targetPlayer";

    protected final IProgressMonitor monitor;

    protected final MavenSession session;

    protected final MojoExecution execution;

    IMaven maven = MavenPlugin.getMaven();

    public AbstractMojoAdapter( final IProgressMonitor monitor, final MavenSession session, final MojoExecution execution )
    {
        this.monitor = monitor;
        this.session = session;
        this.execution = execution;
    }

    public String getTargetPlayer()
    {
        return getParameterValue( TARGET_PLAYER, String.class );
    }

    protected <T> T getParameterValue( String parameter, Class<T> asType )
    {
        PluginExecution pluginExecution = new PluginExecution();
        pluginExecution.setConfiguration( execution.getConfiguration() );
        
        try
        {
            return maven.getMojoParameterValue( parameter, asType, session, execution.getPlugin(), pluginExecution,
                                            execution.getGoal() );
        }
        catch( CoreException e ){}
        
        return null;
    }
}
