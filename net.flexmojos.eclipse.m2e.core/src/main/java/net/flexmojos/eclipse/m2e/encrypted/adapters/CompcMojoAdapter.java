package net.flexmojos.eclipse.m2e.encrypted.adapters;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.FileSet;
import org.apache.maven.model.PatternSet;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecution;
import org.codehaus.plexus.util.DirectoryScanner;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.compiler.ICompcConfiguration;
import org.sonatype.flexmojos.compiler.IIncludeFile;
import org.sonatype.flexmojos.compiler.IIncludeStylesheet;
import org.sonatype.flexmojos.compiler.INamespace;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenIncludeStylesheet;
import org.sonatype.flexmojos.plugin.compiler.attributes.SimplifiablePattern;
import org.sonatype.flexmojos.util.PathUtil;

public class CompcMojoAdapter
    extends MxmlcMojoAdapter
    implements ICompcConfiguration
{
    private static final Logger log =
        LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.internal.adapters.CompcMojoAdapter" );
    
    private static final String INCLUDE_LOOKUP_ONLY = "includeLookupOnly";

    private static final String INCLUDE_CLASSES = "includeClasses";

    private static final String DIRECTORY = "directory";

    private static final String COMPUTE_DIGEST = "computeDigest";

    private static final String INCLUDE_STYLESHEETS = "includeStylesheets";

    private static final String ROOT = "root";

    private static final String INCLUDE_SOURCES = "includeSources";

    private static final String INCLUDE_NAMESPACES = "includeNamespaces";

    private static final String INCLUDE_ALL_NAMESPACES = "includeAllNamespaces";

    private static final String INCLUDE_FILES = "includeFiles";

    private static final String RESOURCES = "resources";

    public CompcMojoAdapter( final IProgressMonitor monitor, final MavenSession session, final MojoExecution config )
    {
        super( monitor, session, config );

        addProperties( ICompcConfiguration.class );
    }

    public Boolean getComputeDigest()
    {
        return getParameterValue( COMPUTE_DIGEST, Boolean.class );
    }

    public Boolean getDirectory()
    {
        return getParameterValue( DIRECTORY, Boolean.class );
    }

    public List<String> getIncludeClasses()
    {
        final SimplifiablePattern includeClasses = getParameterValue( INCLUDE_CLASSES, SimplifiablePattern.class );

        if ( includeClasses == null )
        {
            return null;
        }

        List<String> classes = new ArrayList<String>();

        classes.addAll( includeClasses.getIncludes() );
        classes.addAll( filterClasses( includeClasses.getPatterns(), getSourcePath() ) );

        return classes;
    }

    @SuppressWarnings( "unchecked" )
    public IIncludeFile[] getIncludeFile()
    {
        final SimplifiablePattern includeFiles = getParameterValue( INCLUDE_FILES, SimplifiablePattern.class );
        final List<String> includeNamespaces = getParameterValue( INCLUDE_NAMESPACES, List.class );
        final File[] includeSources = getParameterValue( INCLUDE_SOURCES, File[].class );
        final SimplifiablePattern includeClasses = getParameterValue( INCLUDE_CLASSES, SimplifiablePattern.class );
        final List<Resource> resources = getParameterValue( RESOURCES, List.class );

        List<IIncludeFile> files = new ArrayList<IIncludeFile>();

        List<FileSet> patterns = new ArrayList<FileSet>();
        if ( includeFiles == null && includeNamespaces == null && includeSources == null && includeClasses == null )
        {
            patterns.addAll( resources );
        }
        else if ( includeFiles == null )
        {
            return null;
        }
        else
        {
            // process patterns
            patterns.addAll( includeFiles.getPatterns() );

            // process files
            for ( final String path : includeFiles.getIncludes() )
            {
                final File file = PathUtil.file( path, getResourcesTargetDirectories() );
                
                if( file == null )
                {
                    if( log.isErrorEnabled() )
                        log.error( "Skipping addition of non existant resource '"+path+"' to file includes." );
                    
                    // TODO add problem marker
                    
                    continue;
                }

                files.add( new IIncludeFile()
                {
                    public String name()
                    {
                        return path.replace( '\\', '/' );
                    }

                    public String path()
                    {
                        return file.getAbsolutePath();
                    }
                } );
            }
        }

        for ( FileSet pattern : patterns )
        {
            final DirectoryScanner scan = scan( pattern );
            if ( scan == null )
            {
                continue;
            }

            for ( final String file : scan.getIncludedFiles() )
            {
                files.add( new IIncludeFile()
                {
                    public String name()
                    {
                        return file.replace( '\\', '/' );
                    }

                    public String path()
                    {
                        return PathUtil.file( file, scan.getBasedir() ).getAbsolutePath();
                    }
                } );
            }
        }

        return files.toArray( new IIncludeFile[0] );
    }

    public Boolean getIncludeLookupOnly()
    {
        return getParameterValue( INCLUDE_LOOKUP_ONLY, Boolean.class );
    }

    @SuppressWarnings( "unchecked" )
    public List<String> getIncludeNamespaces()
    {
        final List<String> includeNamespaces = getParameterValue( INCLUDE_NAMESPACES, List.class );

        if ( includeNamespaces != null )
        {
            return includeNamespaces;
        }

        final Boolean includeAllNamespaces = getParameterValue( INCLUDE_ALL_NAMESPACES, Boolean.class );

        if ( includeAllNamespaces )
        {
            return getNamespacesUri();
        }

        return null;
    }

    public File[] getIncludeSources()
    {
        final File[] includeSources = getParameterValue( INCLUDE_SOURCES, File[].class );

        if ( getIncludeFile() == null && getIncludeNamespaces() == null && includeSources == null
            && getIncludeClasses() == null )
        {
            return getSourcePath();
        }
        return includeSources;
    }

    public IIncludeStylesheet[] getIncludeStylesheet()
    {
        final IIncludeStylesheet[] includeStylesheets =
            getParameterValue( INCLUDE_STYLESHEETS, IIncludeStylesheet[].class );

        if ( includeStylesheets == null )
        {
            return null;
        }

        IIncludeStylesheet[] is = new IIncludeStylesheet[includeStylesheets.length];
        for ( int i = 0; i < includeStylesheets.length; i++ )
        {
            final MavenIncludeStylesheet ss = (MavenIncludeStylesheet) includeStylesheets[i];
            is[i] = new IIncludeStylesheet()
            {
                public String name()
                {
                    if ( ss.getName() != null )
                    {
                        return ss.getName();
                    }

                    return PathUtil.file( ss.getPath(), getResourcesTargetDirectories() ).getName();
                }

                public String path()
                {
                    return PathUtil.file( ss.getPath(), getResourcesTargetDirectories() ).getAbsolutePath();
                }
            };
        }

        return is;
    }

    public String getRoot()
    {
        return getParameterValue( ROOT, String.class );
    }

    // -----------------------
    // End ICompcConfiguration
    // -----------------------

    @Override
    public String[] getLocale()
    {
        String[] locale = super.getLocale();
        if ( locale != null )
        {
            return locale;
        }

        return new String[] {};
    }

    protected List<String> filterClasses( List<FileSet> classesPattern, File[] directories )
    {
        directories = PathUtil.existingFiles( directories );

        Set<String> includedFiles = new LinkedHashSet<String>();
        for ( FileSet pattern : classesPattern )
        {
            pattern.setIncludes( toFilePattern( pattern.getIncludes() ) );
            pattern.setExcludes( toFilePattern( pattern.getExcludes() ) );

            if ( pattern.getDirectory() == null )
            {
                for ( File dir : directories )
                {
                    includedFiles.addAll( Arrays.asList( scan( pattern, dir ).getIncludedFiles() ) );
                }
            }
            else
            {
                File dir = PathUtil.file( pattern.getDirectory(), getBasedir() );
                if ( !ArrayUtils.contains( directories, dir ) )
                {
                    throw new IllegalArgumentException( "Pattern does point to an invalid source directory: "
                        + dir.getAbsolutePath() );
                }

                includedFiles.addAll( Arrays.asList( scan( pattern, dir ).getIncludedFiles() ) );
            }
        }

        List<String> classes = new ArrayList<String>();
        for ( String filename : includedFiles )
        {
            String classname = toClass( filename );
            classes.add( classname );
        }

        return classes;
    }

    protected String toClass( String filename )
    {
        String classname = filename;
        classname = classname.replaceAll( "\\.(.)*", "" );
        classname = classname.replace( '\\', '.' );
        classname = classname.replace( '/', '.' );
        return classname;
    }

    private List<String> toFilePattern( List<String> classesIncludes )
    {
        List<String> fileIncludes = new ArrayList<String>();
        for ( String classInclude : classesIncludes )
        {
            fileIncludes.add( classInclude.replace( '.', File.separatorChar ) + ".as" );
            fileIncludes.add( classInclude.replace( '.', File.separatorChar ) + ".fxg" );
            fileIncludes.add( classInclude.replace( '.', File.separatorChar ) + ".mxml" );
        }
        return fileIncludes;
    }

    protected DirectoryScanner scan( FileSet pattern )
    {
        return scan( pattern, PathUtil.file( pattern.getDirectory(), getBasedir() ) );
    }

    protected DirectoryScanner scan( PatternSet pattern, File directory )
    {
        if ( !directory.exists() )
        {
            return null;
        }

        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setBasedir( directory );
        if ( !pattern.getIncludes().isEmpty() )
        {
            scanner.setIncludes( (String[]) pattern.getIncludes().toArray( new String[0] ) );
        }
        if ( !pattern.getExcludes().isEmpty() )
        {
            scanner.setExcludes( (String[]) pattern.getExcludes().toArray( new String[0] ) );
        }
        scanner.addDefaultExcludes();
        scanner.scan();
        return scanner;
    }

    protected DirectoryScanner scan( Resource resource )
    {
        File dir;
        if ( resource.getTargetPath() != null )
        {
            dir = PathUtil.file( resource.getTargetPath(), getBasedir() );
        }
        else
        {
            dir = PathUtil.file( resource.getDirectory(), getBasedir() );
        }

        return scan( resource, dir );
    }

    protected File getBasedir()
    {
        return session.getCurrentProject().getBasedir();
    }

    protected List<String> getNamespacesUri()
    {
        final INamespace[] namespaces = getParameterValue( NAMESPACES, INamespace[].class );

        if ( namespaces == null || namespaces.length == 0 )
        {
            return null;
        }

        List<String> uris = new ArrayList<String>();
        for ( INamespace namespace : namespaces )
        {
            uris.add( namespace.uri() );
        }

        return uris;
    }
}
