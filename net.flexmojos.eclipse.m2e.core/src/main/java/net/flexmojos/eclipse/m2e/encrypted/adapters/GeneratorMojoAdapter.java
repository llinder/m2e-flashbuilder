package net.flexmojos.eclipse.m2e.encrypted.adapters;

import java.io.File;
import java.util.Map;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.IProgressMonitor;
import org.sonatype.flexmojos.compiler.extra.IGeneratorConfiguration;


public class GeneratorMojoAdapter
    extends AbstractMojoAdapter
    implements IGeneratorConfiguration
{
    private static final String INCLUDE_JAVA_CLASSES = "includeJavaClasses";

    private static final String EXCLUDE_JAVA_CLASSES = "excludeJavaClasses";

    private static final String OUTPUT_DIRECTORY = "outputDirectory";

    private static final String BASE_OUTPUT_DIRECTORY = "baseOutputDirectory";

    private static final String GENERATOR_TO_USE = "generatorToUse";

    private static final String EXTRA_OPTIONS = "extraOptions";

    private static final String TEMPLATES = "templates";

    private static final String TRANSLATORS = "translators";

    public GeneratorMojoAdapter( IProgressMonitor monitor, MavenSession session, MojoExecution execution )
    {
        super( monitor, session, execution );
    }

    public String[] getIncludeJavaClasses()
    {
        return getParameterValue( INCLUDE_JAVA_CLASSES, String[].class );
    }

    public String[] getExcludeJavaClasses()
    {
        return getParameterValue( EXCLUDE_JAVA_CLASSES, String[].class );
    }

    public File getOutputDirectory()
    {
        return getParameterValue( OUTPUT_DIRECTORY, File.class );
    }

    public File getBaseOutputDirectory()
    {
        return getParameterValue( BASE_OUTPUT_DIRECTORY, File.class );
    }

    public String getGeneratorToUse()
    {
        return getParameterValue( GENERATOR_TO_USE, String.class );
    }

    @SuppressWarnings( "unchecked" )
    public Map<String, String> getExtraOptions()
    {
        return getParameterValue( EXTRA_OPTIONS, Map.class );
    }

    @SuppressWarnings( "unchecked" )
    public Map<String, String> getTemplates()
    {
        return getParameterValue( TEMPLATES, Map.class );
    }

    public String[] getTranslators()
    {
        return getParameterValue( TRANSLATORS, String[].class );
    }

}
