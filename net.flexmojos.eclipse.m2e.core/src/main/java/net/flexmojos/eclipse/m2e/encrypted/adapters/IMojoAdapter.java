package net.flexmojos.eclipse.m2e.encrypted.adapters;

import java.io.File;
import java.lang.reflect.Method;

public interface IMojoAdapter
{
	public Method[] getConsumedProperties();
	public Method[] getUnconsumedProperties();
	public void setPropertyConsumed( Method property );
	public File writeConfig();
}
