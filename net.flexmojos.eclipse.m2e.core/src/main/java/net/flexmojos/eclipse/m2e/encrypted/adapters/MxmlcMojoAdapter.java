package net.flexmojos.eclipse.m2e.encrypted.adapters;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.not;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.classifier;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.scope;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.type;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.SWF;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.SWZ;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.XML;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.CACHING;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.RSL;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuildingException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.compatibilitykit.FlexCompatibility;
import org.sonatype.flexmojos.compiler.IApplicationDomain;
import org.sonatype.flexmojos.compiler.ICommandLineConfiguration;
import org.sonatype.flexmojos.compiler.ICompilerConfiguration;
import org.sonatype.flexmojos.compiler.IDefaultScriptLimits;
import org.sonatype.flexmojos.compiler.IDefaultSize;
import org.sonatype.flexmojos.compiler.IFrame;
import org.sonatype.flexmojos.compiler.IFramesConfiguration;
import org.sonatype.flexmojos.compiler.ILicense;
import org.sonatype.flexmojos.compiler.ILicensesConfiguration;
import org.sonatype.flexmojos.compiler.ILocalizedDescription;
import org.sonatype.flexmojos.compiler.ILocalizedTitle;
import org.sonatype.flexmojos.compiler.IMetadataConfiguration;
import org.sonatype.flexmojos.compiler.IMxmlConfiguration;
import org.sonatype.flexmojos.compiler.INamespace;
import org.sonatype.flexmojos.compiler.INamespacesConfiguration;
import org.sonatype.flexmojos.compiler.IRuntimeSharedLibraryPath;
import org.sonatype.flexmojos.compiler.IRuntimeSharedLibrarySettingsConfiguration;
import org.sonatype.flexmojos.compiler.extra.IRuntimeSharedLibraryPathExtended;
import org.sonatype.flexmojos.plugin.common.FlexExtension;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenArtifact;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenMetadataConfiguration;
import org.sonatype.flexmojos.plugin.compiler.attributes.MavenNamespace;
import org.sonatype.flexmojos.plugin.utilities.ConfigurationResolver;
import org.sonatype.flexmojos.plugin.utilities.MavenUtils;
import org.sonatype.flexmojos.plugin.utilities.SourceFileResolver;
import org.sonatype.flexmojos.util.PathUtil;


public class MxmlcMojoAdapter
    extends AbstractCompilerMojoAdapter
    implements ICommandLineConfiguration, IMxmlConfiguration, INamespacesConfiguration, IFramesConfiguration,
    ILicensesConfiguration, IMetadataConfiguration, IRuntimeSharedLibrarySettingsConfiguration
{
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.internal.adapters.MxmlcMojoAdapter" );
    
    protected static final String[] DEFAULT_RSL_URLS =
        new String[] { "/{contextRoot}/rsl/{artifactId}-{version}.{extension}" };

    private static final String BENCHMARK = "benchmark";

    private static final String BENCHMARK_COMPILER_DETAILS = "benchmarkCompilerDetails";

    private static final String BENCHMARK_TIME_FILTER = "benchmarkTimeFilter";

    private static final String DEBUG_PASSWORD = "debugPassword";

    private static final String DEFAULT_BACKGROUND_COLOR = "defaultBackgroundColor";

    private static final String DEFAULT_FRAME_RATE = "defaultFrameRate";

    private static final String DEFAULT_SCRIPT_LIMITS = "defaultScriptLimits";

    private static final String DEFAULT_SIZE = "defaultSize";

    private static final String DUMP_CONFIG = "dumpConfig";

    private static final String EXTERNS = "externs";

    private static final String FILE_SPECS = "fileSpecs";

    private static final String FRAMEWORK = "framework";

    private static final String GENERATE_FRAME_LOADER = "generateFrameLoader";

    private static final String INCLUDE_INHERITANCE_DEPENDENCIES_ONLY = "includeInheritanceDependenciesOnly";

    private static final String INCLUDE_RESOURCE_BUNDLES = "includeResourceBundles";

    private static final String INCLUDES = "includes";

    private static final String LAZY_INIT = "lazyInit";

    private static final String LOAD_CONFIG = "loadConfig";

    private static final String LOAD_CONFIGS = "loadConfigs";

    private static final String CONFIG_DIRECTORY = "configDirectory";

    private static final String LOAD_EXTERNS = "loadExterns";

    private static final String PROJECTOR = "projector";

    private static final String RAW_METADATA = "rawMetadata";

    private static final String REMOVE_UNUSED_RSLS = "removeUnusedRsls";

    private static final String STATIC_LINK_RUNTIME_SHARED_LIBRARIES = "staticLinkRuntimeSharedLibraries";

    private static final String SWC_CHECKSUM = "swcChecksum";

    private static final String SWF_VERSION = "swfVersion";

    private static final String TOOLS_LOCALE = "toolsLocale";

    private static final String USE_DIRECT_BLIT = "useDirectBlit";

    private static final String USE_GPU = "useGpu";

    private static final String USE_NETWORK = "useNetwork";

    private static final String VERIFY_DIGESTS = "verifyDigests";

    private static final String SHOW_WARNINGS = "showWarnings";

    private static final String RESOURCE_BUNDLE_LIST = "resourceBundleList";

    private static final String DEFAULT_RESOURCE_BUNDLE_LIST = "defaultResourceBundleList";

    private static final String RSL_URLS = "rslUrls";

    private static final String POLICY_FILE_URLS = "policyFileUrls";

    // private static final String LICENSE_LOCAL_LOOKUP = "licenseLocalLookup";
    // private static final String LICENSES = "licenses";
    private static final String FRAMES = "frames";

    private static final String APPLICATION_DOMAIN = "applicationDomain";

    private static final String FORCE_RSLS = "forceRsls";

    private static final String METADATA = "metadata";

    private static final String SOURCE_FILE = "sourceFile";

    private static final String WRAPPER_ARTIFACT = "wrapperArtifact";

    public MxmlcMojoAdapter( final IProgressMonitor monitor, final MavenSession session, final MojoExecution config )
    {
        super( monitor, session, config );

        addProperties( ICommandLineConfiguration.class );
        addProperties( IFramesConfiguration.class );
        addProperties( ILicensesConfiguration.class );
        addProperties( IMetadataConfiguration.class );
        addProperties( IRuntimeSharedLibrarySettingsConfiguration.class );
    }

    // --------------------------------
    // Start ICommandLineConfiguration
    // --------------------------------
    public Boolean getBenchmark()
    {
        return getParameterValue( BENCHMARK, Boolean.class );
    }

    public Integer getBenchmarkCompilerDetails()
    {
        return getParameterValue( BENCHMARK_COMPILER_DETAILS, Integer.class );
    }

    public Long getBenchmarkTimeFilter()
    {
        return getParameterValue( BENCHMARK_TIME_FILTER, Long.class );
    }

    public ICompilerConfiguration getCompilerConfiguration()
    {
        return this;
    }

    public String getDebugPassword()
    {
        return getParameterValue( DEBUG_PASSWORD, String.class );
    }

    public Integer getDefaultBackgroundColor()
    {
        return getParameterValue( DEFAULT_BACKGROUND_COLOR, Integer.class );
    }

    public Integer getDefaultFrameRate()
    {
        return getParameterValue( DEFAULT_FRAME_RATE, Integer.class );
    }

    public IDefaultScriptLimits getDefaultScriptLimits()
    {
        return getParameterValue( DEFAULT_SCRIPT_LIMITS, IDefaultScriptLimits.class );
    }

    public IDefaultSize getDefaultSize()
    {
        return getParameterValue( DEFAULT_SIZE, IDefaultSize.class );
    }

    public String getDumpConfig()
    {
        return getParameterValue( DUMP_CONFIG, String.class );
    }

    public List<String> getExterns()
    {
        final String[] externs = getParameterValue( EXTERNS, String[].class );

        if ( externs == null )
        {
            return null;
        }

        return Arrays.asList( externs );
    }

    @SuppressWarnings( "unchecked" )
    public List<String> getFileSpecs()
    {
        return getParameterValue( FILE_SPECS, List.class );
    }

    public IFramesConfiguration getFramesConfiguration()
    {
        return this;
    }

    public String getFramework()
    {
        return getParameterValue( FRAMEWORK, String.class );
    }

    public Boolean getGenerateFrameLoader()
    {
        return getParameterValue( GENERATE_FRAME_LOADER, Boolean.class );
    }

    public String[] getHelp()
    {
        // must return null, otherwise will prevent compiler execution
        return null;
    }

    public Boolean getIncludeInheritanceDependenciesOnly()
    {
        return getParameterValue( INCLUDE_INHERITANCE_DEPENDENCIES_ONLY, Boolean.class );
    }

    @SuppressWarnings( "unchecked" )
    public List<String> getIncludeResourceBundles()
    {
        return getParameterValue( INCLUDE_RESOURCE_BUNDLES, List.class );
    }

    public List<String> getIncludes()
    {
        String[] includes = getParameterValue( INCLUDES, String[].class );

        if ( includes == null )
            return null;

        return Arrays.asList( includes );
    }

    public Boolean getLazyInit()
    {
        return getParameterValue( LAZY_INIT, Boolean.class );
    }

    public ILicensesConfiguration getLicensesConfiguration()
    {
        return this;
    }

    public String getLinkReport()
    {
        File linkReport = new File( getTargetDirectory(), getFinalName() + "-" + LINK_REPORT + "." + XML );

        return PathUtil.path( linkReport );
    }

    public String[] getLoadConfig()
    {
        final File loadConfig = getParameterValue( LOAD_CONFIG, File.class );
        final File[] loadConfigs = getParameterValue( LOAD_CONFIGS, File[].class );
        File configDirectory = getParameterValue( CONFIG_DIRECTORY, File.class );
        if ( configDirectory == null )
        {
            // Default to "${basedir}/src/main/config"
            IPath configDirectoryPath = new Path( PathUtil.path( session.getCurrentProject().getBasedir() ) );
            configDirectoryPath = configDirectoryPath.append( "src" ).append( "main" ).append( "config" );
            configDirectory = configDirectoryPath.toFile();
        }

        return PathUtil.paths( ConfigurationResolver.resolveConfiguration( loadConfigs, loadConfig, configDirectory ) );
    }

    @SuppressWarnings( "unchecked" )
    public String[] getLoadExterns()
    {
        Collection<Artifact> artifacts = new LinkedHashSet<Artifact>();

        Set<Artifact> dependencies = getDependencies( classifier( LINK_REPORT ), type( XML ) );
        if ( !dependencies.isEmpty() )
        {
            artifacts.addAll( dependencies );
        }

        final MavenArtifact[] loadExterns = getParameterValue( LOAD_EXTERNS, MavenArtifact[].class );

        if ( loadExterns != null )
        {
            for ( MavenArtifact loadExtern : loadExterns )
            {
                try
                {
                    final Artifact resolvedArtifact =
                        resolve( loadExtern.getGroupId(), loadExtern.getArtifactId(), loadExtern.getVersion(),
                                 LINK_REPORT, XML );
                    artifacts.add( resolvedArtifact );
                }
                catch ( CoreException e )
                {
                    if( log.isErrorEnabled() )
                        log.error( "Error loading externs", e );
                }
            }

        }

        if ( artifacts.isEmpty() )
        {
            return null;
        }

        return PathUtil.paths( MavenUtils.getFilesSet( artifacts ) );
    }

    public IMetadataConfiguration getMetadataConfiguration()
    {
        return getParameterValue( METADATA, MavenMetadataConfiguration.class );
    }

    public String getOutput()
    {
        final File output = getCompilerOutput();
        return PathUtil.path( output );
    }

    public String getProjector()
    {
        return getParameterValue( PROJECTOR, String.class );
    }

    public String getRawMetadata()
    {
        return getParameterValue( RAW_METADATA, String.class );
    }

    public Boolean getRemoveUnusedRsls()
    {
        return getParameterValue( REMOVE_UNUSED_RSLS, Boolean.class );
    }

    public String getResourceBundleList()
    {
        return PathUtil.path( getResourceBundleListFile() );
    }

    public String[] getRuntimeSharedLibraries()
    {
        // TODO should this be null?
        // flexmojos currently has it as null but maybe this will change.
        return null;
    }

    public IRuntimeSharedLibraryPath[] getRuntimeSharedLibraryPath()
    {
        return getRuntimeSharedLibraryPathExtended();
    }

    @SuppressWarnings( "unchecked" )
    public IRuntimeSharedLibraryPathExtended[] getRuntimeSharedLibraryPathExtended()
    {
        // get all the rsl dependencies
        Set<Artifact> dependencies = getDependencies( not( GLOBAL_MATCHER ),//
                                                      anyOf( scope( RSL ), scope( CACHING ) ) );

        if ( dependencies.isEmpty() )
        {
            return null;
        }

        final String[] rslUrls = getRslUrls();
        final String[] policyFileUrls = getPolicyFileUrls();

        // not sure if all this validation are required
        if ( rslUrls.length < policyFileUrls.length //
            && policyFileUrls.length != 0 //
            && rslUrls.length != policyFileUrls.length //
            && rslUrls.length != policyFileUrls.length - 1 )
        {
            if( log.isErrorEnabled() )
                log.error( "The number of elements on RSL Urls and Policy File Urls doesn't match: "
                                          + rslUrls.length + "/" + rslUrls.length );
        }

        List<IRuntimeSharedLibraryPathExtended> rsls = new ArrayList<IRuntimeSharedLibraryPathExtended>();
        for ( final Artifact artifact : dependencies )
        {

            rsls.add( new IRuntimeSharedLibraryPathExtended()
            {
                public String pathElement()
                {
                    return artifact.getFile().getAbsolutePath();
                }

                public Map<String, String> rslUrl()
                {
                    return calculateRuntimeLibraryPath( artifact, rslUrls, policyFileUrls );
                }

                public Artifact getArtifact()
                {
                    return artifact;
                }

                public boolean isCrossdomainRsl()
                {
                    return ( policyFileUrls != null && policyFileUrls.length > 0 );
                }

                public String[] getPolicyFileUrls()
                {
                    return policyFileUrls;
                }

            } );
        }

        return rsls.toArray( new IRuntimeSharedLibraryPathExtended[rsls.size()] );
    }

    public IRuntimeSharedLibrarySettingsConfiguration getRuntimeSharedLibrarySettingsConfiguration()
    {
        return this;
    }

    @FlexCompatibility( minVersion = "4.5.0" )
    public String getSizeReport()
    {
        final File sizeReport = new File( getTargetDirectory(), getFinalName() + "-" + SIZE_REPORT + "." + XML );
        return PathUtil.path( sizeReport );
    }

    public Boolean getStaticLinkRuntimeSharedLibraries()
    {
        return getParameterValue( STATIC_LINK_RUNTIME_SHARED_LIBRARIES, Boolean.class );
    }

    public Boolean getSwcChecksum()
    {
        return getParameterValue( SWC_CHECKSUM, Boolean.class );
    }

    public Integer getSwfVersion()
    {
        return getParameterValue( SWF_VERSION, Integer.class );
    }

    @FlexCompatibility( minVersion = "4.0.0.13007" )
    public String getToolsLocale()
    {
        final String toolsLocale = getParameterValue( TOOLS_LOCALE, String.class );

        if ( toolsLocale == null )
        {
            if( log.isErrorEnabled() )
                log.error( "Invalid toolsLocale it must be not null and must be in Java format."
                                          + "  For example, \"en\" or \"ja_JP\"" );
        }

        return toolsLocale;
    }

    public Boolean getUseDirectBlit()
    {
        return getParameterValue( USE_DIRECT_BLIT, Boolean.class );
    }

    public Boolean getUseGpu()
    {
        return getParameterValue( USE_GPU, Boolean.class );
    }

    public Boolean getUseNetwork()
    {
        return getParameterValue( USE_NETWORK, Boolean.class );
    }

    public Boolean getVerifyDigests()
    {
        return getParameterValue( VERIFY_DIGESTS, Boolean.class );
    }

    public Boolean getVersion()
    {
        // must return null, otherwise will prevent compiler execution
        return null;
    }

    public Boolean getWarnings()
    {
        return getParameterValue( SHOW_WARNINGS, Boolean.class );
    }

    // -------------------------------
    // End ICommandLineConfiguration
    // -------------------------------

    // --------------------------------
    // Start IMxmlConfiguration
    // --------------------------------
    /**
     * Specifies a compatibility version
     * <p>
     * Equivalent to -compiler.mxml.compatibility-version
     * </p>
     * Default: null
     */
    public String getCompatibilityVersion()
    {
        return getParameterValue( AbstractCompilerMojoAdapter.COMPATIBILITY_VERSION, String.class );
    }

    /**
     * Minimum supported SDK version for this library. This string will always be of the form N.N.N. For example, if
     * -minimum-supported-version=2, this string is "2.0.0", not "2".
     * <p>
     * Equivalent to -compiler.mxml.minimum-supported-version
     * </p>
     * Default: null
     */
    public String getMinimumSupportedVersion()
    {
        return getParameterValue( AbstractCompilerMojoAdapter.MINIMUM_SUPPORTED_VERSION, String.class );
    }

    /**
     * DOCME undocumented by adobe
     * <p>
     * Equivalent to -compiler.mxml.qualified-type-selectors
     * </p>
     * Default: null
     */
    public Boolean getQualifiedTypeSelectors()
    {
        return getParameterValue( AbstractCompilerMojoAdapter.QUALIFIED_TYPE_SELECTORS, Boolean.class );
    }

    // ---------------------------------
    // End IMxmlConfiguration
    // ---------------------------------

    // --------------------------------
    // Start INamespacesConfiguration
    // --------------------------------
    /**
     * Specify a URI to associate with a manifest of components for use as MXML elements
     * <p>
     * Equivalent to -compiler.namespaces.namespace
     * </p>
     * Default: null
     */
    public INamespace[] getNamespace()
    {
        return getParameterValue( NAMESPACES, MavenNamespace[].class );
    }

    // --------------------------------
    // End INamespacesConfiguration
    // --------------------------------

    protected String getTargetDirectory()
    {
        final MavenProject project = session.getCurrentProject();
        return project.getBuild().getDirectory();
    }

    protected File getCompilerOutput()
    {
        File output = new File( getTargetDirectory(), getFinalName() + "." + getProjectType() );
        output.getParentFile().mkdirs();
        return output;
    }

    protected File getResourceBundleListFile()
    {
        final File resourceBundleList = getParameterValue( RESOURCE_BUNDLE_LIST, File.class );

        if ( resourceBundleList != null )
        {
            return resourceBundleList;
        }

        if ( getLocalesRuntime() == null )
        {
            return null;
        }

        File defaultResourceBundleList = getParameterValue( DEFAULT_RESOURCE_BUNDLE_LIST, File.class );
        // Set default location if not specified.
        if ( defaultResourceBundleList == null )
        {
            // "${project.build.directory}/${project.build.finalName}-rb.properties"
            IPath drbl = new Path( getTargetDirectory() ).append( getFinalName() + "-rb.properties" );
            defaultResourceBundleList = drbl.toFile();
        }

        defaultResourceBundleList.getParentFile().mkdirs();
        return defaultResourceBundleList;
    }

    protected String[] getRslUrls()
    {
        final String[] rslUrls = getParameterValue( RSL_URLS, String[].class );

        if ( rslUrls == null )
        {
            return DEFAULT_RSL_URLS;
        }
        return rslUrls;
    }

    public String[] getPolicyFileUrls()
    {
        final String[] policyFileUrls = getParameterValue( POLICY_FILE_URLS, String[].class );

        if ( policyFileUrls == null )
        {
            return new String[0];
        }
        return policyFileUrls;
    }

    protected Map<String, String> calculateRuntimeLibraryPath( Artifact artifact, String[] rslUrls,
                                                               String[] policyFileUrls )
    {
        // console.logMessage( "runtime libraries: id: " + artifact.getArtifactId() );

        String scope = artifact.getScope();
        final String extension;
        if ( CACHING.equals( scope ) )
        {
            extension = SWZ;
        }
        else
        {
            extension = SWF;
        }

        Map<String, String> paths = new LinkedHashMap<String, String>();
        for ( int i = 0; i < rslUrls.length; i++ )
        {
            String rsl = rslUrls[i];
            String policy;
            if ( i < policyFileUrls.length )
            {
                policy = policyFileUrls[i];
            }
            else
            {
                policy = null;
            }

            rsl = MavenUtils.interpolateRslUrl( rsl, artifact, extension, getContextRoot() );
            policy = MavenUtils.interpolateRslUrl( policy, artifact, extension, getContextRoot() );

            // console.logMessage( "RSL url: " + rsl + " - " + policy );
            paths.put( rsl, policy );
        }

        return paths;
    }

    // ---------------------------
    // Start ILicenseConfiguration
    // ---------------------------
    public ILicense[] getLicense()
    {
        // Shouldn't be necessary since we are working with Flashbuilder
        return null;
    }

    // ---------------------------
    // End ILicenseConfiguration
    // ---------------------------

    // ----------------------------
    // Start IFrameConfiguration
    // ----------------------------
    /**
     * A SWF frame label with a sequence of classnames that will be linked onto the frame
     * <p>
     * Equivalent to -frames.frame
     * </p>
     * Default: null
     */
    public IFrame[] getFrame()
    {
        return getParameterValue( FRAMES, IFrame[].class );
    }

    // ----------------------------
    // End IFrameConfiguration
    // ----------------------------

    // ---------------------------------
    // Start IRuntimeSharedLibrarySettingsConfiguration
    // ---------------------------------
    /**
     * Override the application domain an RSL is loaded into. The supported values are 'current', 'default', 'parent',
     * or 'top-level'
     * <p>
     * Equivalent to -runtime-shared-library-settings.application-domains
     * </p>
     * Default: null
     */
    public IApplicationDomain[] getApplicationDomain()
    {
        return getParameterValue( APPLICATION_DOMAIN, IApplicationDomain[].class );
    }

    /**
     * Force an RSL to be loaded, overriding the removal caused by using the remove-unused-rsls option.
     * <p>
     * Equivalent to -runtime-shared-library-settings.force-rsls
     * </p>
     * Default: null
     */
    public String[] getForceRsls()
    {
        return getParameterValue( FORCE_RSLS, String[].class );
    }

    // --------------------------------
    // End IRuntimeSharedLibrarySettingsConfiguration
    // --------------------------------

    // --------------------------------
    // Start IMetadataConfiguration
    // --------------------------------
    public String[] getContributor()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getContributor();
        }

        return null;
    }

    public String[] getCreator()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getCreator();
        }

        return null;
    }

    public String getDate()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getDate();
        }

        return null;
    }

    public String getDescription()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getDescription();
        }

        return null;
    }

    public String[] getLanguage()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getLanguage();
        }

        return null;
    }

    public ILocalizedDescription[] getLocalizedDescription()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getLocalizedDescription();
        }

        return null;
    }

    public ILocalizedTitle[] getLocalizedTitle()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getLocalizedTitle();
        }

        return null;
    }

    public String[] getPublisher()
    {
        if ( getMetadata() != null )
        {
            return getMetadata().getPublisher();
        }

        return getCreator();
    }

    public String getTitle()
    {
        if ( getMetadata() != null )
            return getMetadata().getTitle();

        return session.getCurrentProject().getName();
    }

    // --------------------------------
    // End IMetadataConfiguration
    // --------------------------------

    public String getSourceFile()
    {
        final String sourceFile = getParameterValue( SOURCE_FILE, String.class );
        
        final MavenProject project = session.getCurrentProject();
        
        try
        {
            final File file = SourceFileResolver.resolveSourceFile( project.getCompileSourceRoots(), sourceFile, project.getGroupId(), project.getArtifactId() );
            
            return file.getPath();
        }
        catch( Exception e )
        {
            // TODO add error marker
        }
        return null;
        
        // Not enough information here to decern if this should default to .mxml or .as. (Think SWF action script only project).
        //return ( file == null ) ? ( FlexExtension.SWF.equals( getProjectType() ) || FlexExtension.AIR.equals( getProjectType() ) ) ? "main.mxml" : "main.as" : file;
    }

    protected MavenMetadataConfiguration getMetadata()
    {
        return getParameterValue( METADATA, MavenMetadataConfiguration.class );
    }

    /**
     * Tries to construct project for the provided artifact
     * 
     * @param artifact
     * @return MavenProject for the given artifact
     * @throws ProjectBuildingException
     * @throws CoreException
     */
    /*
    private IProject loadProject( Artifact artifact )
        throws CoreException, ProjectBuildingException
    {

        final ProjectBuildingRequest request = session.getProjectBuildingRequest();
        final ArtifactRepository localRepository = session.getLocalRepository();
        final List<ArtifactRepository> remoteRepositories =
            MavenPlugin.getMaven().getArtifactRepositories();

        request.setLocalRepository( localRepository );
        request.setRemoteRepositories( remoteRepositories );
        request.setResolveDependencies( true );
        request.setRepositorySession( session.getRepositorySession() );

        IMavenProjectFacade projectFacade = MavenPlugin.getMavenProjectRegistry().getMavenProject( artifact.getGroupId(), artifact.getArtifactId(), artifact.getVersion() );
        return projectFacade.getProject();
        
        //final ProjectBuilder projectBuilder = lookup( ProjectBuilder.class );
        //return projectBuilder.build( artifact, request ).getProject();
    }
    */

	public Boolean getCompress() {
		// TODO Auto-generated method stub
		return null;
	}

}
