package net.flexmojos.eclipse.m2e.encrypted.adapters;

import java.io.File;
import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.IProgressMonitor;
import org.sonatype.flexmojos.compiler.extra.ISignAirConfiguration;


public class SignAirMojoAdapter extends AbstractMojoAdapter
    implements ISignAirConfiguration
{
    private static final String AIR_OUTPUT = "airOutput";
    
    private static final String CLASSIFIER = "classifier";
    
    private static final String DESCRIPTOR_TEMPLATE = "descriptorTemplate";
    
    private static final String INCLUDE_FILES = "includeFiles";
    
    private static final String INCLUDE_FILE_SETS = "includeFileSets";
    
    private static final String KEYSTORE = "keystore";
    
    private static final String STOREPASS = "storepass";
    
    private static final String STORETYPE = "storetype";
    
    private static final String STRIP_VERSION = "sriptVersion";
    
    private static final String TIMESTAMP_URL = "timestampURL";
    
    
    public SignAirMojoAdapter( IProgressMonitor monitor, MavenSession session, MojoExecution execution )
    {
        super( monitor, session, execution );
    }

    public File getAirOutput()
    {
        return getParameterValue( AIR_OUTPUT, File.class );
    }

    public String getClassifier()
    {
        return getParameterValue( CLASSIFIER, String.class );
    }

    public File getDescriptorTemplate()
    {
        return getParameterValue( DESCRIPTOR_TEMPLATE, File.class );
    }

    public Boolean getFlexBuilderCompatibility()
    {
        // TODO not really sure what the heck this is used for... need to investigate further.
        return true;
    }

    @SuppressWarnings( "unchecked" )
    public List<String> getIncludeFiles()
    {
        return getParameterValue( INCLUDE_FILES, List.class );
    }

    public FileSet[] getFileSets()
    {
        return getParameterValue( INCLUDE_FILE_SETS, FileSet[].class );
    }

    public File getKeystore()
    {
        return getParameterValue( KEYSTORE, File.class );
    }

    public String getStorepass()
    {
        return getParameterValue( STOREPASS, String.class );
    }

    public String getStoretype()
    {
        return getParameterValue( STORETYPE, String.class );
    }

    public Boolean getStripVersion()
    {
        return getParameterValue( STRIP_VERSION, Boolean.class );
    }

    public String getTimestampURL()
    {
        return getParameterValue( TIMESTAMP_URL, String.class );
    }

}
