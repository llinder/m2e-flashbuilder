package net.flexmojos.eclipse.m2e.encrypted.adapters;

import java.io.File;
import java.util.Map;

import net.flexmojos.eclipse.m2e.common.FMConstants;
import net.flexmojos.eclipse.m2e.common.FMEConstants;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.IProgressMonitor;
import org.sonatype.flexmojos.compiler.extra.IHtmlWrapperConfiguration;


public class WrapperMojoAdapter
    extends AbstractMojoAdapter
    implements IHtmlWrapperConfiguration
{
    private static final String PARAMETERS = "parameters";

    private static final String TEMPLATE_URI = "templateURI";
    
    private static final String TEMPLATE_OUTPUT_DIRECTORY = "templateOutputDirectory";

    public WrapperMojoAdapter( IProgressMonitor monitor, MavenSession session, MojoExecution execution )
    {
        super( monitor, session, execution );
    }

    // --------------------------------
    // Start IHtmlWrapperConfiguration
    // --------------------------------

    public String getTemplateURI()
    {
        String uri = getParameterValue( TEMPLATE_URI, String.class );
        if ( uri == null && isWrapperGoal() )
            uri = "embed:client-side-detection-with-history";

        return uri;
    }

    @SuppressWarnings( "unchecked" )
    public Map<String, String> getParameters()
    {
        return getParameterValue( PARAMETERS, Map.class );
    }

    public File getTemplateOutputDirectory()
    {
        return getParameterValue( TEMPLATE_OUTPUT_DIRECTORY, File.class );
    }

    // --------------------------------
    // End IHtmlWrapperConfiguration
    // --------------------------------
    
    private boolean isWrapperGoal()
    {
        return ( FMEConstants.GOAL_WRAPPER.equals( execution.getGoal() ) || FMConstants.GOAL_WRAPPER.equals( execution.getGoal() ) ); 
    }
}
