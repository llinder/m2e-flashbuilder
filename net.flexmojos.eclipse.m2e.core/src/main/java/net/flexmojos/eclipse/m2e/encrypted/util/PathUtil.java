package net.flexmojos.eclipse.m2e.encrypted.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.embedder.ArtifactKey;
import org.eclipse.m2e.core.project.IMavenProjectFacade;

public class PathUtil
{
	public static IProject getWorkspaceProjectForArtifact( ArtifactKey artifact )
	{
		final IMavenProjectFacade[] workspaceProjects = MavenPlugin.getMavenProjectRegistry().getProjects();
		
		// Check if this artifact is a managed eclipse maven project
		for ( IMavenProjectFacade workspaceProject : workspaceProjects )
		{
			final ArtifactKey wsKey = workspaceProject.getArtifactKey();

			if ( wsKey.equals( artifact ) )
				return workspaceProject.getProject();
		}
		
		return null;
	}
	
	public static String getRelativePath( File base, File path )
	{
		final List<String> baselist = getPathList( base );
		final List<String> pathlist = getPathList( path );
		return matchPathLists( baselist, pathlist );
	}
	
	/**
	 * Converts the given path to a relative path using the project
	 * location as the base path.
	 * 
	 * @param path
	 * @return
	 */
	public static IPath getRelativeProjectPath( final IProject project, final String path )
	{
		final File dirPath = new File( path );
		final String relDir = PathUtil.getRelativePath( project.getLocation().toFile(), dirPath );
		final IPath relPath = new Path( relDir );
		
		return relPath;
	}
	
	/**
	 * Converts the given path to a relative path using the main
	 * source directory as the base path.
	 * 
	 * @param path
	 * @return
	 */
	public static IPath getRelativeSourcePath( final MavenProject project, final String path )
	{
		final File dirPath = new File( path );
		final String relDir = PathUtil.getRelativePath( new File( project.getBuild().getSourceDirectory() ), dirPath );
		final IPath relPath = new Path( relDir );
		
		return relPath;
	}
	
	private static List<String> getPathList( final File path )
	{
		List<String> list = new ArrayList<String>();
		
        try
        {
            File canonicalPath = path.getCanonicalFile();
            while ( canonicalPath != null )
            {
                list.add( canonicalPath.getName() );
                canonicalPath = canonicalPath.getParentFile();
            }
        }
        catch ( IOException e )
        {
            // TODO log error
            list = null;
        }
        
        return list;
	}
	
	/**
	 * Evaluate path differences between the given full path and the relative path.
	 * 
	 * The resulting string will be a relative path using ../ notation.
	 * 
	 * @param base
	 * @param relative
	 * @return
	 */
	private static String matchPathLists( final List<String> full, final List<String> relative )
	{
		int i;
        int j;
        String s;
        // start at the beginning of the lists
        // iterate while both lists are equal
        s = "";
        i = full.size() - 1;
        j = relative.size() - 1;

        // first eliminate common root
        while ( ( i >= 0 ) && ( j >= 0 ) && ( full.get( i ).equals( relative.get( j ) ) ) )
        {
            i--;
            j--;
        }

        // for each remaining level in the home path, add a ..
        for ( ; i >= 0; i-- )
        {
            s += ".." + File.separator;
        }

        // for each level in the file path, add the path
        for ( ; j >= 1; j-- )
        {
            s += relative.get( j ) + File.separator;
        }

        // file name
        s += relative.get( j );
        return s;
	}
}
