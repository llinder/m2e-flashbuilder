package net.flexmojos.eclipse.m2e.encryption;

import net.flexmojos.eclipse.m2e.IFlexMojosPlugin;
import net.flexmojos.eclipse.m2e.encryption.internal.NitroClassLoaderException;

public interface IClassLoaderProvider
{
    public ClassLoader getClassLoader( IFlexMojosPlugin plugin ) throws NitroClassLoaderException;
}
