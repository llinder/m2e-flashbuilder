package net.flexmojos.eclipse.m2e.encryption.internal;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

public class NitroClassLoaderException
    extends CoreException
{
    private static final long serialVersionUID = 1L;
    
    public NitroClassLoaderException( IStatus status )
    {
        super( status );
    }
}
