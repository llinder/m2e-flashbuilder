package net.flexmojos.eclipse.m2e.encryption.internal;

import java.util.HashMap;
import java.util.Map;

import net.flexmojos.eclipse.m2e.IFlexMojosPlugin;
import net.flexmojos.eclipse.m2e.encryption.IClassLoaderProvider;

import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.internal.baseadaptor.DefaultClassLoader;

import com.simplifiedlogic.nitro.loaders.NitroClassLoader;

@SuppressWarnings( "restriction" )
public class NitroClassLoaderManager implements IClassLoaderProvider
{
    private final Map<String, NitroClassLoader> classloaders = new HashMap<String, NitroClassLoader>();
    
    public synchronized ClassLoader getClassLoader( IFlexMojosPlugin plugin ) throws NitroClassLoaderException
    {
        if( !classloaders.containsKey( plugin.getPluginId() ) )
        {
            final ClassLoader classLoader = plugin.getClass().getClassLoader();
            
            if ( !( classLoader instanceof DefaultClassLoader ) )
                throw new NitroClassLoaderException( new Status( Status.ERROR, plugin.getPluginId(),
                                                                 "Invalid class loader. Expected "
                                                                     + DefaultClassLoader.class.getName() ) );
            
            final DefaultClassLoader osgiCl = (DefaultClassLoader)classLoader;
    
            try
            {
                final NitroEclipseDelegate delegate = new NitroEclipseDelegate( osgiCl.getDelegate() );
                final NitroClassLoader nitroLoader = new NitroClassLoader( osgiCl, delegate );
                nitroLoader.registerNatives();
                classloaders.put( plugin.getPluginId(), nitroLoader );
            }
            catch ( Exception e )
            {
                throw new NitroClassLoaderException( new Status( Status.ERROR, plugin.getPluginId(),
                                                                 "Error creating Nitro class loader delegate.", e ) );
            }
        }
        
        return classloaders.get( plugin.getPluginId() );
    }
}
