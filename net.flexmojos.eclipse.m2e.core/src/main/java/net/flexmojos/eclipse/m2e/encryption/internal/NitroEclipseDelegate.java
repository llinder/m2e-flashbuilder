package net.flexmojos.eclipse.m2e.encryption.internal;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.Enumeration;

import org.eclipse.osgi.baseadaptor.BaseData;
import org.eclipse.osgi.framework.adaptor.ClassLoaderDelegate;
import org.eclipse.osgi.framework.internal.core.AbstractBundle;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifiedlogic.nitro.loaders.NitroClassLoader;
import com.simplifiedlogic.nitro.loaders.NitroLoaderDelegate;

@SuppressWarnings( "restriction" )
public class NitroEclipseDelegate
    implements NitroLoaderDelegate
{
    private static final Logger log = LoggerFactory.getLogger(NitroEclipseDelegate.class);
    
    private ClassLoaderDelegate delegate = null;

    private BaseData bundleData = null;

    private static final String classExtension = NitroClassLoader.encryptedName( ".class" );

    protected static PrintStream pso = null;

    public NitroEclipseDelegate( ClassLoaderDelegate delegate )
        throws Exception
    {
        this( delegate, null );
    }

    public NitroEclipseDelegate( ClassLoaderDelegate delegate, BaseData bundleData )
        throws Exception
    {
        this.delegate = delegate;
        this.bundleData = bundleData;
    }

    public Class<?> findClass( String classname )
        throws ClassNotFoundException
    {
        logInfo( new StringBuilder().append( "findClass: " ).append( classname ).toString() );
        if ( this.delegate != null )
        {
            logInfo( new StringBuilder().append( "   Looking for class in delegate=" ).append( this.delegate.getClass().getName() ).toString() );
            Class<?> cls = this.delegate.findClass( classname );
            logInfo( new StringBuilder().append( "   found class: " ).append( cls != null ? cls.getName() : "null" ).toString() );
            return cls;
        }

        return null;
    }

    public URL findResource( String resource )
    {
        logInfo( new StringBuilder().append( "findResource: " ).append( resource ).toString() );
        if ( this.delegate != null )
        {
            URL url = this.delegate.findResource( resource );
            if ( url != null )
            {
                logInfo( new StringBuilder().append( "   found: " ).append( url ).toString() );
                if ( resource.endsWith( classExtension ) )
                {
                    Bundle bundle = getBundle( url );
                    if ( bundle != null )
                    {
                        try
                        {
                            checkActivate( bundle );
                        }
                        catch ( Throwable e )
                        {
                            logInfo( new StringBuilder().append( "Error in delegate: " ).append( e.getMessage() ).toString() );
                            logTrace( e );
                        }
                        logInfo( "bundle.getResource" );
                        bundle.getResource( resource );
                    }
                }
            }
            return url;
        }

        return null;
    }

    public Class<?> findRemoteClass( String name )
        throws ClassNotFoundException
    {
        logInfo( new StringBuilder().append( "findRemoteClass: " ).append( name ).toString() );
        if ( this.delegate != null )
        {
            String resource = NitroClassLoader.encryptedName( NitroClassLoader.classToFileName( name ) );
            URL url = this.delegate.findResource( resource );

            if ( url != null )
            {
                logInfo( new StringBuilder().append( "   found: " ).append( url ).toString() );
                if ( ( resource.endsWith( classExtension ) ) || ( resource.endsWith( ".class" ) ) )
                {
                    Bundle bundle = getBundle( url );
                    logInfo( new StringBuilder().append( "    remote bundle:" ).append( bundle ).toString() );
                    if ( ( bundle != null ) && ( bundle.getBundleId() != this.bundleData.getBundleID() ) )
                    {
                        Class<?> cls = null;
                        String activator = (String) bundle.getHeaders().get( "Bundle-Activator" );
                        logInfo( new StringBuilder().append( "    Activator: " ).append( activator ).toString() );
                        if ( ( activator != null ) && ( activator.equals( name ) ) )
                        {
                            cls = bundle.loadClass( name );
                            logInfo( new StringBuilder().append( "    found class before activate: " ).append( cls != null ? cls.getName()
                                                                                                                               : "null" ).toString() );
                        }
                        try
                        {
                            checkActivate( bundle );
                        }
                        catch ( Throwable e )
                        {
                            logTrace( e,
                                      new StringBuilder().append( "Error in delegate: " ).append( e.getMessage() ).toString() );
                        }

                        if ( cls == null )
                        {
                            cls = bundle.loadClass( name );
                            logInfo( new StringBuilder().append( "    found class after activate: " ).append( cls != null ? cls.getName()
                                                                                                                              : "null" ).toString() );
                        }

                        return cls;
                    }
                }
            }
        }

        return null;
    }

    public Enumeration<?> findResources( String resource )
        throws IOException
    {
        if ( this.delegate != null )
        {
            return this.delegate.findResources( resource );
        }
        return null;
    }

    public String findLibrary( String libraryname )
    {
        if ( this.delegate != null )
        {
            return this.delegate.findLibrary( libraryname );
        }
        return null;
    }

    private void checkActivate( Bundle bundle )
        throws Exception
    {
        logInfo( "checkActivate" );

        logInfo( new StringBuilder().append( "    ACTIVATING BUNDLE: " ).append( bundle.getSymbolicName() ).append( " -- " ).append( bundle.getClass().getName() ).toString() );
        logInfo( new StringBuilder().append( "    bundle state: " ).append( bundle.getState() ).toString() );

        Thread threadChangingState = ( (AbstractBundle) bundle ).getStateChanging();
        if ( ( bundle.getState() == 8 ) && ( threadChangingState == Thread.currentThread() ) )
        {
            return;
        }

        logInfo( "    calling start()" );
        bundle.start();
    }

    private Bundle getBundle( URL url )
    {
        if ( this.bundleData != null )
        {
            logInfo( new StringBuilder().append( "    cur bundle: " ).append( this.bundleData.getBundleID() ).append( " : " ).append( this.bundleData.getBundle().getSymbolicName() ).toString() );
            logInfo( new StringBuilder().append( "    location: " ).append( this.bundleData.getBundle().getLocation() ).toString() );
            String idStr = url.getHost();
            long id = 0L;
            try
            {
                id = Integer.parseInt( idStr );
            }
            catch ( Exception e )
            {
            }
            if ( id > 0L )
            {
                BundleContext context = this.bundleData.getBundle().getBundleContext();
                Bundle bundle;
                if ( id != this.bundleData.getBundleID() )
                    bundle = context.getBundle( id );
                else
                    bundle = this.bundleData.getBundle();
                return bundle;
            }
        }
        return null;
    }

    private void logInfo( String msg )
    {
        if( log.isInfoEnabled() )
            log.info( msg );
    }

    private void logTrace( Throwable e )
    {
        if( log.isErrorEnabled() )
            log.error( "Unexpected error", e );
    }

    private void logTrace( Throwable e, String msg )
    {
        if( log.isErrorEnabled() )
            log.error( msg, e );
    }
}
