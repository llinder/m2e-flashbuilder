package net.flexmojos.eclipse.m2e.ide.encrypted.adapter;

import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.CoreMatchers.allOf;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.artifactId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.groupId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.type;

import java.util.Set;

import net.flexmojos.eclipse.m2e.common.FlexConstants;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.project.MavenProject;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

public class BaseProjectAdapter
{
    @SuppressWarnings( "unchecked" )
    protected static boolean isActionScriptOnly( final MavenProject project )
    {   
        final Set<Artifact> artifacts = project.getArtifacts();
        
        Artifact[] as = artifacts.toArray( new Artifact[0] );
        
        final Artifact a = selectFirst( as, allOf( groupId( FlexConstants.FRAMEWORK_GROUPID ), artifactId( "framework"), type( FlexExtension.SWC) ) );
        
        return ( a == null );
    }
    
    @SuppressWarnings("unchecked")
    protected static boolean isApollo( final MavenProject project )
    {
        final Set<Artifact> artifacts = project.getArtifacts();
        
        final Artifact a = selectFirst( artifacts, allOf( groupId( FlexConstants.FRAMEWORK_GROUPID ), artifactId( "airglobal"), type( FlexExtension.SWC) ) );
        
        return ( a != null );
    }
}
