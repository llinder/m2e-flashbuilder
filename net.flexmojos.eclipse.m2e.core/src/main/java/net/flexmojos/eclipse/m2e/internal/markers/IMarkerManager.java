package net.flexmojos.eclipse.m2e.internal.markers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public interface IMarkerManager
{
    /**
     * Add a marker to a resource
     * 
     * @param resource : the IResource to attach the marker to.
     * @param message : the marker's message.
     * @param lineNumber : the resource line to attach the marker to.
     * @param severity : the severity of the marker.
     */
    public IMarker addMarker( IResource resource, String type, String message, int lineNumber, int severity );

    /**
     * Add a marker to a resource. If singleton is true then the marker will only be added if it doesn't already exist
     * on the specified resource.
     * 
     * @param resource
     * @param type
     * @param message
     * @param lineNumber
     * @param severity
     * @param singleton
     * @return
     */
    public IMarker addMarker( IResource resource, String type, String message, int lineNumber, int severity, boolean singleton );

    /**
     * Delete all markers of the specified type (including subtypes) from an IResource
     */
    public void deleteMarkers( IResource resource, String type )
        throws CoreException;

    /**
     * Delete all markers of the specified type from an IResource
     */
    public void deleteMarkers(IResource resource, boolean includeSubtypes, String type) throws CoreException;
}
