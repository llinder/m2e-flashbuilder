package net.flexmojos.eclipse.m2e.internal.markers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarkerManager
    implements IMarkerManager
{
    private static final Logger log = LoggerFactory.getLogger( MarkerManager.class );

    public IMarker addMarker( IResource resource, String type, String message, int lineNumber, int severity )
    {
        return addMarker( resource, type, message, lineNumber, severity, false );
    }

    public void deleteMarkers( IResource resource, String type )
        throws CoreException
    {
        deleteMarkers( resource, true /* includeSubtypes */, type );
    }

    public void deleteMarkers( IResource resource, boolean includeSubtypes, String type )
        throws CoreException
    {
        if ( resource != null && resource.exists() )
        {
            resource.deleteMarkers( type, includeSubtypes, IResource.DEPTH_INFINITE );
        }
    }

    public void deleteMarkers( IResource resource, String type, String attrName, String attrValue )
        throws CoreException
    {
        if ( resource == null || !resource.exists() )
        {
            return;
        }

        IMarker[] markers = resource.findMarkers( type, false /* includeSubtypes */, IResource.DEPTH_ZERO );
        for ( IMarker marker : markers )
        {
            if ( eq( attrValue, marker.getAttribute( attrName ) ) )
            {
                marker.delete();
            }
        }
    }

    public IMarker addMarker( IResource resource, String type, String message, int lineNumber, int severity,
                              boolean singleton )
    {
        IMarker marker = null;
        try
        {
            if ( resource.isAccessible() )
            {
                if ( lineNumber == -1 )
                    lineNumber = 1;

                if ( singleton )
                {
                    marker = findMarker( resource, type, message, lineNumber, severity, false /* isTransient */);
                    if ( marker != null )
                    {
                        // This marker already exists
                        return marker;
                    }
                }

                marker = resource.createMarker( type );
                marker.setAttribute( IMarker.MESSAGE, message );
                marker.setAttribute( IMarker.SEVERITY, severity );
                marker.setAttribute( IMarker.TRANSIENT, false );

                marker.setAttribute( IMarker.LINE_NUMBER, lineNumber );
                if ( log.isDebugEnabled() )
                    log.debug( "Created marker '{}' on resource '{}'.", message, resource.getFullPath() );
            }
        }
        catch ( CoreException ex )
        {
            log.error( "Unable to add marker; " + ex.toString(), ex ); //$NON-NLS-1$
        }
        return marker;
    }

    private IMarker findMarker( IResource resource, String type, String message, int lineNumber, int severity,
                                boolean isTransient )
        throws CoreException
    {
        IMarker[] markers = resource.findMarkers( type, false /* includeSubtypes */, IResource.DEPTH_ZERO );
        if ( markers == null || markers.length == 0 )
        {
            return null;
        }
        for ( IMarker marker : markers )
        {
            if ( eq( message, marker.getAttribute( IMarker.MESSAGE ) )
                && eq( lineNumber, marker.getAttribute( IMarker.LINE_NUMBER ) )
                && eq( severity, marker.getAttribute( IMarker.SEVERITY ) )
                && eq( isTransient, marker.getAttribute( IMarker.TRANSIENT ) ) )
            {
                return marker;
            }
        }
        return null;
    }

    private static <T> boolean eq( T a, T b )
    {
        if ( a == null )
        {
            if ( b == null )
            {
                return true;
            }
            return false;
        }
        return a.equals( b );
    }

}
