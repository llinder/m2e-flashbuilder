package org.sonatype.flexmojos.compiler;

import java.io.File;
import java.util.List;


public interface IASDocConfiguration extends IFlexConfiguration {
  String[] getHelp();

  Boolean getIncludeLookupOnly();

  String[] getLoadConfig();

  String getOutput();

  Boolean getVersion();

  List getDocClasses();

  List getDocNamespaces();

  File[] getDocSources();

  String getExamplesPath();

  List getExcludeClasses();

  Boolean getExcludeDependencies();

  String getFooter();

  Boolean getKeepXml();

  Integer getLeftFramesetWidth();

  String getMainTitle();

  String getPackageDescriptionFile();

  Boolean getSkipXsl();

  String getTemplatesPath();

  String getWindowTitle();

  Boolean getRestoreBuiltinClasses();

  Boolean getLenient();

  File[] getExcludeSources();

  Boolean getDateInFooter();

  Boolean getIncludeAllForAsdoc();

  Boolean getWarnings();

  String getDumpConfig();

  String getFramework();

  Boolean getBenchmark();

  Integer getBenchmarkCompilerDetails();

  Long getBenchmarkTimeFilter();

  String getDebugPassword();

  Integer getDefaultBackgroundColor();

  Integer getDefaultFrameRate();

  IDefaultScriptLimits getDefaultScriptLimits();

  IDefaultSize getDefaultSize();

  List getExterns();

  Boolean getGenerateFrameLoader();

  List getIncludes();

  Boolean getLazyInit();

  String getLinkReport();

  String getSizeReport();

  String[] getLoadExterns();

  String getRawMetadata();

  String getResourceBundleList();

  String[] getRuntimeSharedLibraries();

  Boolean getUseNetwork();

  IRuntimeSharedLibraryPath[] getRuntimeSharedLibraryPath();

  Boolean getStaticLinkRuntimeSharedLibraries();

  Boolean getVerifyDigests();

  Boolean getRemoveUnusedRsls();

  Boolean getIncludeInheritanceDependenciesOnly();

  String getTargetPlayer();

  Integer getSwfVersion();

  Boolean getUseDirectBlit();

  Boolean getUseGpu();

  Boolean getSwcChecksum();

  String getToolsLocale();

  IPackagesConfiguration getPackagesConfiguration();

  ICompilerConfiguration getCompilerConfiguration();

  IFramesConfiguration getFramesConfiguration();

  IMetadataConfiguration getMetadataConfiguration();

  ILicensesConfiguration getLicensesConfiguration();

  IRuntimeSharedLibrarySettingsConfiguration getRuntimeSharedLibrarySettingsConfiguration();

  String HELP = "getHelp";

  String INCLUDE_LOOKUP_ONLY = "getIncludeLookupOnly";

  String LOAD_CONFIG = "getLoadConfig";

  String OUTPUT = "getOutput";

  String VERSION = "getVersion";

  String DOC_CLASSES = "getDocClasses";

  String DOC_NAMESPACES = "getDocNamespaces";

  String DOC_SOURCES = "getDocSources";

  String EXAMPLES_PATH = "getExamplesPath";

  String EXCLUDE_CLASSES = "getExcludeClasses";

  String EXCLUDE_DEPENDENCIES = "getExcludeDependencies";

  String FOOTER = "getFooter";

  String KEEP_XML = "getKeepXml";

  String LEFT_FRAMESET_WIDTH = "getLeftFramesetWidth";

  String MAIN_TITLE = "getMainTitle";

  String PACKAGE_DESCRIPTION_FILE = "getPackageDescriptionFile";

  String SKIP_XSL = "getSkipXsl";

  String TEMPLATES_PATH = "getTemplatesPath";

  String WINDOW_TITLE = "getWindowTitle";

  String RESTORE_BUILTIN_CLASSES = "getRestoreBuiltinClasses";

  String LENIENT = "getLenient";

  String EXCLUDE_SOURCES = "getExcludeSources";

  String DATE_IN_FOOTER = "getDateInFooter";

  String INCLUDE_ALL_FOR_ASDOC = "getIncludeAllForAsdoc";

  String WARNINGS = "getWarnings";

  String DUMP_CONFIG = "getDumpConfig";

  String FRAMEWORK = "getFramework";

  String BENCHMARK = "getBenchmark";

  String BENCHMARK_COMPILER_DETAILS = "getBenchmarkCompilerDetails";

  String BENCHMARK_TIME_FILTER = "getBenchmarkTimeFilter";

  String DEBUG_PASSWORD = "getDebugPassword";

  String DEFAULT_BACKGROUND_COLOR = "getDefaultBackgroundColor";

  String DEFAULT_FRAME_RATE = "getDefaultFrameRate";

  String DEFAULT_SCRIPT_LIMITS = "getDefaultScriptLimits";

  String DEFAULT_SIZE = "getDefaultSize";

  String EXTERNS = "getExterns";

  String GENERATE_FRAME_LOADER = "getGenerateFrameLoader";

  String INCLUDES = "getIncludes";

  String LAZY_INIT = "getLazyInit";

  String LINK_REPORT = "getLinkReport";

  String SIZE_REPORT = "getSizeReport";

  String LOAD_EXTERNS = "getLoadExterns";

  String RAW_METADATA = "getRawMetadata";

  String RESOURCE_BUNDLE_LIST = "getResourceBundleList";

  String RUNTIME_SHARED_LIBRARIES = "getRuntimeSharedLibraries";

  String USE_NETWORK = "getUseNetwork";

  String RUNTIME_SHARED_LIBRARY_PATH = "getRuntimeSharedLibraryPath";

  String STATIC_LINK_RUNTIME_SHARED_LIBRARIES = "getStaticLinkRuntimeSharedLibraries";

  String VERIFY_DIGESTS = "getVerifyDigests";

  String REMOVE_UNUSED_RSLS = "getRemoveUnusedRsls";

  String INCLUDE_INHERITANCE_DEPENDENCIES_ONLY = "getIncludeInheritanceDependenciesOnly";

  String TARGET_PLAYER = "getTargetPlayer";

  String SWF_VERSION = "getSwfVersion";

  String USE_DIRECT_BLIT = "getUseDirectBlit";

  String USE_GPU = "getUseGpu";

  String SWC_CHECKSUM = "getSwcChecksum";

  String TOOLS_LOCALE = "getToolsLocale";

}
