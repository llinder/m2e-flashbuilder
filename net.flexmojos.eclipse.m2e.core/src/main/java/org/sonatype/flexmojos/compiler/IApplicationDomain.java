package org.sonatype.flexmojos.compiler;

public interface IApplicationDomain extends IFlexArgument {
  String pathElement();

  String applicationDomainTarget();

  String[] ORDER = new String[] {"pathElement", "applicationDomainTarget",  };

}
