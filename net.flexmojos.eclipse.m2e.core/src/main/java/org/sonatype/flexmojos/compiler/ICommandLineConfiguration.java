package org.sonatype.flexmojos.compiler;

import java.util.List;

@SuppressWarnings( "rawtypes" )
public interface ICommandLineConfiguration extends IFlexConfiguration {
  String[] getHelp();

  List getIncludeResourceBundles();

  String[] getLoadConfig();

  String getOutput();

  Boolean getVersion();


  List getFileSpecs();

  String getProjector();

  Boolean getWarnings();

  String getDumpConfig();

  String getFramework();

  Boolean getBenchmark();

  Integer getBenchmarkCompilerDetails();

  Long getBenchmarkTimeFilter();

  String getDebugPassword();

  Integer getDefaultBackgroundColor();

  Integer getDefaultFrameRate();

  IDefaultScriptLimits getDefaultScriptLimits();

  IDefaultSize getDefaultSize();

  List getExterns();

  Boolean getGenerateFrameLoader();

  List getIncludes();

  Boolean getLazyInit();

  String getLinkReport();

  String getSizeReport();

  String[] getLoadExterns();

  String getRawMetadata();

  String getResourceBundleList();

  String[] getRuntimeSharedLibraries();

  Boolean getUseNetwork();

  IRuntimeSharedLibraryPath[] getRuntimeSharedLibraryPath();

  Boolean getStaticLinkRuntimeSharedLibraries();

  Boolean getVerifyDigests();

  Boolean getRemoveUnusedRsls();

  Boolean getIncludeInheritanceDependenciesOnly();

  String getTargetPlayer();

  Integer getSwfVersion();

  Boolean getUseDirectBlit();

  Boolean getUseGpu();

  Boolean getSwcChecksum();

  String getToolsLocale();

  ICompilerConfiguration getCompilerConfiguration();

  IFramesConfiguration getFramesConfiguration();

  IMetadataConfiguration getMetadataConfiguration();

  ILicensesConfiguration getLicensesConfiguration();

  IRuntimeSharedLibrarySettingsConfiguration getRuntimeSharedLibrarySettingsConfiguration();

  String HELP = "getHelp";

  String INCLUDE_RESOURCE_BUNDLES = "getIncludeResourceBundles";

  String LOAD_CONFIG = "getLoadConfig";

  String OUTPUT = "getOutput";

  String VERSION = "getVersion";

  String FILE_SPECS = "getFileSpecs";

  String PROJECTOR = "getProjector";

  String WARNINGS = "getWarnings";

  String DUMP_CONFIG = "getDumpConfig";

  String FRAMEWORK = "getFramework";

  String BENCHMARK = "getBenchmark";

  String BENCHMARK_COMPILER_DETAILS = "getBenchmarkCompilerDetails";

  String BENCHMARK_TIME_FILTER = "getBenchmarkTimeFilter";

  String DEBUG_PASSWORD = "getDebugPassword";

  String DEFAULT_BACKGROUND_COLOR = "getDefaultBackgroundColor";

  String DEFAULT_FRAME_RATE = "getDefaultFrameRate";

  String DEFAULT_SCRIPT_LIMITS = "getDefaultScriptLimits";

  String DEFAULT_SIZE = "getDefaultSize";

  String EXTERNS = "getExterns";

  String GENERATE_FRAME_LOADER = "getGenerateFrameLoader";

  String INCLUDES = "getIncludes";

  String LAZY_INIT = "getLazyInit";

  String LINK_REPORT = "getLinkReport";

  String SIZE_REPORT = "getSizeReport";

  String LOAD_EXTERNS = "getLoadExterns";

  String RAW_METADATA = "getRawMetadata";

  String RESOURCE_BUNDLE_LIST = "getResourceBundleList";

  String RUNTIME_SHARED_LIBRARIES = "getRuntimeSharedLibraries";

  String USE_NETWORK = "getUseNetwork";

  String RUNTIME_SHARED_LIBRARY_PATH = "getRuntimeSharedLibraryPath";

  String STATIC_LINK_RUNTIME_SHARED_LIBRARIES = "getStaticLinkRuntimeSharedLibraries";

  String VERIFY_DIGESTS = "getVerifyDigests";

  String REMOVE_UNUSED_RSLS = "getRemoveUnusedRsls";

  String INCLUDE_INHERITANCE_DEPENDENCIES_ONLY = "getIncludeInheritanceDependenciesOnly";

  String TARGET_PLAYER = "getTargetPlayer";

  String SWF_VERSION = "getSwfVersion";

  String USE_DIRECT_BLIT = "getUseDirectBlit";

  String USE_GPU = "getUseGpu";

  String SWC_CHECKSUM = "getSwcChecksum";

  String TOOLS_LOCALE = "getToolsLocale";

}
