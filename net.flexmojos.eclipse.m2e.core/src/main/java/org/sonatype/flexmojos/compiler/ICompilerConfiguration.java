package org.sonatype.flexmojos.compiler;

import java.io.File;
import java.util.List;

@SuppressWarnings( "rawtypes" )
public interface ICompilerConfiguration extends IFlexConfiguration {
  Boolean getAccessible();

  String getActionscriptFileEncoding();

  Boolean getAdjustOpdebugline();

  Boolean getAllowSourcePathOverlap();

  Boolean getAs3();

  IDefine[] getDefine();

  Boolean getConservative();

  String getContextRoot();

  Boolean getDebug();

  Boolean getCompress();

  String getDefaultsCssUrl();

  Boolean getDoc();

  Boolean getEs();

  File[] getExternalLibraryPath();

  Boolean getHeadlessServer();

  File[] getIncludeLibraries();

  Boolean getIncremental();

  Boolean getKeepAllTypeSelectors();

  String[] getKeepAs3Metadata();

  Boolean getKeepGeneratedActionscript();

  Boolean getKeepGeneratedSignatures();

  Boolean getEnableRuntimeDesignLayers();

  Boolean getEnableSwcVersionFiltering();

  File[] getLibraryPath();

  String[] getLocale();

  Integer getMemoryUsageFactor();

  Boolean getMobile();

  String getMinimumSupportedVersion();

  Boolean getOmitTraceStatements();

  Boolean getOptimize();

  String getPreloader();

  Boolean getResourceHack();

  String getServices();

  Boolean getShowActionscriptWarnings();

  Boolean getShowBindingWarnings();

  Boolean getShowDependencyWarnings();

  Boolean getReportInvalidStylesAsWarnings();

  Boolean getReportMissingRequiredSkinPartsAsWarnings();

  Boolean getShowInvalidCssPropertyWarnings();

  Boolean getShowDeprecationWarnings();

  Boolean getShowShadowedDeviceFontWarnings();

  Boolean getShowUnusedTypeSelectorWarnings();

  Boolean getDisableIncrementalOptimizations();

  File getSignatureDirectory();

  File[] getSourcePath();

  Boolean getStrict();

  List getTheme();

  List getDefaultsCssFiles();

  String getTranslationFormat();

  Boolean getUseResourceBundleMetadata();

  Boolean getVerboseStacktraces();

  Boolean getWarnArrayTostringChanges();

  Boolean getWarnAssignmentWithinConditional();

  Boolean getWarnBadArrayCast();

  Boolean getWarnBadBoolAssignment();

  Boolean getWarnBadDateCast();

  Boolean getWarnBadEs3TypeMethod();

  Boolean getWarnBadEs3TypeProp();

  Boolean getWarnBadNanComparison();

  Boolean getWarnBadNullAssignment();

  Boolean getWarnBadNullComparison();

  Boolean getWarnBadUndefinedComparison();

  Boolean getWarnBooleanConstructorWithNoArgs();

  Boolean getWarnChangesInResolve();

  Boolean getWarnClassIsSealed();

  Boolean getWarnConstNotInitialized();

  Boolean getWarnConstructorReturnsValue();

  Boolean getWarnDeprecatedEventHandlerError();

  Boolean getWarnDeprecatedFunctionError();

  Boolean getWarnDeprecatedPropertyError();

  Boolean getWarnDuplicateArgumentNames();

  Boolean getWarnDuplicateVariableDef();

  Boolean getWarnForVarInChanges();

  Boolean getWarnImportHidesClass();

  Boolean getWarnInstanceOfChanges();

  Boolean getWarnInternalError();

  Boolean getWarnLevelNotSupported();

  Boolean getWarnMissingNamespaceDecl();

  Boolean getWarnNegativeUintLiteral();

  Boolean getWarnNoConstructor();

  Boolean getWarnNoExplicitSuperCallInConstructor();

  Boolean getWarnNoTypeDecl();

  Boolean getWarnNumberFromStringChanges();

  Boolean getWarnScopingChangeInThis();

  Boolean getWarnSlowTextFieldAddition();

  Boolean getWarnUnlikelyFunctionValue();

  Boolean getWarnXmlClassHasChanged();

  Boolean getArchiveClassesAndAssets();

  Boolean getGenerateAbstractSyntaxTree();

  Boolean getIsolateStyles();

  Boolean getAllowDuplicateDefaultStyleDeclarations();

  String getJavaProfilerClass();

  IFontsConfiguration getFontsConfiguration();

  IMxmlConfiguration getMxmlConfiguration();

  INamespacesConfiguration getNamespacesConfiguration();

  IExtensionsConfiguration getExtensionsConfiguration();

  String ACCESSIBLE = "getAccessible";

  String ACTIONSCRIPT_FILE_ENCODING = "getActionscriptFileEncoding";

  String ADJUST_OPDEBUGLINE = "getAdjustOpdebugline";

  String ALLOW_SOURCE_PATH_OVERLAP = "getAllowSourcePathOverlap";

  String AS3 = "getAs3";

  String DEFINE = "getDefine";

  String CONSERVATIVE = "getConservative";

  String CONTEXT_ROOT = "getContextRoot";

  String DEBUG = "getDebug";

  String COMPRESS = "getCompress";

  String DEFAULTS_CSS_URL = "getDefaultsCssUrl";

  String DOC = "getDoc";

  String ES = "getEs";

  String EXTERNAL_LIBRARY_PATH = "getExternalLibraryPath";

  String HEADLESS_SERVER = "getHeadlessServer";

  String INCLUDE_LIBRARIES = "getIncludeLibraries";

  String INCREMENTAL = "getIncremental";

  String KEEP_ALL_TYPE_SELECTORS = "getKeepAllTypeSelectors";

  String KEEP_AS3_METADATA = "getKeepAs3Metadata";

  String KEEP_GENERATED_ACTIONSCRIPT = "getKeepGeneratedActionscript";

  String KEEP_GENERATED_SIGNATURES = "getKeepGeneratedSignatures";

  String ENABLE_RUNTIME_DESIGN_LAYERS = "getEnableRuntimeDesignLayers";

  String ENABLE_SWC_VERSION_FILTERING = "getEnableSwcVersionFiltering";

  String LIBRARY_PATH = "getLibraryPath";

  String LOCALE = "getLocale";

  String MEMORY_USAGE_FACTOR = "getMemoryUsageFactor";

  String MOBILE = "getMobile";

  String MINIMUM_SUPPORTED_VERSION = "getMinimumSupportedVersion";

  String OMIT_TRACE_STATEMENTS = "getOmitTraceStatements";

  String OPTIMIZE = "getOptimize";

  String PRELOADER = "getPreloader";

  String RESOURCE_HACK = "getResourceHack";

  String SERVICES = "getServices";

  String SHOW_ACTIONSCRIPT_WARNINGS = "getShowActionscriptWarnings";

  String SHOW_BINDING_WARNINGS = "getShowBindingWarnings";

  String SHOW_DEPENDENCY_WARNINGS = "getShowDependencyWarnings";

  String REPORT_INVALID_STYLES_AS_WARNINGS = "getReportInvalidStylesAsWarnings";

  String REPORT_MISSING_REQUIRED_SKIN_PARTS_AS_WARNINGS = "getReportMissingRequiredSkinPartsAsWarnings";

  String SHOW_INVALID_CSS_PROPERTY_WARNINGS = "getShowInvalidCssPropertyWarnings";

  String SHOW_DEPRECATION_WARNINGS = "getShowDeprecationWarnings";

  String SHOW_SHADOWED_DEVICE_FONT_WARNINGS = "getShowShadowedDeviceFontWarnings";

  String SHOW_UNUSED_TYPE_SELECTOR_WARNINGS = "getShowUnusedTypeSelectorWarnings";

  String DISABLE_INCREMENTAL_OPTIMIZATIONS = "getDisableIncrementalOptimizations";

  String SIGNATURE_DIRECTORY = "getSignatureDirectory";

  String SOURCE_PATH = "getSourcePath";

  String STRICT = "getStrict";

  String THEME = "getTheme";

  String DEFAULTS_CSS_FILES = "getDefaultsCssFiles";

  String TRANSLATION_FORMAT = "getTranslationFormat";

  String USE_RESOURCE_BUNDLE_METADATA = "getUseResourceBundleMetadata";

  String VERBOSE_STACKTRACES = "getVerboseStacktraces";

  String WARN_ARRAY_TOSTRING_CHANGES = "getWarnArrayTostringChanges";

  String WARN_ASSIGNMENT_WITHIN_CONDITIONAL = "getWarnAssignmentWithinConditional";

  String WARN_BAD_ARRAY_CAST = "getWarnBadArrayCast";

  String WARN_BAD_BOOL_ASSIGNMENT = "getWarnBadBoolAssignment";

  String WARN_BAD_DATE_CAST = "getWarnBadDateCast";

  String WARN_BAD_ES3_TYPE_METHOD = "getWarnBadEs3TypeMethod";

  String WARN_BAD_ES3_TYPE_PROP = "getWarnBadEs3TypeProp";

  String WARN_BAD_NAN_COMPARISON = "getWarnBadNanComparison";

  String WARN_BAD_NULL_ASSIGNMENT = "getWarnBadNullAssignment";

  String WARN_BAD_NULL_COMPARISON = "getWarnBadNullComparison";

  String WARN_BAD_UNDEFINED_COMPARISON = "getWarnBadUndefinedComparison";

  String WARN_BOOLEAN_CONSTRUCTOR_WITH_NO_ARGS = "getWarnBooleanConstructorWithNoArgs";

  String WARN_CHANGES_IN_RESOLVE = "getWarnChangesInResolve";

  String WARN_CLASS_IS_SEALED = "getWarnClassIsSealed";

  String WARN_CONST_NOT_INITIALIZED = "getWarnConstNotInitialized";

  String WARN_CONSTRUCTOR_RETURNS_VALUE = "getWarnConstructorReturnsValue";

  String WARN_DEPRECATED_EVENT_HANDLER_ERROR = "getWarnDeprecatedEventHandlerError";

  String WARN_DEPRECATED_FUNCTION_ERROR = "getWarnDeprecatedFunctionError";

  String WARN_DEPRECATED_PROPERTY_ERROR = "getWarnDeprecatedPropertyError";

  String WARN_DUPLICATE_ARGUMENT_NAMES = "getWarnDuplicateArgumentNames";

  String WARN_DUPLICATE_VARIABLE_DEF = "getWarnDuplicateVariableDef";

  String WARN_FOR_VAR_IN_CHANGES = "getWarnForVarInChanges";

  String WARN_IMPORT_HIDES_CLASS = "getWarnImportHidesClass";

  String WARN_INSTANCE_OF_CHANGES = "getWarnInstanceOfChanges";

  String WARN_INTERNAL_ERROR = "getWarnInternalError";

  String WARN_LEVEL_NOT_SUPPORTED = "getWarnLevelNotSupported";

  String WARN_MISSING_NAMESPACE_DECL = "getWarnMissingNamespaceDecl";

  String WARN_NEGATIVE_UINT_LITERAL = "getWarnNegativeUintLiteral";

  String WARN_NO_CONSTRUCTOR = "getWarnNoConstructor";

  String WARN_NO_EXPLICIT_SUPER_CALL_IN_CONSTRUCTOR = "getWarnNoExplicitSuperCallInConstructor";

  String WARN_NO_TYPE_DECL = "getWarnNoTypeDecl";

  String WARN_NUMBER_FROM_STRING_CHANGES = "getWarnNumberFromStringChanges";

  String WARN_SCOPING_CHANGE_IN_THIS = "getWarnScopingChangeInThis";

  String WARN_SLOW_TEXT_FIELD_ADDITION = "getWarnSlowTextFieldAddition";

  String WARN_UNLIKELY_FUNCTION_VALUE = "getWarnUnlikelyFunctionValue";

  String WARN_XML_CLASS_HAS_CHANGED = "getWarnXmlClassHasChanged";

  String ARCHIVE_CLASSES_AND_ASSETS = "getArchiveClassesAndAssets";

  String GENERATE_ABSTRACT_SYNTAX_TREE = "getGenerateAbstractSyntaxTree";

  String ISOLATE_STYLES = "getIsolateStyles";

  String ALLOW_DUPLICATE_DEFAULT_STYLE_DECLARATIONS = "getAllowDuplicateDefaultStyleDeclarations";

  String JAVA_PROFILER_CLASS = "getJavaProfilerClass";

}
