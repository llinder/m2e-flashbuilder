package org.sonatype.flexmojos.compiler;

public interface IDefaultScriptLimits extends IFlexArgument {
  String maxRecursionDepth();

  String maxExecutionTime();

  String[] ORDER = new String[] {"maxRecursionDepth", "maxExecutionTime",  };

}
