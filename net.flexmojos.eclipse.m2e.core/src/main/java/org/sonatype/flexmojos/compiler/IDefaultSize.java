package org.sonatype.flexmojos.compiler;

public interface IDefaultSize extends IFlexArgument {
  String width();

  String height();

  String[] ORDER = new String[] {"width", "height",  };

}
