package org.sonatype.flexmojos.compiler;

public interface IDefine extends IFlexArgument {
  String name();

  String value();

  String[] ORDER = new String[] {"name", "value",  };

}
