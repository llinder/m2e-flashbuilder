package org.sonatype.flexmojos.compiler;

import java.io.File;


public interface IDigestConfiguration extends IFlexConfiguration {
  File getRslFile();

  File getSwcPath();

  Boolean getSigned();

  String RSL_FILE = "getRslFile";

  String SWC_PATH = "getSwcPath";

  String SIGNED = "getSigned";

}
