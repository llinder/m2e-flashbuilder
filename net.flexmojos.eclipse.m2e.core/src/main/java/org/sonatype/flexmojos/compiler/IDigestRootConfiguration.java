package org.sonatype.flexmojos.compiler;

public interface IDigestRootConfiguration extends IFlexConfiguration {
  String[] getHelp();

  Boolean getVersion();

  IDigestConfiguration getDigestConfiguration();

  String HELP = "getHelp";

  String VERSION = "getVersion";

}
