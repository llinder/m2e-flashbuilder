package org.sonatype.flexmojos.compiler;

import java.io.File;


public interface IExtension extends IFlexArgument {
  File extension();

  String[] parameters();

  String[] ORDER = new String[] {"extension", "parameters",  };

}
