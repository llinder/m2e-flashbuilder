package org.sonatype.flexmojos.compiler;

public interface IExtensionsConfiguration extends IFlexConfiguration {
  IExtension[] getExtension();

  String EXTENSION = "getExtension";

}
