package org.sonatype.flexmojos.compiler;

import java.util.List;


public interface IFontsConfiguration extends IFlexConfiguration {
  Boolean getFlashType();

  Boolean getAdvancedAntiAliasing();

  String getLocalFontsSnapshot();

  List getLocalFontPaths();

  List getManagers();

  String getMaxCachedFonts();

  String getMaxGlyphsPerFace();

  ILanguages getLanguagesConfiguration();

  String FLASH_TYPE = "getFlashType";

  String ADVANCED_ANTI_ALIASING = "getAdvancedAntiAliasing";

  String LOCAL_FONTS_SNAPSHOT = "getLocalFontsSnapshot";

  String LOCAL_FONT_PATHS = "getLocalFontPaths";

  String MANAGERS = "getManagers";

  String MAX_CACHED_FONTS = "getMaxCachedFonts";

  String MAX_GLYPHS_PER_FACE = "getMaxGlyphsPerFace";

}
