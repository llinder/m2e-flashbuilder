package org.sonatype.flexmojos.compiler;

public interface IFrame extends IFlexArgument {
  String label();

  String[] classname();

  String[] ORDER = new String[] {"label", "classname",  };

}
