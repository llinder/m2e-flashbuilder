package org.sonatype.flexmojos.compiler;

public interface IFramesConfiguration extends IFlexConfiguration {
  IFrame[] getFrame();

  String FRAME = "getFrame";

}
