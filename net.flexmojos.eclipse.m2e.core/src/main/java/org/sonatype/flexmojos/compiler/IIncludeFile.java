package org.sonatype.flexmojos.compiler;

public interface IIncludeFile extends IFlexArgument {
  String name();

  String path();

  String[] ORDER = new String[] {"name", "path",  };

}
