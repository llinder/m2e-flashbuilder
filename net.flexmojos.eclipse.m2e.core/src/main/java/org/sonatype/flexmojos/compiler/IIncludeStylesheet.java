package org.sonatype.flexmojos.compiler;

public interface IIncludeStylesheet extends IFlexArgument {
  String name();

  String path();

  String[] ORDER = new String[] {"name", "path",  };

}
