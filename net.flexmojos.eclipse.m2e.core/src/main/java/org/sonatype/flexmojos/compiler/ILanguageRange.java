package org.sonatype.flexmojos.compiler;

public interface ILanguageRange extends IFlexArgument {
  String lang();

  String range();

  String[] ORDER = new String[] {"lang", "range",  };

}
