package org.sonatype.flexmojos.compiler;

public interface ILanguages extends IFlexConfiguration {
  ILanguageRange[] getLanguageRange();

  String LANGUAGE_RANGE = "getLanguageRange";

}
