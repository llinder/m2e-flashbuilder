package org.sonatype.flexmojos.compiler;

public interface ILicense extends IFlexArgument {
  String product();

  String serialNumber();

  String[] ORDER = new String[] {"product", "serialNumber",  };

}
