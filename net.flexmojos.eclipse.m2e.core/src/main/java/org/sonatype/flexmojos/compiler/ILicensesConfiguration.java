package org.sonatype.flexmojos.compiler;

public interface ILicensesConfiguration extends IFlexConfiguration {
  ILicense[] getLicense();

  String LICENSE = "getLicense";

}
