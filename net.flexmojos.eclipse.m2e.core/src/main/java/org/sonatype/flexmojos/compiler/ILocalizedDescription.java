package org.sonatype.flexmojos.compiler;

public interface ILocalizedDescription extends IFlexArgument {
  String text();

  String lang();

  String[] ORDER = new String[] {"text", "lang",  };

}
