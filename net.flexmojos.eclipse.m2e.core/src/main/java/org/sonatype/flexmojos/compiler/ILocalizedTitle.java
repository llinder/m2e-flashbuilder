package org.sonatype.flexmojos.compiler;

public interface ILocalizedTitle extends IFlexArgument {
  String title();

  String lang();

  String[] ORDER = new String[] {"title", "lang",  };

}
