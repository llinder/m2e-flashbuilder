package org.sonatype.flexmojos.compiler;

public interface IMetadataConfiguration extends IFlexConfiguration {
  String[] getContributor();

  String[] getCreator();

  String getDate();

  String getDescription();

  String[] getLanguage();

  ILocalizedDescription[] getLocalizedDescription();

  ILocalizedTitle[] getLocalizedTitle();

  String[] getPublisher();

  String getTitle();

  String CONTRIBUTOR = "getContributor";

  String CREATOR = "getCreator";

  String DATE = "getDate";

  String DESCRIPTION = "getDescription";

  String LANGUAGE = "getLanguage";

  String LOCALIZED_DESCRIPTION = "getLocalizedDescription";

  String LOCALIZED_TITLE = "getLocalizedTitle";

  String PUBLISHER = "getPublisher";

  String TITLE = "getTitle";

}
