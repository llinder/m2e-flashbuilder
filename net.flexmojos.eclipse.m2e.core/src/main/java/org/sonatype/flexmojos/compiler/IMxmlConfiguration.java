package org.sonatype.flexmojos.compiler;

public interface IMxmlConfiguration extends IFlexConfiguration {
  String getMinimumSupportedVersion();

  String getCompatibilityVersion();

  Boolean getQualifiedTypeSelectors();

  String MINIMUM_SUPPORTED_VERSION = "getMinimumSupportedVersion";

  String COMPATIBILITY_VERSION = "getCompatibilityVersion";

  String QUALIFIED_TYPE_SELECTORS = "getQualifiedTypeSelectors";

}
