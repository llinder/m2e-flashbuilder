package org.sonatype.flexmojos.compiler;

public interface INamespace extends IFlexArgument {
  String uri();

  String manifest();

  String[] ORDER = new String[] {"uri", "manifest",  };

}
