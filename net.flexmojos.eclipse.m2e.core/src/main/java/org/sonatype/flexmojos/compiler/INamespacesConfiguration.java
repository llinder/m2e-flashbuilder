package org.sonatype.flexmojos.compiler;

public interface INamespacesConfiguration extends IFlexConfiguration {
  INamespace[] getNamespace();

  String NAMESPACE = "getNamespace";

}
