package org.sonatype.flexmojos.compiler;

public interface IPackagesConfiguration extends IFlexConfiguration {
  String[] getPackage();

  String PACKAGE = "getPackage";

}
