package org.sonatype.flexmojos.compiler;

import java.util.Map;


public interface IRuntimeSharedLibraryPath extends IFlexArgument {
  String pathElement();

  Map rslUrl();

  String[] ORDER = new String[] {"pathElement", "rslUrl",  };

}
