package org.sonatype.flexmojos.compiler;

public interface IRuntimeSharedLibrarySettingsConfiguration extends IFlexConfiguration {
  String[] getForceRsls();

  IApplicationDomain[] getApplicationDomain();

  String FORCE_RSLS = "getForceRsls";

  String APPLICATION_DOMAIN = "getApplicationDomain";

}
