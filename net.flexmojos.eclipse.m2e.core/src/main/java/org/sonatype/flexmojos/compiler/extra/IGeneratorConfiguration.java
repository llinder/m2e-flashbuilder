package org.sonatype.flexmojos.compiler.extra;

import java.io.File;
import java.util.Map;

public interface IGeneratorConfiguration
{
    public String[] getIncludeJavaClasses();
    public String[] getExcludeJavaClasses();
    public File getOutputDirectory();
    public File getBaseOutputDirectory();
    public String getGeneratorToUse();
    public Map<String,String> getExtraOptions();
    public Map<String,String> getTemplates();
    public String[] getTranslators();
}
