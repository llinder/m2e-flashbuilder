package org.sonatype.flexmojos.compiler.extra;

import java.io.File;
import java.util.Map;

public interface IHtmlWrapperConfiguration
{
	String getTemplateURI();
	Map<String,String> getParameters();
	String getTargetPlayer();
	File getTemplateOutputDirectory();
}
