package org.sonatype.flexmojos.compiler.extra;

import org.apache.maven.artifact.Artifact;
import org.sonatype.flexmojos.compiler.IRuntimeSharedLibraryPath;

public interface IRuntimeSharedLibraryPathExtended extends
		IRuntimeSharedLibraryPath
{
	Artifact getArtifact();
	boolean isCrossdomainRsl();
	String[] getPolicyFileUrls();
}
