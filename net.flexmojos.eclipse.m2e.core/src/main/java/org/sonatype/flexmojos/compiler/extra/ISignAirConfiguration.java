package org.sonatype.flexmojos.compiler.extra;

import java.io.File;
import java.util.List;

import org.apache.maven.model.FileSet;

public interface ISignAirConfiguration
{
    public File getAirOutput();
    public String getClassifier();
    public File getDescriptorTemplate();
    public Boolean getFlexBuilderCompatibility();
    public List<String> getIncludeFiles();
    public FileSet[] getFileSets();
    public File getKeystore();
    public String getStorepass();
    public String getStoretype();
    public Boolean getStripVersion();
    public String getTimestampURL();
}
