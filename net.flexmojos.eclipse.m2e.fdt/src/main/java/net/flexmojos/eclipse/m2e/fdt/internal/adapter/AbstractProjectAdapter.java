package net.flexmojos.eclipse.m2e.fdt.internal.adapter;

import net.flexmojos.eclipse.m2e.internal.adapters.MxmlcMojoAdapter;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.powerflasher.fdt.core.resources.IFlashProject;
import com.powerflasher.fdt.internal.core.resources.DefaultOutputFolder;

@SuppressWarnings( "restriction" )
public abstract class AbstractProjectAdapter<T> implements ProjectAdapter<T>
{
    private final MojoExecution execution;
    
    public AbstractProjectAdapter( MojoExecution execution )
    {
        this.execution = execution;
    }
    
    protected MojoExecution getExecution()
    {
        return execution;
    }
   
    @SuppressWarnings( { "unchecked" } )
    public T applyMavenConfiguration( Object project, ProjectConfigurationRequest request,
                                                  IProgressMonitor monitor )
    {
        if( project instanceof IFlashProject )
        {
            IFlashProject flashProject = (IFlashProject)project;
            
            MxmlcMojoAdapter mojoAdapter = new MxmlcMojoAdapter( monitor, request.getMavenSession(), execution );
            
            // Set target directory
            DefaultOutputFolder outputFolder = (DefaultOutputFolder)flashProject.getDefaultOutputFolder();
            outputFolder.setFolder( mojoAdapter.getBuildDirectory() );
        }
        
        return (T)project;
    }
    
    
}
