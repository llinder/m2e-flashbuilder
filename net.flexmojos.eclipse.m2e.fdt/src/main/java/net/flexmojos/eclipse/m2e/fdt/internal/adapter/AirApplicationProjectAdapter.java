package net.flexmojos.eclipse.m2e.fdt.internal.adapter;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.powerflasher.fdt.core.resources.IFlashProject;
import com.powerflasher.fdt.internal.core.resources.ProjectAdapterFactory;

@SuppressWarnings( "restriction" )
public class AirApplicationProjectAdapter
    extends AbstractProjectAdapter<IFlashProject>
{

    public AirApplicationProjectAdapter( MojoExecution execution )
    {
        super( execution );
    }

    public IFlashProject createProject( IProject project, IProgressMonitor monitor )
    {
        return (IFlashProject)new ProjectAdapterFactory().getAdapter( project, IFlashProject.class );
    }

    @Override
    public IFlashProject applyMavenConfiguration( Object project, ProjectConfigurationRequest request,
                                                  IProgressMonitor monitor )
    {
        super.applyMavenConfiguration( project, request, monitor );
        
        if( project instanceof IFlashProject )
        {
            IFlashProject flashProject = (IFlashProject)project;
            
            // TODO apply air application configuration
            
            return flashProject;
        }
        return null;
    }
    
}
