package net.flexmojos.eclipse.m2e.fdt.internal.adapter;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;


public interface ProjectAdapter<T>
{
    public T createProject( IProject project, IProgressMonitor monitor );
    
    public T applyMavenConfiguration( Object project, ProjectConfigurationRequest request, IProgressMonitor monitor );
}
