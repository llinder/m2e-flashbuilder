package net.flexmojos.eclipse.m2e.fdt.internal.adapter;

import net.flexmojos.eclipse.m2e.ide.internal.adapter.BaseProjectAdapter;

import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

public class ProjectAdapterFactory extends BaseProjectAdapter
{
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.fdt.internal.adapter.ProjectAdapterFactory" );
    
    public static ProjectAdapter<?> getAdapter( MavenProject project, MojoExecution exec )
    {
        final ProjectType type = getProjectType( project );
        switch( type )
        {
            case APPLICATION:
                return new ApplicationProjectAdapter( exec );
            case AIR_APPLICATION:
                return new AirApplicationProjectAdapter( exec );
            case MOBILE_APPLICATION:
                return new MobileApplicationProjectAdapter( exec );
            case LIBRARY:
                return new LibraryProjectAdapter( exec );
            default:
                return new ApplicationProjectAdapter( exec );
        }
    }
    
    private static ProjectType getProjectType( final MavenProject project )
    {
        ProjectType type = null;
        
        if( project.getPackaging().equals( FlexExtension.AIR ) )
        {
            // TODO prompt if mobile or not
            // First inspect if mobileframework.swc then use mobile.
            // If not mobileframework.swc then prompt user to make a choice.
            return ProjectType.AIR_APPLICATION;
        }
        else if( project.getPackaging().equals( FlexExtension.SWF ) )
        {
            if( isApollo( project ) )
            {
                // TODO prompt if mobile or not
                // First inspect if mobileframework.swc then use mobile.
                // If not mobileframework.swc then prompt user to make a choice.
                return ProjectType.AIR_APPLICATION;
            }
            else
            {
                return ProjectType.APPLICATION;
            }
        }
        else if( project.getPackaging().equals( FlexExtension.SWC ) )
        {
            return ProjectType.LIBRARY;
        }
        else
        {
            if( log.isWarnEnabled() )
                log.warn( "Unable to determine FDT project type. Defaulting to regular Web Application." );
            
            type = ProjectType.APPLICATION;
        }
        
        return type;
    }
    
    public static enum ProjectType
    {
        APPLICATION,
        AIR_APPLICATION,
        MOBILE_APPLICATION,
        LIBRARY
    }
}
