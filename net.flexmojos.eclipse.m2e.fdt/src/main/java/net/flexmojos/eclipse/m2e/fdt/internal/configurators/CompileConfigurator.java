package net.flexmojos.eclipse.m2e.fdt.internal.configurators;

import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_COMPILE_SWC;
import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_COMPILE_SWF;
import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_WRAPPER;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_ARTIFACTID;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_GROUPID;

import java.util.List;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.configurator.AbstractEncryptedConfigurator;
import net.flexmojos.eclipse.m2e.configurator.InternalConfigurator;
import net.flexmojos.eclipse.m2e.fdt.internal.adapter.ProjectAdapter;
import net.flexmojos.eclipse.m2e.fdt.internal.adapter.ProjectAdapterFactory;

import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

public class CompileConfigurator extends InternalConfigurator
{
    private static final Logger log = LoggerFactory.getLogger( CompileConfigurator.class );

    public CompileConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        super( fbConfigurator );
    }
    
    @Override
    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        super.configure( request, monitor );
        
        final IProject project = request.getProject();
        final MavenProject mavenProject = request.getMavenProject();
        final MojoExecution compileExecution = getExecution( getCompileGoalForPackaging( mavenProject.getPackaging() ) );

        final ProjectAdapter<?> adapter = 
                        ProjectAdapterFactory.getAdapter( mavenProject, compileExecution );
        
        monitor.setTaskName( "Configuring FlexMojos project " + request.getProject().getName() );
        
        final Object fdtFlashProject = adapter.createProject( project, monitor );
        
        adapter.applyMavenConfiguration( fdtFlashProject, request, monitor );
    }

    protected String getCompileGoalForPackaging( String packaging ) throws CoreException
    {
        if ( FlexExtension.SWF.equals( packaging ) || FlexExtension.AIR.equals( packaging ) )
        {
            return GOAL_COMPILE_SWF;
        }
        else if ( FlexExtension.SWC.equals( packaging ) )
        {
            return GOAL_COMPILE_SWC;
        }
        
        throw new CoreException( new Status( IStatus.ERROR, FlexMojosPlugin.PLUGIN_ID, "No known goal for packaging "+packaging) );
    }
    
    @Override
    protected List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return request.getMavenProjectFacade().
                        getMojoExecutions( PLUGIN_GROUPID,
                                           PLUGIN_ARTIFACTID,
                                           monitor,
                                           GOAL_COMPILE_SWF,
                                           GOAL_COMPILE_SWC,
                                           GOAL_WRAPPER );
    }

    @Override
    protected boolean isEnterprise()
    {
        return false;
    }

}
