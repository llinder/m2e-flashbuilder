package net.flexmojos.eclipse.m2e.flashbuilder.integration.tests;

import net.flexmojos.eclipse.m2e.flashbuilder.tests.common.AbstractFlashBuilderMavenProjectTestCase;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.adobe.flexbuilder.project.actionscript.ActionScriptCore;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;

@SuppressWarnings( "restriction" )
public class BaseTests
    extends AbstractFlashBuilderMavenProjectTestCase
{
    
    @Before
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    @Test
    public void testHelloWord()
        throws Exception
    {
        IProject project = buildProject( "projects/hello-world/pom.xml" );
        assertNoErrors( project );
        IActionScriptProject asProject = ActionScriptCore.getProject( project );
        assertNotNull( "Check valid flex project.", asProject );
        assertTrue( "Output file should exist.", getOutputFile( asProject ).toFile().exists() );
    }
    
    @Test
    public void testHelloWordGenerateHtml()
        throws Exception
    {
        IProject project = buildProject( "projects/hello-world-generatehtml/pom.xml" );
        assertNoErrors( project );
        IActionScriptProject asProject = ActionScriptCore.getProject( project );
        assertNotNull( "Check valid flex project.", asProject );
        assertTrue( "Output file should exist.", getOutputFile( asProject ).toFile().exists() );
        
        // Validate html-template directory, embed:express-installation
        IPath templateFolder = project.getLocation().append( "html-template" );
        assertFalse( "Checkout html-template exists.", templateFolder.isEmpty() );
        IPath indexTemplate = templateFolder.append( "index.template" ).addFileExtension( "html" );
        assertTrue( "Check index.template exists.", indexTemplate.toFile().exists() );
        IPath playerProductInstall = templateFolder.append( "playerProductInstall" ).addFileExtension( "swf" );
        assertTrue( "Check playerProductInstall.swf", playerProductInstall.toFile().exists() );
        IPath AC_OETags = templateFolder.append( "AC_OETags" ).addFileExtension( "js" );
        assertTrue( "Check AC_OETags.js", AC_OETags.toFile().exists() );
    }
    
    @Test
    public void testSimpleFlexLibrary()
        throws Exception
    {
        IProject project = buildProject( "projects/simple-flex-library/pom.xml" );
        assertNoErrors( project );
        IActionScriptProject asProject = ActionScriptCore.getProject( project );
        assertNotNull( "Check valid flex project.", asProject );
        assertTrue( "Output file should exist.", getOutputFile( asProject ).toFile().exists() );
    }

    @After
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }
}
