package net.flexmojos.eclipse.m2e.flashbuilder.tests.common;

import java.io.IOException;

import net.flexmojos.eclipse.m2e.tests.common.AbstractMavenProjectTestCase;
import net.flexmojos.eclipse.m2e.tests.common.WorkspaceHelpers;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.m2e.core.project.ResolverConfiguration;

import com.adobe.flexbuilder.project.IFlexLibraryProject;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.util.PathUtils;

@SuppressWarnings( "restriction" )
public abstract class AbstractFlashBuilderMavenProjectTestCase
    extends AbstractMavenProjectTestCase
{
    protected static void assertNoErrors( IActionScriptProject project )
        throws CoreException
    {
        WorkspaceHelpers.assertNoErrors( (IProject) project );
    }

    protected IProject buildProject( String pom )
        throws IOException, CoreException, InterruptedException
    {
        ResolverConfiguration configuration = new ResolverConfiguration();
        IProject project = importProject( pom, configuration );
        waitForJobsToComplete();
        project.build( IncrementalProjectBuilder.FULL_BUILD, monitor );
        project.build( IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor );
        waitForJobsToComplete();
        return project;
    }
    
    protected IPath getOutputFile( IActionScriptProject project )
    {
        IPath output = (IPath) project.getProjectSettings().getOutputFolder();
        
        if( project instanceof IFlexLibraryProject )
        {
            output = output.append( project.getProject().getName() ).addFileExtension( "swc" );
        }
        else
        {
            IFile mainApplication = project.getMainApplication();
            output = output.append( mainApplication.getName().replace( mainApplication.getFileExtension(), "swf" ) );
        }
        
        output = PathUtils.relativeToAbsolute( output, project.getProject().getLocation() );
        return output;
    }
}
