package net.flexmojos.eclipse.m2e.flashbuilder.flexconfig.tests;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import net.flexmojos.eclipse.m2e.flashbuilder.internal.flexconfig.DefaultFlexConfigWritter;

import org.junit.Before;
import org.junit.Test;
import org.sonatype.flexmojos.compiler.ICompilerConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;



public class TestDefaultConfigWritter
{
	private ICompilerConfiguration compConfig;
	
	@Before
	public void setUp()
	{
		// Mock ICompilerConfiguration
		compConfig = createMock( ICompilerConfiguration.class );
		
		// Themes
		List<String> themes = new ArrayList<String>();
		themes.add( "/home/ted/proj/src/main/flex/main.css" );
		themes.add( "/home/ted/proj/src/main/flex/alt.css" );
		expect( compConfig.getTheme() ).andReturn( themes );
		
		// keep-all-type-selectors
		expect( compConfig.getKeepAllTypeSelectors() ).andReturn( true );
		
		// keep-as3-metadata
		expect( compConfig.getKeepAs3Metadata() ).andReturn( new String[] { "KeepMe1", "KeepMe2" } );
		
		
		
		replay( compConfig );
	}
	
	@Test
	public void testWriteThemes() throws Exception
	{
		ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		
		// Create flex-config.xml
		DefaultFlexConfigWritter writter = new DefaultFlexConfigWritter();
		writter.setMojoAdapter( compConfig );
		writter.writeConfig( oStream );
		
		// Read the configuration
		Document flexConfigDoc = readConfig( oStream.toByteArray() );
		
		// Test
		XPathFactory xf = XPathFactory.newInstance();
		XPath xpath = xf.newXPath();
		
		XPathExpression expr = xpath.compile("//flex-config/compiler/theme");
		NodeList themeNodes = (NodeList) expr.evaluate(flexConfigDoc, XPathConstants.NODESET);
		
		assertThat( "theme", themeNodes, notNullValue() );
		assertThat( "theme count", themeNodes.getLength(), equalTo( 1 ) );
		
		XPathExpression expr2 = xpath.compile("//flex-config/compiler/theme/filename");
		NodeList nameNodes = (NodeList) expr2.evaluate(flexConfigDoc, XPathConstants.NODESET);
		
		assertThat( "filename", nameNodes, notNullValue() );
		assertThat( "filename count", nameNodes.getLength(), equalTo( 2 ) );
		assertThat( nameNodes.item(0).getTextContent(), equalTo( "/home/ted/proj/src/main/flex/main.css" ) );
		assertThat( nameNodes.item(1).getTextContent(), equalTo( "/home/ted/proj/src/main/flex/alt.css" ) );
	}
	
	@Test
	public void testKeepAllTypeSelectors() throws Exception
	{	
		ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		
		// Create flex-config.xml
		DefaultFlexConfigWritter writter = new DefaultFlexConfigWritter();
		writter.setMojoAdapter( compConfig );
		writter.writeConfig( oStream );
		
		// Read the configuration
		Document flexConfigDoc = readConfig( oStream.toByteArray() );
		
		// Test
		XPathFactory xf = XPathFactory.newInstance();
		XPath xpath = xf.newXPath();
		
		XPathExpression expr = xpath.compile("//flex-config/compiler/keep-all-type-selectors");
		NodeList katsNodes = (NodeList) expr.evaluate(flexConfigDoc, XPathConstants.NODESET);
		
		assertThat( katsNodes, notNullValue() );
		assertThat( katsNodes.getLength(), equalTo( 1 ) );
		assertThat( katsNodes.item(0).getTextContent(), equalTo( "true" ) );
	}
	
	@Test
	public void testKeepAs3Metadata() throws Exception
	{	
		ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		
		// Create flex-config.xml
		DefaultFlexConfigWritter writter = new DefaultFlexConfigWritter();
		writter.setMojoAdapter( compConfig );
		writter.writeConfig( oStream );
		
		// Read the configuration
		Document flexConfigDoc = readConfig( oStream.toByteArray() );
		
		// Test
		XPathFactory xf = XPathFactory.newInstance();
		XPath xpath = xf.newXPath();
		
		XPathExpression expr = xpath.compile("//flex-config/compiler/keep-as3-metadata");
		NodeList keepNodes = (NodeList) expr.evaluate(flexConfigDoc, XPathConstants.NODESET);
		
		assertThat( "keep-as3-metadata", keepNodes, notNullValue() );
		assertThat( "keep-as3-metadata node coude", keepNodes.getLength(), equalTo( 1 ) );
		
		XPathExpression expr2 = xpath.compile("//flex-config/compiler/keep-as3-metadata/name");
		NodeList nameNodes = (NodeList) expr2.evaluate(flexConfigDoc, XPathConstants.NODESET);
		
		assertThat( "names", nameNodes, notNullValue() );
		assertThat( "names count", nameNodes.getLength(), equalTo( 2 ) );
		
		assertThat( nameNodes.item(0).getTextContent(), equalTo( "KeepMe1" ) );
		assertThat( nameNodes.item(1).getTextContent(), equalTo( "KeepMe2" ) );
	}
	
	private Document readConfig( byte[] buf ) throws Exception
	{
		ByteArrayInputStream iStream = new ByteArrayInputStream( buf );
		
        DocumentBuilderFactory factory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.parse( iStream );
        
        NodeList flexConfig = doc.getElementsByTagName( "flex-config" );
        
        assertThat( "Expected root node", flexConfig.item(0).getNodeName(), equalTo( "flex-config" ) );
        
        return doc;
	}
}
