package net.flexmojos.eclipse.m2e.flashbuilder;

import java.util.List;
import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.eclipse.core.runtime.IPath;
import org.hamcrest.Matcher;
import org.sonatype.flexmojos.compiler.extra.IRuntimeSharedLibraryPathExtended;

import com.adobe.flexbuilder.project.IClassPathEntry;

/**
 * Instances of this class can be used to incrementally define IClassPathEntry[] arrays used by Flashbuilder
 * to describe Flashbuilder Project classpath and contents of Flashbuilder classpath containers.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IClassPathDescriptor
{
    public void setRuntimeSharedLibraryPath( IRuntimeSharedLibraryPathExtended[] rslPaths );
    
    public IRuntimeSharedLibraryPathExtended[] getRuntimeSharedLibraryPath();
    
	/**
	 * @return true if classpath contains entry with specified path, false otherwise.
	 */
	public boolean containsPath( IPath path );

	/**
	 * Adds project source folder to the classpath.
	 * The source folder must exist in the workspace unless generated is true.
	 * In the latter case, the source classpath entry will be marked as optional.
	 * 
	 *  @param sourceArtifact Source artifact to add
	 *  @param outputLocation Output location in the workspace where the sources will be extracted.
	 *  
	 *  @return fully populated entry descriptor
	 */
	public IClassPathEntryDescriptor addSourceEntry( Artifact sourceArtifact, IPath outputLocation );

	/**
	 * Adds fully populated IClassPathEntry instance to the classpath.
	 * 
	 * @param entry Flex classpath entry.
	 * 
	 * @return fully populated entry descriptor
	 */
	public IClassPathEntryDescriptor addEntry( IClassPathEntry entry );

	/**
	 * Adds and returns new library entry to the classpath.
	 * 
	 * @param artifact Artifact to convert and add to the classpath.
	 * 
	 * @return Descriptor populated from artifact data
	 */
	public IClassPathEntryDescriptor addEntry( Artifact artifact );
	
	/**
	 * Adds and returns new collection library entry to the classpath.
	 * 
	 * @param artifacts Artifacts to convert and add to the classpath.
	 * 
	 * @return Descriptors populated from artifact data
	 */
	public List<IClassPathEntryDescriptor> addEntries( Set<Artifact> artifact );

	/**
	 * Removes entry with specified path from the classpath.
	 * That this can remove multiple entries because classpath does
	 * not enforce uniqueness of entry paths.
	 * 
	 * @param path Path to remove
	 * 
	 * @return List of descriptors which have been removed
	 */
	public List<IClassPathEntryDescriptor> removeEntries( IPath path );

	/**
	 * Removed entries using one or more matchers from the classpath
	 * 
	 * @param matchers One or more matchers to use for removal evaluation
	 * 
	 * @return List of descriptors which have been removed 
	 */
	public List<IClassPathEntryDescriptor> removeEntries(Matcher<?> ...matchers );

	/**
	 * Renders classpath as IClasspathEntry[] array
	 */
	public IClassPathEntry[] toEntriesArray();

	/**
	 * Returns underlying "live" list of IClasspathEntryDescriptor instances.
	 * Changes to the list will be immediately reflected in the classpath.
	 */
	public Set<IClassPathEntryDescriptor> getEntryDescriptors();

}
