package net.flexmojos.eclipse.m2e.flashbuilder;

import org.eclipse.core.runtime.IPath;
import org.eclipse.m2e.core.embedder.ArtifactKey;

import com.adobe.flexbuilder.project.IClassPathEntry;

public interface IClassPathEntryDescriptor
{
	  public IPath getPath();

	  public int getEntryKind();

	  public IPath getSourcePath();
	  
	  public void setSourcePath( IPath value );

	  public IPath getAsdocUrl();

	  public void setOutputLocation(IPath outputLocation);

	  public String getGroupId();

	  public String getArtifactId();

	  public ArtifactKey getArtifactKey();

	  public String getScope();
	  
	  public String getType();
	  
	  public String getClassifier();
	  
	  public String getVersion();
	  
	  public boolean isWorkspaceProject();

	  /**
	   * Create IClasspathEntry with information collected in this descriptor
	   */
	  public IClassPathEntry toClassPathEntry();
}
