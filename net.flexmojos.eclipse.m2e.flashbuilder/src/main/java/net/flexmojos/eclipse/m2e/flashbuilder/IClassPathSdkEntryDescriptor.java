package net.flexmojos.eclipse.m2e.flashbuilder;

public interface IClassPathSdkEntryDescriptor
    extends IClassPathEntryDescriptor
{
    public boolean isChildEntry( IClassPathEntryDescriptor entry );
    public void addChildEntry( IClassPathEntryDescriptor entry );
}
