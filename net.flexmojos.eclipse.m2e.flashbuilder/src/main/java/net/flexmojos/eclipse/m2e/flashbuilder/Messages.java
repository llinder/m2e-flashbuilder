package net.flexmojos.eclipse.m2e.flashbuilder;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
	private static final String BUNDLE_NAME = "net.flexmojos.eclipse.m2e.flashbuilder.messages"; // $NON-NLS-1$
	
	public static String FlashbuilderSwfProjectConfigurator_task_name;
	public static String FlashbuilderSwcProjectConfigurator_task_name;
	
	public static String ErrorFlexMojosNotFound_msg;
	public static String ErrorFlexGlobalDependencyNotFound_msg;
	
	static
	{
		// initialize resource bundle
		NLS.initializeMessages( BUNDLE_NAME, Messages.class );
	}
	
	private Messages() {}

}
