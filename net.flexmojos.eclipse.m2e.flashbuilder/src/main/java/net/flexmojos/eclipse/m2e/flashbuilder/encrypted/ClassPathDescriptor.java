package net.flexmojos.eclipse.m2e.flashbuilder.encrypted;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathEntryDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathSdkEntryDescriptor;

import org.apache.maven.artifact.Artifact;
import org.eclipse.core.runtime.IPath;
import org.eclipse.m2e.core.embedder.ArtifactKey;
import org.hamcrest.Matcher;
import org.sonatype.flexmojos.compiler.extra.IRuntimeSharedLibraryPathExtended;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class ClassPathDescriptor
    implements IClassPathDescriptor
{
    
    private IRuntimeSharedLibraryPathExtended[] rslPaths;
    
    private IMutableActionScriptProjectSettings context;

    private Set<IClassPathEntryDescriptor> entryDescriptors = new LinkedHashSet<IClassPathEntryDescriptor>();

    private IClassPathSdkEntryDescriptor sdkDescriptor;

    public ClassPathDescriptor()
    {
    }
    
    public void setRuntimeSharedLibraryPath( IRuntimeSharedLibraryPathExtended[] rslPaths )
    {
        this.rslPaths = rslPaths;
    }
    
    public IRuntimeSharedLibraryPathExtended[] getRuntimeSharedLibraryPath()
    {
        return rslPaths;
    }

    public ClassPathDescriptor( IMutableActionScriptProjectSettings context )
    {
        this.context = context;
        this.sdkDescriptor = new ClassPathSdkEntryDescriptor( context );
    }

    public boolean containsPath( IPath path )
    {
        IClassPathEntry entries[] = context.getLibraryPath();
        for ( IClassPathEntry entry : entries )
        {
            if ( entry.getResolvedPath().equals( path ) )
                return true;
        }

        return false;
    }

    public IClassPathEntryDescriptor addSourceEntry( Artifact sourceArtifact, IPath outputLocation )
    {
        final ArtifactKey key = new ArtifactKey( sourceArtifact );

        for ( IClassPathEntryDescriptor entry : entryDescriptors )
        {
            if ( entry.getArtifactKey().equals( key ) )
                entry.setSourcePath( outputLocation );
        }
        return null;
    }

    public IClassPathEntryDescriptor addEntry( IClassPathEntry entry )
    {
        // TODO Auto-generated method stub
        return null;
    }

    public IClassPathEntryDescriptor addEntry( Artifact artifact )
    {
        final IClassPathEntryDescriptor entry = makeDescriptor( artifact );
        
        // Don't insert an entry that was not accepted by 'makeDescriptor'
        if( entry != null )
        {
            if( sdkDescriptor.isChildEntry( entry ) )
                sdkDescriptor.addChildEntry( entry );
            else
                entryDescriptors.add( entry );
        }

        return entry;
    }

    public List<IClassPathEntryDescriptor> addEntries( Set<Artifact> artifact )
    {
        final List<IClassPathEntryDescriptor> entries = new ArrayList<IClassPathEntryDescriptor>();

        final Iterator<Artifact> iter = artifact.iterator();
        while ( iter.hasNext() )
        {
            final IClassPathEntryDescriptor entry = addEntry( iter.next() );
            if ( entry != null )
                entries.add( entry );
        }

        return entries;
    }

    public List<IClassPathEntryDescriptor> removeEntries( IPath path )
    {
        List<IClassPathEntryDescriptor> entries = new ArrayList<IClassPathEntryDescriptor>();

        for ( IClassPathEntryDescriptor entry : entryDescriptors )
        {
            if ( entry.getPath().equals( path ) )
            {
                entryDescriptors.remove( entry );
                entries.add( entry );
            }
        }

        return entries;
    }

    public List<IClassPathEntryDescriptor> removeEntries( Matcher<?>... matchers )
    {
        // TODO Auto-generated method stub
        return null;
    }

    public IClassPathEntry[] toEntriesArray()
    {
        final int l = entryDescriptors.size();

        final IClassPathEntry[] entries = new IClassPathEntry[l + 1]; // +1 for sdk

        Iterator<IClassPathEntryDescriptor> iter = entryDescriptors.iterator();

        int i = 0;
        while ( iter.hasNext() )
        {
            final IClassPathEntryDescriptor descriptor = iter.next();
            entries[i++] = descriptor.toClassPathEntry();
        }

        entries[i++] = sdkDescriptor.toClassPathEntry();

        return entries;
    }

    public Set<IClassPathEntryDescriptor> getEntryDescriptors()
    {
        return entryDescriptors;
    }

    private IClassPathEntryDescriptor makeDescriptor( Artifact artifact )
    {
        // ignore rb.swc beacons
        if ( FlexExtension.RB_SWC.equals( artifact.getType() ) && artifact.getClassifier() == null )
            return null;

        // Regular library entry
        IClassPathEntryDescriptor descriptor = new ClassPathEntryDescriptor( this, artifact, context );
        
        // ignore resource bundles when they belong to a workspace project
        if( FlexExtension.RB_SWC.equals( artifact.getType() ) && descriptor.isWorkspaceProject() )
        	return null;

        return descriptor;
    }

}
