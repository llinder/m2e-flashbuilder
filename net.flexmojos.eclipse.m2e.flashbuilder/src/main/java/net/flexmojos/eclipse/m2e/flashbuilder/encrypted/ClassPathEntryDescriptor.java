package net.flexmojos.eclipse.m2e.flashbuilder.encrypted;

import static org.sonatype.flexmojos.plugin.common.FlexScopes.CACHING;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.EXTERNAL;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.INTERNAL;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.MERGED;
import static org.sonatype.flexmojos.plugin.common.FlexScopes.RSL;

import java.util.Map;

import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathEntryDescriptor;

import org.apache.maven.artifact.Artifact;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.m2e.core.embedder.ArtifactKey;
import org.sonatype.flexmojos.compiler.extra.IRuntimeSharedLibraryPathExtended;
import org.sonatype.flexmojos.plugin.common.FlexExtension;
import org.sonatype.flexmojos.util.PathUtil;

import com.adobe.flexbuilder.project.ClassPathEntryFactory;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.IFlexLibraryProject;
import com.adobe.flexbuilder.project.IFlexLibraryProjectSettings;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProjectSettings;
import com.adobe.flexbuilder.project.common.CrossDomainRslEntry;
import com.adobe.flexbuilder.project.internal.FlexLibraryCore;

public class ClassPathEntryDescriptor
    implements IClassPathEntryDescriptor
{
    private static final String FRAMEWORK_GROUP_ID = "com.adobe.flex.framework";

    private static final String PLAYER_GLOBAL = "playerglobal";

    private static final String AIR_GLOBAL = "airglobal";

    private String type;

    private String scope;

    private IPath path;

    private IPath sourcePath;

    private IPath asdocUrl;

    private int entryKind;

    private IActionScriptProjectSettings context;

    private ArtifactKey artifactKey;

    private IClassPathDescriptor descriptor;

    /**
     * Constructor for creating a descriptor for a single artifact.
     * 
     * @param artifact
     */
    public ClassPathEntryDescriptor( IClassPathDescriptor descriptor, Artifact artifact,
                                     IActionScriptProjectSettings context )
    {
        this.descriptor = descriptor;

        scope = ( artifact.getScope() == null ) ? "compile" : artifact.getScope();
        path = new Path( artifact.getFile().getAbsolutePath() );
        entryKind = IClassPathEntry.KIND_LIBRARY_FILE;
        this.context = context;
        type = artifact.getType();

        artifactKey = new ArtifactKey( artifact );
    }

    /**
     * Constructor that creates a descriptor for a set of artifacts that make up a Flex SDK library
     * 
     * @param artifacts
     */
    public ClassPathEntryDescriptor( IActionScriptProjectSettings context )
    {
        entryKind = IClassPathEntry.KIND_FLEX_SDK;
        this.context = context;
    }

    public IPath getPath()
    {
        return path;
    }

    public int getEntryKind()
    {
        return entryKind;
    }

    public IPath getSourcePath()
    {
        return sourcePath;
    }

    public void setSourcePath( IPath value )
    {
        sourcePath = value;
    }

    public IPath getAsdocUrl()
    {
        return asdocUrl;
    }

    public void setAsdocUrl( IPath value )
    {
        asdocUrl = value;
    }

    public void setOutputLocation( IPath value )
    {

    }

    public String getGroupId()
    {
        return ( artifactKey != null ) ? artifactKey.getGroupId() : null;
    }

    public String getArtifactId()
    {
        return ( artifactKey != null ) ? artifactKey.getArtifactId() : null;
    }

    public String getClassifier()
    {
        return ( artifactKey != null ) ? artifactKey.getClassifier() : null;
    }

    public String getVersion()
    {
        return ( artifactKey != null ) ? artifactKey.getVersion() : null;
    }

    public ArtifactKey getArtifactKey()
    {
        return artifactKey;
    }

    public String getScope()
    {
        return scope;
    }

    public String getType()
    {
        return type;
    }

    public IClassPathEntry toClassPathEntry()
    {
        String path = null;
        String sourcePath = null;

        final IFlexLibraryProject workspaceProject = getWorkspaceProject();
        if ( workspaceProject != null )
        {
        	// Set library path
            IFile swc = workspaceProject.getOutputDebugSWCFile();
            path = swc.getFullPath().toString();
            
            // Set source path
            final IPath refSourceDir = workspaceProject.getProjectSettings().getMainSourceFolder();
            sourcePath = workspaceProject.getProject().getFullPath().append( refSourceDir ).toFile().toString();
        }
        else
        {
            path = PathUtil.path( getPath().toFile() );
        }

        IClassPathEntry entry = ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_LIBRARY_FILE, path, context );
        if ( sourcePath != null )
            entry.setSourceValue( sourcePath );

        entry = applyPathEntryConfig( entry );

        return entry;
    }
    
    public boolean isWorkspaceProject()
    {
    	return (getWorkspaceProject() != null);
    }

    protected IClassPathEntry applyPathEntryConfig( IClassPathEntry entry )
    {
        final int defaultLinkType = getDefaultLinkType();//entry.getContext().getDefaultLinkType();
        final int linkType = getLinkType();

        entry.setUseDefaultLinkType( linkType == defaultLinkType );

        entry.setLinkType( linkType );

        if ( entry.getLinkType() == IClassPathEntry.LINK_TYPE_RSL )
        {
            IRuntimeSharedLibraryPathExtended[] rslPaths = descriptor.getRuntimeSharedLibraryPath();
            for ( IRuntimeSharedLibraryPathExtended rslPath : rslPaths )
            {
                Artifact rslArtifact = rslPath.getArtifact();
                if ( artifactKey.getGroupId().equals( rslArtifact.getGroupId() )
                    && artifactKey.getArtifactId().equals( rslArtifact.getArtifactId() ) )
                {
                    @SuppressWarnings( "unchecked" )
                    Map<String, String> paths = rslPath.rslUrl();
                    if ( !paths.isEmpty() )
                    {
                        String firstPath = paths.keySet().iterator().next();
                        entry.setRslUrl( firstPath );
                        entry.setAutoExtractSwf( true ); // FM doesn't auto extract should we?
                    }
                }
            }
        }
        else if( entry.getLinkType() == IClassPathEntry.LINK_TYPE_CROSS_DOMAIN_RSL )
        {
            // TODO investigate how to handle this.
        }
        else
        {
            //entry.setCrossDomainRsls( new CrossDomainRslEntry[0] );
            //entry.setAutoExtractSwf( false );
            //entry.setRslUrl( null );
            //entry.setApplicationDomain( null );
        }

        return entry;
    }

    protected int getDefaultLinkType()
    {
        return context.getDefaultLinkType();
    }

    protected int getLinkType()
    {
        boolean globalArtifact =
            ( FRAMEWORK_GROUP_ID.equals( artifactKey.getGroupId() ) && FlexExtension.SWC.equals( this.type ) && ( PLAYER_GLOBAL.equals( artifactKey.getArtifactId() ) || AIR_GLOBAL.equals( artifactKey.getArtifactId() ) ) );

        if ( globalArtifact )
        {
            return IClassPathEntry.LINK_TYPE_EXTERNAL;
        }
        else if ( MERGED.equals( scope ) )
        {
            return IClassPathEntry.LINK_TYPE_INTERNAL;
        }
        else if ( EXTERNAL.equals( scope ) )
        {
            if ( context instanceof IFlexLibraryProjectSettings )
            {
                return IClassPathEntry.LINK_TYPE_DEFAULT;
            }
            else
            {
                return IClassPathEntry.LINK_TYPE_EXTERNAL;
            }
        }
        else if ( INTERNAL.equals( scope ) )
        {
            // TODO not sure what to do with this... return merged for now.
            return IClassPathEntry.LINK_TYPE_INTERNAL;
        }
        else if ( RSL.equals( scope ) )
        {
            return IClassPathEntry.LINK_TYPE_RSL;
        }
        else if ( CACHING.equals( scope ) )
        {
            return IClassPathEntry.LINK_TYPE_CROSS_DOMAIN_RSL;
        }
        else if ( Artifact.SCOPE_COMPILE.equals( scope ) )
        {
            if ( context instanceof IFlexLibraryProjectSettings )
            {
                return IClassPathEntry.LINK_TYPE_DEFAULT;
            }
            else
            {
                return IClassPathEntry.LINK_TYPE_INTERNAL;
            }
        }
        else
        {
            if ( context instanceof IFlexLibraryProjectSettings )
            {
                return IClassPathEntry.LINK_TYPE_DEFAULT;
            }
            else
            {
                return IClassPathEntry.LINK_TYPE_INTERNAL;
            }
        }

    }

    private IFlexLibraryProject getWorkspaceProject()
    {
    	
    	final IProject project = net.flexmojos.eclipse.m2e.encrypted.util.PathUtil.getWorkspaceProjectForArtifact( this.artifactKey );
    	return ( project != null ) ?
    		FlexLibraryCore.getProject( project.getProject() ) :
    			null;
    	
    }

    public String toString()
    {
        return artifactKey.toString();
    }
}
