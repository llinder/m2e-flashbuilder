package net.flexmojos.eclipse.m2e.flashbuilder.encrypted;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathEntryDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathSdkEntryDescriptor;

import org.eclipse.core.runtime.IPath;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

import com.adobe.flexbuilder.project.ClassPathEntryFactory;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.IFlexSDKClassPathEntry;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProjectSettings;

public class ClassPathSdkEntryDescriptor
    extends ClassPathEntryDescriptor
    implements IClassPathSdkEntryDescriptor
{
    private static final String ADOBE_GROUP_ID = "com.adobe.flex.framework";

    private IFlexSDKClassPathEntry sdkEntry;

    private IClassPathEntry[] sdkBaseChildEntries;

    private Set<ClassPathEntryDescriptor> childEntries;

    public ClassPathSdkEntryDescriptor( IActionScriptProjectSettings context )
    {
        super( context );
        sdkEntry = ClassPathEntryFactory.newFlexSDKEntry( context );
        sdkBaseChildEntries = sdkEntry.getChildLibraries( null );
        childEntries = new LinkedHashSet<ClassPathEntryDescriptor>();
    }

    public boolean isChildEntry( IClassPathEntryDescriptor entry )
    {
        if ( FlexExtension.RB_SWC.equals( entry.getType() ) )
            return false;

        if ( !ADOBE_GROUP_ID.equals( entry.getGroupId() ) )
            return false;

        for ( IClassPathEntry child : sdkBaseChildEntries )
        {
            if ( child.getValue().indexOf( entry.getArtifactId() ) != -1 )
                return true;
        }

        return false;
    }

    public void addChildEntry( IClassPathEntryDescriptor entry )
    {
        childEntries.add( (ClassPathEntryDescriptor)entry );
    }

    @Override
    public IClassPathEntry toClassPathEntry()
    {
        // Remove SDK entries that are not POM dependencies
        for ( IClassPathEntry lib : sdkBaseChildEntries )
        {
            boolean found = false;
            Iterator<ClassPathEntryDescriptor> iter = childEntries.iterator();
            
            while( iter.hasNext() )
            {
                IClassPathEntryDescriptor dep = iter.next();
                IPath libPath = lib.getResolvedPath();
                if ( libPath.lastSegment().equals( dep.getArtifactId()+"."+dep.getType() ) )
                {
                    found = true;
                    break;
                }
            }
            
            if( !found )
                sdkEntry.addToExclusionList( lib );
        }
        
        // Reorder and apply configuration changes
        Iterator<ClassPathEntryDescriptor> iter = childEntries.iterator();
        int order = 0;
        while ( iter.hasNext() )
        {
            IClassPathEntry matchedLib = null;
            ClassPathEntryDescriptor dep = iter.next();

            for ( IClassPathEntry lib : sdkBaseChildEntries )
            {
                IPath libPath = lib.getResolvedPath();
                if ( libPath.lastSegment().equals( dep.getArtifactId()+"."+dep.getType() ) )
                {
                    matchedLib = lib;
                    break;
                }
            }

            if ( matchedLib != null )
            {
                matchedLib.setLibraryIndex( order++ );
                dep.applyPathEntryConfig( matchedLib );
            }
        }

        return sdkEntry;
    }

}
