package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public abstract class AbstractProjectAdapter<T extends IActionScriptProject, S extends IMutableActionScriptProjectSettings> implements ProjectAdapter<T,S>
{
    final protected StringBuilder additionalCompilerArgs = new StringBuilder();
    
    final private MojoExecution execution;

    public AbstractProjectAdapter( MojoExecution exec )
    {
        this.execution = exec;
    }

    protected abstract T getProject( IProject project, S context, IProgressMonitor monitor )
        throws CoreException;

    protected abstract S getSettings( final T project )
        throws CoreException;
    
    protected MojoExecution getExecution()
    {
        return execution;
    }
}
