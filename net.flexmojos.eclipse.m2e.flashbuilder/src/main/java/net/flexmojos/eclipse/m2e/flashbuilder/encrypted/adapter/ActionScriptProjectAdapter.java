package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.actionscript.ActionScriptCore;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.actionscript.internal.ActionScriptProject;

public class ActionScriptProjectAdapter
    extends AbstractProjectAdapter<IActionScriptProject, IMutableActionScriptProjectSettings>
{

    public ActionScriptProjectAdapter( MojoExecution exec )
    {
        super( exec );
    }

    public IActionScriptProject createProject( IProject project, IMutableActionScriptProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        return getProject( project, context, monitor );
    }

    public IMutableActionScriptProjectSettings applyMavenConfiguration( IProject project,
                                                                        IMutableActionScriptProjectSettings context,
                                                                        IClassPathDescriptor classpath,
                                                                        ProjectConfigurationRequest request,
                                                                        IProgressMonitor monitor )
    {
        // SWF compilation configuration
        // Set configuration specific to Action Script Projects.
        MojoExecution exec = getExecution();
        if ( exec != null )
        {
            final MxmlcMojoAdapter mojoAdapter =
                new MxmlcMojoAdapter( monitor, request.getMavenSession(), exec );

            final ActionScriptSettingsAdapter settingsAdapter = 
                            new ActionScriptSettingsAdapter( project, mojoAdapter, context, classpath );

            settingsAdapter.applyConfiguration();
        }
        
        return context;
    }

    @Override
    protected IActionScriptProject getProject( IProject project, IMutableActionScriptProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        // Create and return new Action Script Project
        final IActionScriptProject asProject = new ActionScriptProject( context, project, monitor );

        Assert.isNotNull( project, "Unable to initialize Action Script Project." );

        return asProject;
    }

    protected IMutableActionScriptProjectSettings getSettings( IActionScriptProject project )
    {
        // IMutableActionScriptProjectSettings settings =
        // (IMutableActionScriptProjectSettings) project.getProjectSettings();
        IMutableActionScriptProjectSettings settings = project.getProjectSettingsClone();

        return settings;
    }

    public IMutableActionScriptProjectSettings createContext( IProject project )
    {
        // Create new settings
        IMutableActionScriptProjectSettings settings =
            ActionScriptCore.createProjectDescription( project.getName(), project.getLocation(), false );

        return settings;
    }
}
