package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.encrypted.adapters.CompcMojoAdapter;
import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.flexconfig.DefaultFlexConfigWritter;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.markers.IFlashbuilderMarkers;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.ArrayUtil;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.ClassPathUtils;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.FlexUtil;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.MavenUtils;
import net.flexmojos.eclipse.m2e.internal.markers.IMarkerManager;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.artifact.Artifact;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.plugin.common.FlexExtension;
import org.sonatype.flexmojos.util.PathUtil;

import com.adobe.flexbuilder.project.ApplicationDomainCollection;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.Module;
import com.adobe.flexbuilder.project.actionscript.IActionScriptBuildTargetSettings;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProjectSettings;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.actionscript.PublishSourceSettings;
import com.adobe.flexbuilder.project.internal.FlexProjectCore;
import com.adobe.flexbuilder.project.sdks.IFlexSDK;
import com.adobe.flexbuilder.project.themes.IFlexTheme;
import com.adobe.flexbuilder.util.FlashPlayerVersion;
import com.adobe.flexbuilder.util.PathUtils;

public class ActionScriptSettingsAdapter
    implements ISettingsAdapter, IActionScriptProjectSettings
{
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.adapter.ActionScriptSettingsAdapter" );
    
    private final MxmlcMojoAdapter adapter;
    
    private final IProject project;

    private final IMutableActionScriptProjectSettings settings;

    private final IClassPathDescriptor classpath;

    private StringBuilder args = new StringBuilder();

    private Map<IPath, IPath> buildCssFiles;

    private IPath[] applicationPaths;
    
    public ActionScriptSettingsAdapter( IProject project, MxmlcMojoAdapter mojoAdapter, IMutableActionScriptProjectSettings context, IClassPathDescriptor classpath )
    {
        this.project = project;
        this.adapter = mojoAdapter;
        this.settings = context;
        this.classpath = classpath;
    }
    
    protected IProject getProject()
    {
        return project;
    }
    protected MxmlcMojoAdapter getMojoAdapter()
    {
        return adapter;
    }
    protected IMutableActionScriptProjectSettings getSettings()
    {
        return settings;
    }

    public void applyConfiguration()
    {
        applySharedConfiguration();
    }

    /**
     * Applies configuration that is common to all project types.
     */
    protected void applySharedConfiguration()
    {
        if( !( adapter instanceof CompcMojoAdapter ) )
            settings.setApplicationPaths( getApplicationPaths() );

        // autoSortRsls
        if ( getAutoRSLOrdering() != null )
            settings.setAutoRSLOrdering( getAutoRSLOrdering() );

        // AFAIK used for AIR only 
        // settings.setBuildTargetSettings(arg0, arg1);

        // TODO does this map to FlexMojos? I don't think so but not 100% sure
        // settings.setCopyDependentFiles( false );

        settings.setFlex3CompatibilityMode( isFlex3CompatibilityMode() );

        settings.setFlexSDKName( getFlexSDKName() );

        settings.setMainSourceFolder( getMainSourceFolder() );

        settings.setOutputFolder( getOutputFolder() );

        if ( getRootURL() != null )
            settings.setRootURL( getRootURL() );

        settings.setShowWarnings( getShowWarnings() );

        // same as setMainSourceFolder
        // settings.setSourceFolder(arg0);

        settings.setSourcePath( getSourcePath() );

        // Only settings that apply to SWF or AIR projects
        if ( FlexExtension.SWF.equals( adapter.getProjectType() ) || FlexExtension.AIR.equals( adapter.getProjectType() ) )
        {
            // buildCssFiles
            if ( getBuildCSSFiles() != null )
                settings.setBuildCSSFiles( getBuildCSSFiles() );

            if ( getGenerateAccessibleSWF() != null )
                settings.setGenerateAccessibleSWF( getGenerateAccessibleSWF() );

            if ( getMainApplicationPath() != null )
                settings.setMainApplicationPath( getMainApplicationPath() );
            
            // Default to internal link type for SWF && AIR
            settings.setDefaultLinkType( IClassPathEntry.LINK_TYPE_INTERNAL );
        }
        else if( FlexExtension.SWC.equals( adapter.getProjectType() ) )
        {
            // Default to external link type for SWC
            settings.setDefaultLinkType( IClassPathEntry.LINK_TYPE_DEFAULT );
        }

        // Write additional configuration to flex-config.xml
        IPath compilerConfigPath = getProjectLocation().append( ".flexConfig.xml" );
        try
        {
            FileOutputStream configOutputStream = new FileOutputStream( compilerConfigPath.toFile() );
            DefaultFlexConfigWritter configWritter = new DefaultFlexConfigWritter();
            configWritter.setMojoAdapter( this.adapter );
            configWritter.writeConfig( configOutputStream );
        }
        catch ( Exception e )
        {
            if( log.isErrorEnabled() )
                log.error( "Error applying settings configuration", e );
        }

        // Additional compiler args
        addLocales();
        addConfigFiles();

        settings.setAdditionalCompilerArgs( args.toString() );
        
        classpath.setRuntimeSharedLibraryPath( adapter.getRuntimeSharedLibraryPathExtended() );
        classpath.addEntries( adapter.getDependencies( true ) );
        settings.setLibraryPath( getLibraryPath() );
    }

    protected void addLocales()
    {
        String[] locales = adapter.getLocale();
        if ( locales == null || locales.length == 0 )
        {
            // Add default per flexbuilder convention
            locales = new String[] { "en_US" };
        }

        args.append( " -locale" );
        for ( String locale : locales )
        {
            args.append( " " );
            args.append( locale );
        }
    }

    protected void addConfigFiles()
    {
        String[] files = null;

        // add extra config
        IPath extraConfig = new Path( ".flexConfig.xml" );
        // Has to be relative to the source path :(
        IPath mainSourcePath = getMainSourceFolder().makeAbsolute();
        extraConfig = extraConfig.makeRelativeTo( mainSourcePath );
        String extraConfigRelPath = extraConfig.toFile().toString();
        files = new String[] { extraConfigRelPath };

        files = ArrayUtil.concat( files, adapter.getLoadConfig() );

        if ( files != null && files.length > 0 )
        {

            // NOTE: using just '=' causes internal build error with FlexBuilder 3 on MacOSX with Eclipse Galileo.
            final String seperator = "+=";
            for ( String file : files )
            {
                args.append( " -load-config" );
                args.append( seperator );
                args.append( file );
            }
        }
    }

    // not used
    public boolean canChangeSDKConfig()
    {
        return false;
    }

    // not used
    public IClassPathEntry[] computeCompilerLibraryPath()
    {
        return null;
    }

    // not used
    public IProject[] computeDynamicReferences()
    {
        return null;
    }

    // not used
    public IClassPathEntry[] computeToolingLibraryPath()
    {
        return null;
    }

    public IPath getAbsoluteOutputFolder()
    {
        return new Path( PathUtil.path( adapter.getOutputDirectory() ) );
    }

    // not used
    public File getActionScriptProjectDescriptionFile()
    {
        return null;
    }

    public String getAdditionalCompilerArgs()
    {
        return args.toString();
    }

    public IPath[] getAirExcludePaths( IPath arg0 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    public IPath[] getApplicationPaths()
    {
        if ( applicationPaths != null )
            return applicationPaths;

        final String sourceFile = adapter.getSourceFile();

        // Ignore .css source files.
        if ( sourceFile != null && !sourceFile.contains( ".css" ) )
        {
            // main application (can only be one per Maven convention )
            IPath mainAppPath = new Path( sourceFile );
            
            // Make path relative to main source directory
            if( mainAppPath.isAbsolute() )
            {
                IPath mainSourceFolder = new Path( adapter.getMainSourcePath() );
                mainAppPath = PathUtils.absoluteToRelative( mainAppPath, mainSourceFolder );
            }
            applicationPaths = new IPath[] { mainAppPath };
        }
        else
        {
            applicationPaths = new IPath[0];
        }

        return applicationPaths;
    }

    public Map<IPath, IPath> getBuildCSSFiles()
    {
        if ( buildCssFiles != null )
            return buildCssFiles;

        Map<IPath, IPath> cssMap = null;

        if ( adapter.getSourceFile() != null )
        {
            if ( adapter.getSourceFile().contains( ".css" ) )
            {
                // Source file is relative to source folder.
                String css = adapter.getSourceFile();

                // Build a list of source paths to search.
                List<File> sourcePaths = Arrays.asList( adapter.getSourcePath() );
                File mainSourcePath = PathUtil.file( adapter.getMainSourcePath(), adapter.getProjectLocation() );
                sourcePaths.add( mainSourcePath );

                // Convert source file to full source path
                for ( File file : sourcePaths )
                {
                    File cssFile = new File( file, css );
                    if ( cssFile.exists() )
                    {
                        // Found source file... convert to IPath
                        IPath cssPath = new Path( PathUtil.path( cssFile ) );
                        // Get output directory
                        IPath destPath = getOutputFolder();

                        // Create map ( source file path, destination path)
                        cssMap = new HashMap<IPath, IPath>();
                        cssMap.put( cssPath, destPath );
                    }
                }
            }
        }

        return buildCssFiles = cssMap;
    }

    /* Burrito */
    public IActionScriptBuildTargetSettings getBuildTargetSettings( String arg0 )
    {
        // TODO not sure how this is used.
        return null;
    }

    public String[] getBuildTargets()
    {
        // TODO not sure how this is used.
        return null;
    }

    public IClassPathEntry[] getDefaultLibraryPath()
    {
        // TODO not sure if this is necessary
        return null;
    }

    public int getDefaultLinkType()
    {
        // TODO not sure best way to map this
        return 0;
    }

    public int getEffectiveDefaultLinkType()
    {
        // TODO not sure best way to map this
        return 0;
    }

    public String getFlexConfigPath()
    {
        // TODO don't think we need this
        return null;
    }

    public IFlexSDK getFlexSDK()
    {
        // TODO shouldn't need to touch this.
        return null;
    }

    public String getFlexSDKName()
    {
        final IFlexSDK sdks[] = FlexProjectCore.getDefault().getFlexSDKPreferences().getItems();// .getFlexSDKs();

        final Version sdkVersion = getSdkVersion();
        
        // Clear problem markers related to SDK
        final IMarkerManager markerManager = FlexMojosPlugin.getDefault().getMarkerManager();
        try
        {
            markerManager.deleteMarkers( getProject(), IFlashbuilderMarkers.MISSING_FLEX_SDK );
        }
        catch ( CoreException e )
        {
            if( log.isErrorEnabled() )
                log.error( "Unable to remove flex sdk markers.", e );
        }
        
        
        IFlexSDK sdk = null;
        try
        {
            sdk = FlexUtil.getFlexSdkByVersion( sdks, sdkVersion );
        }
        catch ( CoreException e1 )
        {
            if( log.isErrorEnabled() )
                log.error( "Error reading flex sdk name", e1 );
        }
        
        // Add problem marker if Flex SDK was not found.
        if( sdk == null )
        {
            // Add problem marker
            final String message =  "Unable to resolve matching Flex SDK for version "+sdkVersion.toString()+". Please install the proper Flex SDK in Flashbuilder and run Maven Update Project Configuraton.";
            markerManager.addMarker( getProject(), IFlashbuilderMarkers.MISSING_FLEX_SDK, message, 0, IMarker.SEVERITY_ERROR );
            
            // Return blank string so Flashbuilder will default to latest SDK.
            return "";
        }
        else
        {
            return sdk.getName();
        }
    }

    public IFlexTheme getFlexTheme()
    {
        // TODO handle this.
        return null;
    }

    public IClassPathEntry[] getLibraryPath()
    {
        return classpath.toEntriesArray();
    }

    public IPath getMainApplicationPath()
    {
        final IPath[] paths = getApplicationPaths();
        return ( paths != null && paths.length > 0 ) ? paths[0] : null;
    }

    public IPath getMainSourceFolder()
    {
        // Convert to relative path
        final File f = new File( adapter.getMainSourcePath() );
        final File home = adapter.getProjectLocation();
        final String r = PathUtil.relativePath( home, f );

        return new Path( r );
    }

    public Module[] getModules()
    {
        org.sonatype.flexmojos.plugin.compiler.attributes.Module modules[] = adapter.getModules();

        List<Module> fModules = new ArrayList<Module>();

        // convert flexmojos modules to flashbuilder modiles
        for ( int i = 0; i < modules.length; i++ )
        {
            org.sonatype.flexmojos.plugin.compiler.attributes.Module module = modules[i];

            final IPath moduleSource = new Path( module.getSourceFile() );

            String moduleFinalName;
            if ( module.getFinalName() != null )
            {
                moduleFinalName = module.getFinalName();
            }
            else
            {
                File moduleSourceFile = adapter.resolveSourceFile( module.getSourceFile() );
                String classifier = FilenameUtils.getBaseName( moduleSourceFile.getName() ).toLowerCase();

                moduleFinalName = adapter.getFinalName() + "-" + classifier;
            }

            File moduleOutputDir;
            if ( module.getDestinationPath() != null )
            {
                moduleOutputDir = new File( adapter.getBuildDirectory(), module.getDestinationPath() );
            }
            else
            {
                moduleOutputDir = new File( adapter.getBuildDirectory() );
            }

            IPath outputSwf = new Path( PathUtil.path( moduleOutputDir ) ).append( moduleFinalName );

            // TODO figure out how to correctly handle the optimization context.
            // NOTE I think this probably should be the path to the externs xml file.
            Module fModule = new Module( moduleSource, outputSwf, module.isOptimize(), null ); // optimizationContext)

            fModules.add( fModule );
        }

        return fModules.toArray( new Module[0] );
    }

    public IPath getOutputFolder()
    {
        // Convert to relative path
        File f = new File( adapter.getBuildDirectory() );
        File home = adapter.getProjectLocation();
        String r = PathUtil.relativePath( home, f );

        return new Path( r );
    }

    public IPath getProjectLocation()
    {
        return new Path( PathUtil.path( adapter.getProjectLocation() ) );
    }

    public String getProjectName()
    {
        return adapter.getProjectName();
    }

    public String getProjectUUID()
    {
        // TODO figure out what this is.
        return null;
    }

    public PublishSourceSettings getPublishSourceSettings()
    {
        // TODO implement this
        return null;
    }

    public String getRootURL()
    {
        // TODO I have no idea what this is.
        return null;
    }

    public String getServerContextRoot()
    {
        return adapter.getContextRoot();
    }

    public IClassPathEntry[] getSourceAttachmentPaths()
    {
        // This will attach source to FlashBuilder debugging.
        // I don't think we need this because we can attach source
        // to the individual SWC files instead.
        return null;
    }

    public IPath getSourceFolder()
    {
        // same as getMainSourceFolder
        return getMainSourceFolder();
    }

    public IClassPathEntry[] getSourcePath()
    {
        final File[] files = adapter.getSourcePath();
        
        return ClassPathUtils.getSourcePaths( settings, files );
        
        /*
        final Set<IClassPathEntry> entries = new LinkedHashSet<IClassPathEntry>();

        final File home = adapter.getProjectLocation();

        for ( int i = 0; i < files.length; i++ )
        {
            // ignore paths that match main source path
            if ( files[i].toString().equals( adapter.getMainSourcePath() ) )
                continue;

            final File f = files[i];

            // make path relative
            final String path = PathUtil.relativePath( home, f );

            final IClassPathEntry entry = ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_PATH, path, settings );

            entries.add( entry );
        }

        return entries.toArray( new IClassPathEntry[0] );
        */
    }

    public FlashPlayerVersion getTargetPlayerVersion()
    {
        // Get version from FlexMojos configuration
        String version = adapter.getTargetPlayer();

        // Convert to appropriate type and format
        return FlexUtil.getTargetPlayer( version );
    }

    public IPath getUnresolvedOutputFolderPath()
    {
        // TODO no idea.
        return null;
    }

    public void invalidateCaches()
    {
        // not used
    }

    public boolean isAnalyticsEnabled()
    {
        // TODO no idea.
        return false;
    }

    public boolean isAutoRSLOrdering()
    {
        return ( adapter.getAutoSortRsls() == null ) ? true : adapter.getAutoSortRsls();
    }

    public boolean isCopyDependentFiles()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isEnableHistoryManagement()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isFlex3CompatibilityMode()
    {
        return "3.0".equals( adapter.getCompatibilityVersion() );
    }

    public boolean isGenerateAccessibleSWF()
    {
        return ( adapter.getAccessible() == null ) ? false : adapter.getAccessible();
    }

    public boolean isGenerateHTMLWrappers()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isHTMLExpressInstall()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isHTMLPlayerVersionCheck()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isIncludeNetmonSwc()
    {
        // we can safely ignore this and let maven add dependency.
        return false;
    }

    public boolean isLocalOutputFolder()
    {
        // TODO no idea
        return false;
    }

    public boolean isShowWarnings()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isStrictCompile()
    {
        return ( adapter.getStrict() == null ) ? false : adapter.getStrict();
    }

    public boolean isUseDebugRSLSwfs()
    {
        // TODO not sure this can map to FlexMojos. Just return true for now.
        return true;
    }

    public boolean isUseFTEInMXComponents()
    {
        // TODO I don't think this maps to FlexMojos
        return false;
    }

    public boolean isUseMinimumPlayerVersionForSDK()
    {
        // TODO I don't think this maps to FlexMojos
        return false;
    }

    public boolean isValidateFlashCatalystCompatibility()
    {
        // TODO I don't think this maps to FlexMojos
        return false;
    }

    public boolean isVerifyDigests()
    {
        return adapter.getVerifyDigests();
    }

    public boolean useAIRConfig()
    {
        return adapter.isAirProject();
    }

    // -----------------------------------------------
    // End IActionScriptProjectSettings
    // -----------------------------------------------

    protected Boolean getAutoRSLOrdering()
    {
        return adapter.getAutoSortRsls();
    }

    protected Boolean getGenerateAccessibleSWF()
    {
        return adapter.getAccessible();
    }

    protected Boolean getShowWarnings()
    {
        return !adapter.getCompilerWarnings().isEmpty();
    }

    protected Version getSdkVersion()
    {
        Version version = null;

        Collection<Artifact> globals = adapter.getGlobalArtifact();
        Artifact global = globals.iterator().next();

        if ( global != null )
        {
            version = MavenUtils.getVersionFromArtifact( global );
        }

        return version;

    }
    
	public File[] getForceRSLs() {
		// TODO Auto-generated method stub
		return null;
	}

	public File getTemporaryActionScriptPropertiesFile() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isRemoveUnusedRSL() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/* - FB 4.5.0 i62 */
    public ApplicationDomainCollection getApplicationDomains()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
