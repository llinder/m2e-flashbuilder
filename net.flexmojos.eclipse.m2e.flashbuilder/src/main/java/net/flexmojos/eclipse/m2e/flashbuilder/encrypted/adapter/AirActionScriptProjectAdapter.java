package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.air.ApolloProjectCore;
import com.adobe.flexbuilder.project.air.IApolloActionScriptProject;
import com.adobe.flexbuilder.project.air.IMutableApolloActionScriptProjectSettings;
import com.adobe.flexbuilder.project.air.IMutableApolloBuildTargetSettings;
import com.adobe.flexbuilder.project.air.internal.ApolloActionScriptProject;

public class AirActionScriptProjectAdapter
    extends AbstractProjectAdapter<IApolloActionScriptProject, IMutableApolloActionScriptProjectSettings>
{

    public AirActionScriptProjectAdapter( final MojoExecution exec )
    {
        super( exec );
    }

    public IApolloActionScriptProject createProject( IProject project, IMutableActionScriptProjectSettings context,
                                                     IProgressMonitor monitor )
        throws CoreException
    {

        return getProject( project, (IMutableApolloActionScriptProjectSettings) context, monitor );
    }

    public IMutableApolloActionScriptProjectSettings applyMavenConfiguration( IProject project,
                                                                              IMutableActionScriptProjectSettings context,
                                                                              IClassPathDescriptor classpath,
                                                                              ProjectConfigurationRequest request,
                                                                              IProgressMonitor monitor )
    {
        final MojoExecution exec = getExecution();
        if ( exec != null )
        {
            final MxmlcMojoAdapter mojoAdapter = new MxmlcMojoAdapter( monitor, request.getMavenSession(), exec );

            final AirActionScriptProjectSettingsAdapter settingsAdapter =
                new AirActionScriptProjectSettingsAdapter( project, mojoAdapter, context, classpath );

            settingsAdapter.applyConfiguration();
        }

        return (IMutableApolloActionScriptProjectSettings) context;
    }

    @Override
    protected IApolloActionScriptProject getProject( IProject project,
                                                     IMutableApolloActionScriptProjectSettings context,
                                                     IProgressMonitor monitor )
        throws CoreException
    {
        final IApolloActionScriptProject apolloProject = new ApolloActionScriptProject( context, project, monitor );

        Assert.isNotNull( project, "Unable to initialize AIR ActionScript Project." );

        return apolloProject;
    }

    protected IMutableApolloActionScriptProjectSettings getSettings( final IApolloActionScriptProject project )
    {
        final IMutableApolloActionScriptProjectSettings settings =
            (IMutableApolloActionScriptProjectSettings) project.getProjectSettingsClone();
        return settings;
    }

    public IMutableApolloActionScriptProjectSettings createContext( IProject project )
    {
        // Create new settings
        IMutableApolloActionScriptProjectSettings settings =
            ApolloProjectCore.createApolloActionScriptSettings( project.getName(), project.getLocation() );

        IMutableApolloBuildTargetSettings buildSettings = ApolloProjectCore.createBuildTargetSettings( "default" );
        settings.setBuildTargetSettings( "default", buildSettings );

        return settings;
    }

}
