package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.FlexServerType;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.air.ApolloProjectCore;
import com.adobe.flexbuilder.project.air.IApolloProject;
import com.adobe.flexbuilder.project.air.IMutableApolloBuildTargetSettings;
import com.adobe.flexbuilder.project.air.IMutableApolloProjectSettings;
import com.adobe.flexbuilder.project.air.internal.ApolloProject;

public class AirProjectAdapter
    extends AbstractProjectAdapter<IApolloProject, IMutableApolloProjectSettings>
{

    public AirProjectAdapter( final MojoExecution exec )
    {
        super( exec );
    }

    public IApolloProject createProject( IProject project, IMutableActionScriptProjectSettings context,
                                         IProgressMonitor monitor )
        throws CoreException
    {

        return getProject( project, (IMutableApolloProjectSettings) context, monitor );
    }

    public IMutableApolloProjectSettings applyMavenConfiguration( IProject project,
                                                                  IMutableActionScriptProjectSettings context,
                                                                  IClassPathDescriptor classpath,
                                                                  ProjectConfigurationRequest request,
                                                                  IProgressMonitor monitor )
    {
        final MojoExecution exec = getExecution();
        if ( exec != null )
        {
            final MxmlcMojoAdapter mojoAdapter = new MxmlcMojoAdapter( monitor, request.getMavenSession(), exec );

            final AirProjectSettingsAdapter settingsAdapter =
                new AirProjectSettingsAdapter( project, mojoAdapter, context, classpath );

            settingsAdapter.applyConfiguration();
        }

        return (IMutableApolloProjectSettings) context;
    }

    @Override
    protected IApolloProject getProject( IProject project, IMutableApolloProjectSettings context,
                                         IProgressMonitor monitor )
        throws CoreException
    {
        final IApolloProject apolloProject = new ApolloProject( context, project, monitor );

        Assert.isNotNull( project, "Unable to initialize AIR Project." );

        return apolloProject;
    }

    protected IMutableApolloProjectSettings getSettings( final IApolloProject project )
    {
        final IMutableApolloProjectSettings settings =
            (IMutableApolloProjectSettings) project.getProjectSettingsClone();
        return settings;
    }

    public IMutableApolloProjectSettings createContext( IProject project )
    {
        // Create new settings
        IMutableApolloProjectSettings settings =
            ApolloProjectCore.createApolloSettings( project.getName(), project.getLocation(), FlexServerType.NO_SERVER );

        IMutableApolloBuildTargetSettings buildSettings = ApolloProjectCore.createBuildTargetSettings( "default" );
        settings.setBuildTargetSettings( "default", buildSettings );

        return settings;
    }

}
