package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.eclipse.core.resources.IProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.air.IApolloBuildTargetSettings;
import com.adobe.flexbuilder.project.air.IApolloProjectSettings;
import com.adobe.flexbuilder.project.air.IMutableApolloProjectSettings;

public class AirProjectSettingsAdapter
    extends FlexSettingsAdapter
    implements IApolloProjectSettings
{
    @SuppressWarnings( "unused" )
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.adapter.AirProjectSettingsAdapter" );
    
    private IMutableApolloProjectSettings settings;
    
    public AirProjectSettingsAdapter( IProject project, MxmlcMojoAdapter mojoAdapter,
                                      IMutableActionScriptProjectSettings context, IClassPathDescriptor classpath )
    {
        super( project, mojoAdapter, context, classpath );
        
        if( settings instanceof IMutableApolloProjectSettings )
            this.settings = (IMutableApolloProjectSettings)context;
        else
            throw new IllegalArgumentException( "Expected instance of IMutableApolloProjectSettings but was "+settings.getClass().getSimpleName() );
    }

    // -----------------------------------------------
    // Start IApolloProjectSettings
    // -----------------------------------------------

    @Override
    public IApolloBuildTargetSettings getBuildTargetSettings( String targetName )
    {
        // no-op
        return null;
    }
}
