package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.encrypted.adapters.CompcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.IFlexBasedProject;
import com.adobe.flexbuilder.project.IFlexLibraryProject;
import com.adobe.flexbuilder.project.IMutableFlexLibraryProjectSettings;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.internal.FlexLibraryCore;
import com.adobe.flexbuilder.project.internal.FlexLibraryProject;

public class FlexLibraryProjectAdapter
    extends AbstractProjectAdapter<IFlexLibraryProject, IMutableFlexLibraryProjectSettings>
{

    public FlexLibraryProjectAdapter( final MojoExecution exec )
    {
        super( exec );
    }

    public IFlexLibraryProject createProject( IProject project, IMutableActionScriptProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        return getProject( project, (IMutableFlexLibraryProjectSettings)context, monitor );
    }

    public IMutableFlexLibraryProjectSettings applyMavenConfiguration( IProject project,
                                                                       IMutableActionScriptProjectSettings context,
                                                                       IClassPathDescriptor classpath,
                                                                       ProjectConfigurationRequest request,
                                                                       IProgressMonitor monitor )
    {
        // final IMutableFlexLibraryProjectSettings settings =
        // (IMutableFlexLibraryProjectSettings) flashBuilderProject.getProjectSettings();

        context.setGenerateHTMLWrappers( false );

        // Flex Library Project SWC compilation configuration
        MojoExecution exec = getExecution();
        if ( exec != null )
        {
            final CompcMojoAdapter mojoAdapter =
                new CompcMojoAdapter( monitor, request.getMavenSession(), exec );

            final FlexLibrarySettingsAdapter settingsAdapter = 
                            new FlexLibrarySettingsAdapter( project, mojoAdapter, context, classpath );
           
            settingsAdapter.applyConfiguration();
        }

        return (IMutableFlexLibraryProjectSettings)context;
    }

    @Override
    protected IFlexLibraryProject getProject( IProject project, IMutableFlexLibraryProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        // Create and return new Flex Project
        final IFlexBasedProject flexLibProject = new FlexLibraryProject( context, project, monitor );

        Assert.isNotNull( project, "Unable to initialize Flex Library Project." );

        return (IFlexLibraryProject) flexLibProject;
    }

    protected IMutableFlexLibraryProjectSettings getSettings( final IFlexLibraryProject project )
    {
        // final IMutableFlexLibraryProjectSettings settings =
        // (IMutableFlexLibraryProjectSettings) project.getFlexLibraryProjectSettings();

        final IMutableFlexLibraryProjectSettings settings = project.getFlexLibraryProjectSettingsClone();

        return settings;
    }

    public IMutableFlexLibraryProjectSettings createContext( IProject project )
    {
        // Create new settings
        IMutableFlexLibraryProjectSettings settings =
            FlexLibraryCore.//createProjectDescription( project.getName(), project.getLocation(), false, false );
            createProjectDescription( project.getName(), project.getLocation(), false, false, false );

        return settings;
    }

}
