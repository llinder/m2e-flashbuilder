package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.flexmojos.eclipse.m2e.encrypted.adapters.CompcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.compiler.IIncludeFile;
import org.sonatype.flexmojos.compiler.INamespace;
import org.sonatype.flexmojos.util.PathUtil;

import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.IFlexLibraryProjectSettings;
import com.adobe.flexbuilder.project.IMutableFlexLibraryProjectSettings;
import com.adobe.flexbuilder.project.XMLNamespaceManifestPath;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class FlexLibrarySettingsAdapter extends ActionScriptSettingsAdapter implements IFlexLibraryProjectSettings
{
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.adapter.FlexLibrarySettingsAdapter" );
    
    private final CompcMojoAdapter adapter;
    private final IMutableFlexLibraryProjectSettings settings;
    
    private String[] includeClasses;
    private Map<IPath,IPath> includeFiles;
    private XMLNamespaceManifestPath[] manifestPaths;
    
    public FlexLibrarySettingsAdapter( IProject project, CompcMojoAdapter mojoAdapter,
                                       IMutableActionScriptProjectSettings context, IClassPathDescriptor classpath )
    {
        super( project, mojoAdapter, context, classpath );
       
        this.adapter = mojoAdapter;
        
        if( context instanceof IMutableFlexLibraryProjectSettings )
            this.settings = (IMutableFlexLibraryProjectSettings)context;
        else
            throw new IllegalArgumentException( "Expected instance of IMutableFlexLibraryProjectSettings but was "+context.getClass().getSimpleName() );
    }

	
	@Override
	public void applyConfiguration()
	{	
		super.applyConfiguration();
		
		settings.setIncludeClasses( getIncludeClasses() );
		
		settings.setIncludeAllClasses( isIncludeAllClasses() );
		
		settings.setManifestPaths( getManifestPaths() );
		
		settings.setIncludeFiles( getIncludeFiles() );
	}

	public File getFlexLibraryProjectDescriptionFile()
	{
		// not used.
		return null;
	}

	public String[] getIncludeClasses()
	{
		if( includeClasses != null )
			return includeClasses;
		
		List<String> classes = adapter.getIncludeClasses();
		
		return includeClasses = ( classes != null ) ? classes.toArray( new String[0] ) : new String[] {};
	}

	public Map<IPath, IPath> getIncludeFiles()
	{
		if( includeFiles != null )
			return includeFiles;
		
		final Map<IPath, IPath> fileMap = new HashMap<IPath, IPath>();
		
		final IIncludeFile files[] = adapter.getIncludeFile();
		if( files != null )
		{
			for( IIncludeFile file : files )
			{
				// Flash Builder only supports file includes that are relative to a source path
				
				final String includePath = file.path();
				boolean matched = false;
				
				// Build list of source paths
				final List<IPath> sourcePaths = new ArrayList<IPath>();
				sourcePaths.add( getMainSourceFolder() );
				IClassPathEntry[] sourceEntries = getSourcePath();
				for( int i=0; i<sourceEntries.length; i++ )
					sourcePaths.add( sourceEntries[i].getSourcePath() );
					
				// Find source path match
				for( IPath path : sourcePaths )
				{
					final String sourcePath = PathUtil.path( path.toFile() );
					if( includePath.contains( sourcePath ) )
					{
					    // Make paths relative to the source directory (usually ./target/classes)
					    final IPath key = new Path( includePath.substring( sourcePath.length()+1 ) );
                        final IPath value = new Path( file.path().substring( sourcePath.length()+1 ) );
						//final IPath key = new Path( new Path( includePath ).lastSegment() );
						//final IPath value = new Path( file.path().substring( sourcePath.length()+1 ) );
						
						fileMap.put( key, value );
						matched = true;
						break;
					}
				}

				if( !matched )
				{
				    if( log.isErrorEnabled() )
				        log.error( "File include '"+includePath+"' was not found in any source paths."  );
				}
			}
		}
		
		return includeFiles = fileMap;
	}

	public XMLNamespaceManifestPath[] getManifestPaths()
	{
		if( manifestPaths != null )
			return manifestPaths;
		
		List<XMLNamespaceManifestPath> paths = new ArrayList<XMLNamespaceManifestPath>();
		
		final INamespace[] namespaces = adapter.getNamespace();
		
		if( namespaces != null )
		{
			
			for( INamespace namespace : namespaces )
			{
				IPath manifestPath = new Path( namespace.manifest() );
				
				paths.add( new XMLNamespaceManifestPath( namespace.uri(), manifestPath ) );
			}
		}
		
		return manifestPaths = paths.toArray( new XMLNamespaceManifestPath[0] );
	}

	public boolean isIncludeAllClasses()
	{
		String[] includes = getIncludeClasses();
		return ( includes.length < 1 );
	}

	public boolean useMultiPlatformConfig()
	{
		// TODO not sure what this is for.
		return false;
	}
	

}
