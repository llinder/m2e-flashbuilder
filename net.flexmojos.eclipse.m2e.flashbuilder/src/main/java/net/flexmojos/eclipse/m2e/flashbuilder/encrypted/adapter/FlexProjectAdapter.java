package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.FlexProjectManager;
import com.adobe.flexbuilder.project.FlexServerType;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.IFlexProject;
import com.adobe.flexbuilder.project.IMutableFlexProjectSettings;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.internal.FlexProject;

public class FlexProjectAdapter
    extends AbstractProjectAdapter<IFlexProject, IMutableFlexProjectSettings>
{

    public FlexProjectAdapter( final MojoExecution exec )
    {
        super( exec );
    }

    public IFlexProject createProject( IProject project, IMutableActionScriptProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        return getProject( project, (IMutableFlexProjectSettings)context, monitor );
    }

    public IMutableFlexProjectSettings applyMavenConfiguration( IProject project,
                                                                IMutableActionScriptProjectSettings context,
                                                                IClassPathDescriptor classpath,
                                                                ProjectConfigurationRequest request,
                                                                IProgressMonitor monitor )
    {
        context.setGenerateHTMLWrappers( false );

        // Apply swf settings
        MojoExecution exec = getExecution();
        if ( exec != null )
        {
            final MxmlcMojoAdapter mojoAdapter =
                new MxmlcMojoAdapter( monitor, request.getMavenSession(), exec );

            final FlexSettingsAdapter settingsAdapter = 
                            new FlexSettingsAdapter( project, mojoAdapter, context, classpath );
            
            settingsAdapter.applyConfiguration();
        }

        return (IMutableFlexProjectSettings)context;
    }

    @Override
    protected IFlexProject getProject( IProject project, IMutableFlexProjectSettings context, IProgressMonitor monitor )
        throws CoreException
    {
        // Create and return new Flex Project
        final IFlexProject flexProject = new FlexProject( context, project, monitor );

        Assert.isNotNull( project, "Unable to initialize Flex Project." );
        
        // Flex project is adding 'libs' folder to classpath... need to remove it.
        final IClassPathEntry[] libPaths = context.getLibraryPath();
        final List<IClassPathEntry> finalLibPaths = new ArrayList<IClassPathEntry>();
        for( int i=0; i<libPaths.length; i++ )
        {
            final IClassPathEntry entry = libPaths[i];
            if( !( entry.getKind() == IClassPathEntry.KIND_PATH ) )
                finalLibPaths.add( entry );
        }
        
        context.setLibraryPath( finalLibPaths.toArray( new IClassPathEntry[0] ) );
        
        flexProject.setProjectDescription( context, monitor );

        return flexProject;
    }

    protected IMutableFlexProjectSettings getSettings( IFlexProject project )
    {
        // final IMutableFlexProjectSettings settings = (IMutableFlexProjectSettings) project.getFlexProjectSettings();

        final IMutableFlexProjectSettings settings = project.getFlexProjectSettingsClone();
        return settings;
    }

    public IMutableFlexProjectSettings createContext( IProject project )
    {
        // Create new settings
        IMutableFlexProjectSettings settings =
            FlexProjectManager.createFlexProjectDescription( project.getName(), project.getLocation(), false,
                                                             FlexServerType.NO_SERVER );
        return settings;
    }

}
