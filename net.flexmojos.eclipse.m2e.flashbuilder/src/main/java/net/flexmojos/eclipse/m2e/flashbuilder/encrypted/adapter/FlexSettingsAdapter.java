package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import java.io.File;
import java.net.URL;

import net.flexmojos.eclipse.m2e.encrypted.adapters.MxmlcMojoAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.flexbuilder.project.FlexServerType;
import com.adobe.flexbuilder.project.IFlexProjectSettings;
import com.adobe.flexbuilder.project.IMutableFlexProjectSettings;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class FlexSettingsAdapter
    extends ActionScriptSettingsAdapter
    implements IFlexProjectSettings
{
    @SuppressWarnings( "unused" )
    private static final Logger log =
                    LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.adapter.FlexSettingsAdapter" );
    
    private IFlexProjectSettings settings;
    
    public FlexSettingsAdapter( IProject project, MxmlcMojoAdapter mojoAdapter,
                                IMutableActionScriptProjectSettings context, IClassPathDescriptor classpath )
    {
        super( project, mojoAdapter, context, classpath );
        
        if ( context instanceof IMutableFlexProjectSettings )
            this.settings = (IMutableFlexProjectSettings) context;
        else
            throw new IllegalArgumentException( "Expected instance of IMutableFlexProjectSettings but was "
                            + settings.getClass().getSimpleName() );
        
    }

    @Override
    public void applyConfiguration()
    {
        super.applyConfiguration();
    }

    // -----------------------------------------------
    // Start IFlexProjectSettings
    // -----------------------------------------------
    public boolean getAspProjectType()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public String getCFRootFolder()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public IPath getCFWebInfParent()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public String getDataModelLocation()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean getEnableServiceManager()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public File getFlexProjectDescriptionFile()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public FlexServerType getFlexServerType()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public String getFlexWarLocation()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public int getServerFeatures()
    {
        // TODO Auto-generated method stub
        return 0;
    }

    public IPath getServerRoot()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public URL getServerRootURL()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public String getUIAuthoringChoice()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean hasBlazeDS()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean hasLCDS()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean hasServer()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean hasServerFeatures( int arg0 )
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean hasServicesCompilerArguments()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public IStatus validate()
    {
        // TODO Auto-generated method stub
        return null;
    }

    // -----------------------------------------------
    // End IFlexProjectSettings
    // -----------------------------------------------

    /*
     * -FB 4.5.0 i62 public ApplicationDomainCollection getApplicationDomains() { // TODO Auto-generated method stub
     * return null; }
     */

    public File[] getForceRSLs()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public File getTemporaryActionScriptPropertiesFile()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isRemoveUnusedRSL()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public File getTemporaryFlexPropertiesFile()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
