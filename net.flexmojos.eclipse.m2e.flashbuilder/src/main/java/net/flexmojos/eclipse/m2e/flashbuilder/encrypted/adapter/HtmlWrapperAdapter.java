package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import org.apache.maven.project.MavenProject;
import org.sonatype.flexmojos.compiler.extra.IHtmlWrapperConfiguration;

import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class HtmlWrapperAdapter
{
    static final String HTML_TEMPLATE_CLIENT_SIDE_DETECTION =       "embed:client-side-detection";
    static final String HTML_TEMPLATE_CLIENT_SIDE_DETECTION_HIST =  "embed:client-side-detection-with-history";
    static final String HTML_TEMPLATE_EXPRESS_INSTALL =             "embed:express-installation";
    static final String HTML_TEMPLATE_EXPRESS_INSTALL_HIST =        "embed:express-installation-with-history";
    static final String HTML_TEMPLATE_NO_PLAYER_DETECTION =         "embed:no-player-detection";
    static final String HTML_TEMPLATE_NO_PLAYER_DETECTION_HIST =    "embed:no-player-detection-with-history";
    
    private IHtmlWrapperConfiguration configuration;

    public HtmlWrapperAdapter( MavenProject project, IHtmlWrapperConfiguration config )
    {
        this.configuration = config;
    }

    public IMutableActionScriptProjectSettings applyWrapperConfiguration( IMutableActionScriptProjectSettings settings )
    {
        final String templateURI = configuration.getTemplateURI();

        if( templateURI == null )
        {
            settings.setHTMLExpressInstall( false );
            settings.setHTMLPlayerVersionCheck( false );
            settings.setEnableHistoryManagement( false );
            settings.setGenerateHTMLWrappers( false );
        }
        else
        {
            settings.setGenerateHTMLWrappers( true );
            
            if ( HTML_TEMPLATE_CLIENT_SIDE_DETECTION.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( false );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( true );
            }
            else if ( HTML_TEMPLATE_CLIENT_SIDE_DETECTION_HIST.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( true );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( true );
            }
            else if ( HTML_TEMPLATE_EXPRESS_INSTALL.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( false );
                settings.setHTMLExpressInstall( true );
                settings.setHTMLPlayerVersionCheck( true );
            }
            else if ( HTML_TEMPLATE_EXPRESS_INSTALL_HIST.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( true );
                settings.setHTMLExpressInstall( true );
                settings.setHTMLPlayerVersionCheck( true );
            }
            else if ( HTML_TEMPLATE_NO_PLAYER_DETECTION.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( false );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( false );
            }
            else if ( HTML_TEMPLATE_NO_PLAYER_DETECTION_HIST.equals( templateURI ) )
            {
                settings.setEnableHistoryManagement( true );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( false );
            }
            else if ( templateURI.indexOf( "zip:" ) > -1 )
            {
                settings.setEnableHistoryManagement( false );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( false );
            }
            else if ( templateURI.indexOf( "folder:" ) > -1 )
            {
                settings.setEnableHistoryManagement( false );
                settings.setHTMLExpressInstall( false );
                settings.setHTMLPlayerVersionCheck( false );
            }
        }

        return settings;
    }    
}
