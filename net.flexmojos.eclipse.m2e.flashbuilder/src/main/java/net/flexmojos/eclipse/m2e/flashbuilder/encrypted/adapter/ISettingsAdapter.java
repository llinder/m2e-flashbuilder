package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;


public interface ISettingsAdapter
{
    public void applyConfiguration();
}
