package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;

import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public interface ProjectAdapter<T extends IActionScriptProject, S extends IMutableActionScriptProjectSettings>
{
    public T createProject( IProject project, IMutableActionScriptProjectSettings settings, IProgressMonitor monitor )
        throws CoreException;
    
    public S createContext( IProject project );

    public IMutableActionScriptProjectSettings applyMavenConfiguration( IProject project,
                                                                        IMutableActionScriptProjectSettings context,
                                                                        IClassPathDescriptor classpath,
                                                                        ProjectConfigurationRequest request,
                                                                        IProgressMonitor monitor );
}
