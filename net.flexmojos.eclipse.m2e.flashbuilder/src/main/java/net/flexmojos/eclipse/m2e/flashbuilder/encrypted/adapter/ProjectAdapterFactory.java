package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter;


import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.CoreMatchers.allOf;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.artifactId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.groupId;
import static org.sonatype.flexmojos.matcher.artifact.ArtifactMatcher.type;

import java.util.Set;

import net.flexmojos.eclipse.m2e.common.FlexConstants;
import net.flexmojos.eclipse.m2e.ide.encrypted.adapter.BaseProjectAdapter;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class ProjectAdapterFactory extends BaseProjectAdapter
{
    private static final Logger log = LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.adapter.ProjectAdapterFactory" );
    
    /**
     * Returns the appropriate Flashbuilder project adapter for the given maven project.
     * 
     * @param project Maven project
     * @param exec Maven execution
     * @return Flashbuilder project adapter
     */
	public static ProjectAdapter<? extends IActionScriptProject, ? extends IMutableActionScriptProjectSettings> getAdapter( MavenProject mavenProject, MojoExecution exec )
	{
		ProjectAdapter<? extends IActionScriptProject, ? extends IMutableActionScriptProjectSettings> strategy = null;
		
		final ProjectType type = getProjectType( mavenProject );
		
		switch( type )
		{
		case ACTIONSCRIPT_APPLICATION:
			strategy = new ActionScriptProjectAdapter( exec );
			break;
		case ACTIONSCRIPT_LIBRARY:
			strategy = new FlexLibraryProjectAdapter( exec );
			break;
		case FLEX_APPLICATION:
			strategy = new FlexProjectAdapter( exec );
			break;
		case FLEX_LIBRARY:
			strategy = new FlexLibraryProjectAdapter( exec );
			break;
		case AIR_ACTIONSCRIPT_APPLICATION:
		    strategy = new AirActionScriptProjectAdapter( exec );
		    break;
		case AIR_APPLICATION:
			strategy = new AirProjectAdapter( exec );
			break;
		case AIR_LIBRARY:
		    strategy = new FlexLibraryProjectAdapter( exec );
			break;
		}
		
		return strategy;
	}
	
	/**
	 * Evaluates what type of Flashbuilder project should be
	 * created using various information on the POM.
	 * 
	 * @return
	 */
	private static ProjectType getProjectType( final MavenProject project )
	{
		ProjectType type = null;
		
		boolean isActionScriptOntly = isActionScriptOnly( project );
		
		// Applications
		if( project.getPackaging().equals( FlexExtension.AIR ) )
		{
		    if( isActionScriptOntly )
            {
                type = ProjectType.AIR_ACTIONSCRIPT_APPLICATION;
            }
		    else
		    {
		        type = ProjectType.AIR_APPLICATION;
		    }
		}
		else if( project.getPackaging().equals( FlexConstants.SWF ) )
		{
		    // Apollo
            if( isApollo( project ) )
            {
                if( isActionScriptOntly )
                {
                    type = ProjectType.AIR_ACTIONSCRIPT_APPLICATION;
                }
                else
                {
                    type = ProjectType.AIR_APPLICATION;
                }
            }
			// Actionscript only
		    else if( isActionScriptOntly )
			{
				type = ProjectType.ACTIONSCRIPT_APPLICATION;
			}
			// Flex Web
			else
			{
				type = ProjectType.FLEX_APPLICATION;
			}
		}
		// Libraries
		else if( project.getPackaging().equals( FlexConstants.SWC ) )
		{
			// Actionscript only lib
			if( isActionScriptOntly )
			{
				type = ProjectType.ACTIONSCRIPT_LIBRARY;
			}
			// Apollo lib
			else if( isApollo( project ) )
			{
				type = ProjectType.AIR_LIBRARY;
			}
			// Flex Web lib
			else
			{
				type = ProjectType.FLEX_LIBRARY;
			}
		}
		// Unknown
		else
		{
		    if( log.isWarnEnabled() )
		        log.warn( "Unable to determine Flashbuilder project type. Defaulting to regular Flex Web Application." );
		    
			type = ProjectType.FLEX_APPLICATION;
		}
				
		return type;
	}
	
	public static enum ProjectType
	{
		FLEX_APPLICATION,
		FLEX_LIBRARY,
		ACTIONSCRIPT_APPLICATION,
		ACTIONSCRIPT_LIBRARY,
		AIR_APPLICATION,
        AIR_LIBRARY,
		AIR_ACTIONSCRIPT_APPLICATION,
		AIR_ACTIONSCRIPT_LIBRARY;
	}
}
