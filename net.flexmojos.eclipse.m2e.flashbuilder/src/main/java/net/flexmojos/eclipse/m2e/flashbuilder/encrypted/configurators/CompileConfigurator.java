package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators;

import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_COMPILE_SWC;
import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_COMPILE_SWF;
import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_WRAPPER;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_ARTIFACTID;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_GROUPID;

import java.io.File;
import java.util.List;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.configurator.AbstractEncryptedConfigurator;
import net.flexmojos.eclipse.m2e.configurator.InternalConfigurator;
import net.flexmojos.eclipse.m2e.flashbuilder.IClassPathDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.ClassPathDescriptor;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter.HtmlWrapperAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter.ProjectAdapter;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.adapter.ProjectAdapterFactory;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.ClassPathUtils;

import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.sonatype.flexmojos.compiler.extra.IGeneratorConfiguration;
import org.sonatype.flexmojos.compiler.extra.IHtmlWrapperConfiguration;
import org.sonatype.flexmojos.plugin.common.FlexExtension;

import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;

public class CompileConfigurator
    extends InternalConfigurator
{

    public CompileConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        super( fbConfigurator );
    }

    @Override
    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        super.configure( request, monitor );

        final IProject project = request.getProject();
        final MavenProject mavenProject = request.getMavenProject();
        final MojoExecution compileExecution = getExecution( getCompileGoalForPackaging( mavenProject.getPackaging() ) );

        monitor.setTaskName( "Configuring FlexMojos project " + request.getProject().getName() );

        final ProjectAdapter<? extends IActionScriptProject, ? extends IMutableActionScriptProjectSettings> adapter =
            ProjectAdapterFactory.getAdapter( mavenProject, compileExecution );

        IMutableActionScriptProjectSettings settings = adapter.createContext( project );

        final IClassPathDescriptor classpath = new ClassPathDescriptor( settings );

        settings = adapter.applyMavenConfiguration( project, settings, classpath, request, monitor );
        
        // Apply wrapper mojo config to Flashbuilder settings.
        final IHtmlWrapperConfiguration wrapperConfig = getMojoConfiguration( IHtmlWrapperConfiguration.class, monitor, request.getMavenSession() ); //super.getExecution( "wrapper" );
        if ( wrapperConfig != null )
            settings = new HtmlWrapperAdapter( mavenProject, wrapperConfig ).applyWrapperConfiguration( settings );

        // Apply generate mojo config to Flashbuilder settings
        final IGeneratorConfiguration genConfig = getMojoConfiguration( IGeneratorConfiguration.class, monitor, request.getMavenSession() ); //super.getExecution( "generate" );
        if( genConfig != null )
        {
            final IClassPathEntry[] sourcePaths = ClassPathUtils.getSourcePaths( settings, new File[] { genConfig.getOutputDirectory(), genConfig.getBaseOutputDirectory() } );
            settings.setSourcePath( sourcePaths );
        }
        

        final IActionScriptProject flashBuilderProject = adapter.createProject( project, settings, monitor );

        
        flashBuilderProject.setProjectDescription( settings, monitor );

        // Rebuild the project (doing this will cause m2e to flag first import as changed... 
        // after lots of investigation it appears to be an order of operations issue.
        // After commenting this out everything works fine and the flash builder project actually builds on its own after being created above.
        //request.getProject().build( IncrementalProjectBuilder.FULL_BUILD, monitor );
    }
    
    protected String getCompileGoalForPackaging( String packaging ) throws CoreException
    {
        if ( FlexExtension.SWF.equals( packaging ) || FlexExtension.AIR.equals( packaging ) )
        {
            return GOAL_COMPILE_SWF;
        }
        else if ( FlexExtension.SWC.equals( packaging ) )
        {
            return GOAL_COMPILE_SWC;
        }
        
        throw new CoreException( new Status( IStatus.ERROR, FlexMojosPlugin.PLUGIN_ID, "No known goal for packaging "+packaging) );
    }

    @Override
    protected List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return request.getMavenProjectFacade().
                        getMojoExecutions( PLUGIN_GROUPID,
                                           PLUGIN_ARTIFACTID,
                                           monitor,
                                           GOAL_COMPILE_SWF,
                                           GOAL_COMPILE_SWC,
                                           GOAL_WRAPPER );
    }

    @Override
    protected boolean isEnterprise()
    {
        return false;
    }
}
