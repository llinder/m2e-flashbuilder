package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators;

import static net.flexmojos.eclipse.m2e.common.FMEConstants.GOAL_COMPILE_SWC;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.GOAL_COMPILE_SWF;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.GOAL_WRAPPER;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.PLUGIN_ARTIFACTID;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.PLUGIN_GROUPID;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.AIR;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.SWC;
import static org.sonatype.flexmojos.plugin.common.FlexExtension.SWF;

import java.util.List;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.configurator.AbstractEncryptedConfigurator;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

public class ECompileConfigurator
    extends CompileConfigurator
{

    public ECompileConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        super( fbConfigurator );
    }

    @Override
    protected String getCompileGoalForPackaging( String packaging ) throws CoreException
    {
        if ( SWF.equals( packaging ) || AIR.equals( packaging ) )
        {
            return GOAL_COMPILE_SWF;
        }
        else if ( SWC.equals( packaging ) )
        {
            return GOAL_COMPILE_SWC;
        }
        
        throw new CoreException( new Status( IStatus.ERROR, FlexMojosPlugin.PLUGIN_ID, "No known goal for packaging "+packaging) );
    }
    
    @Override
    protected List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return request.getMavenProjectFacade().
                        getMojoExecutions( PLUGIN_GROUPID,
                                           PLUGIN_ARTIFACTID,
                                           monitor,
                                           GOAL_COMPILE_SWF,
                                           GOAL_COMPILE_SWC,
                                           GOAL_WRAPPER );
    }
    
    @Override
    protected boolean isEnterprise()
    {
        return true;
    }
}
