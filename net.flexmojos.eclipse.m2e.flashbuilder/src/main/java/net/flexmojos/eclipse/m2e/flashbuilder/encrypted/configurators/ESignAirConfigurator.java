package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators;

import static net.flexmojos.eclipse.m2e.common.FMEConstants.GOAL_SIGN_AIR;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.PLUGIN_ARTIFACTID;
import static net.flexmojos.eclipse.m2e.common.FMEConstants.PLUGIN_GROUPID;

import java.util.List;

import net.flexmojos.eclipse.m2e.configurator.AbstractEncryptedConfigurator;
import net.flexmojos.eclipse.m2e.configurator.InternalConfigurator;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;

public class ESignAirConfigurator
    extends InternalConfigurator
{

    public ESignAirConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        super( fbConfigurator );
    }

    @Override
    protected List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return request.getMavenProjectFacade().
                        getMojoExecutions( PLUGIN_GROUPID,
                                           PLUGIN_ARTIFACTID,
                                           monitor,
                                           GOAL_SIGN_AIR );
    }
    
    @Override
    protected boolean isEnterprise()
    {
        return true;
    }
}
