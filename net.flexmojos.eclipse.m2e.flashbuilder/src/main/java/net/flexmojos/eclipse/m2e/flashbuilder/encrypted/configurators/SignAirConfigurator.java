package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.configurators;

import static net.flexmojos.eclipse.m2e.common.FMConstants.GOAL_SIGN_AIR;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_ARTIFACTID;
import static net.flexmojos.eclipse.m2e.common.FMConstants.PLUGIN_GROUPID;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.flexmojos.eclipse.m2e.common.FMConstants;
import net.flexmojos.eclipse.m2e.configurator.AbstractEncryptedConfigurator;
import net.flexmojos.eclipse.m2e.configurator.InternalConfigurator;
import net.flexmojos.eclipse.m2e.encrypted.adapters.SignAirMojoAdapter;
import net.flexmojos.eclipse.m2e.encrypted.util.PathUtil;
import net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util.ClassPathUtils;

import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.flexmojos.plugin.utilities.FileInterpolationUtil;

import com.adobe.flexbuilder.project.air.ApolloProjectCore;
import com.adobe.flexbuilder.project.air.IAIRBasedProject;
import com.adobe.flexbuilder.project.air.IApolloBuildTargetSettings.IPackageType;
import com.adobe.flexbuilder.project.air.IMutableApolloActionScriptProjectSettings;
import com.adobe.flexbuilder.project.air.IMutableApolloBuildTargetSettings;

public class SignAirConfigurator
    extends InternalConfigurator
{
    private static final Logger log =
        LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.configurators.SignAirConfigurator" );

    public SignAirConfigurator( AbstractEncryptedConfigurator fbConfigurator )
    {
        super( fbConfigurator );
    }

    @Override
    public void configure( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        super.configure( request, monitor );

        final IProject project = request.getProject();
        final MojoExecution signExecution = getExecution( FMConstants.GOAL_SIGN_AIR );

        monitor.setTaskName( "Configuring Flex Mojos AIR Signing for project " + request.getProject().getName() );

        IAIRBasedProject apolloProject = ApolloProjectCore.getAIRProject( project );
        Assert.isNotNull( apolloProject, "Unexpected project type." );

        IMutableApolloActionScriptProjectSettings settings =
            (IMutableApolloActionScriptProjectSettings) apolloProject.getProjectSettingsClone();
        
        // Purge problematic libs path (for some reason reading the project is causing it to be added).
        settings.setLibraryPath( ClassPathUtils.purgeLibsPath( settings.getLibraryPath() ) );
        
        // Get sign air mojo adapter
        final SignAirMojoAdapter mojoAdapter =
            new SignAirMojoAdapter( monitor, request.getMavenSession(), signExecution );

        // Attempt to get build target settings from Flashbuilder config.
        IMutableApolloBuildTargetSettings targetSettings =
            (IMutableApolloBuildTargetSettings) settings.getBuildTargetSettingsClone( "default" );

        // If Flashbuilder config didn't provide us with a default target then create one
        if ( targetSettings == null )
            targetSettings = ApolloProjectCore.createBuildTargetSettings( "default" );

        // Apply POM configuration to build target settings.
        if ( targetSettings != null )
        {
            // TODO adjust packaging type based on POM
            targetSettings.setPackageType( IPackageType.AIR );

            // timestamp
            targetSettings.setTimestamp( ( mojoAdapter.getTimestampURL() != null ) );

            // keystore
            final File cert = mojoAdapter.getKeystore();
            if ( cert.exists() )
            {
                final IPath certPath = PathUtil.getRelativeProjectPath( project, cert.getAbsolutePath() );
                targetSettings.setCertificatePath( certPath );
            }

            settings.setBuildTargetSettings( "default", targetSettings );
        }

        // Create AIR application descriptor
        new AirDescriptor( apolloProject, request.getMavenProject() ).create( mojoAdapter.getDescriptorTemplate(), monitor );

        apolloProject.setProjectDescription( settings, monitor );
    }

    @Override
    protected List<MojoExecution> getFlexMojoExecutions( ProjectConfigurationRequest request, IProgressMonitor monitor )
        throws CoreException
    {
        return request.getMavenProjectFacade().getMojoExecutions( PLUGIN_GROUPID, PLUGIN_ARTIFACTID, monitor,
                                                                  GOAL_SIGN_AIR );
    }

    @Override
    protected boolean isEnterprise()
    {
        return false;
    }
}

/**
 * Utility class for managing the creation of an AIR application descriptor from a template file.
 */
class AirDescriptor
{
    private static final Logger log =
        LoggerFactory.getLogger( "net.flexmojos.eclipse.m2e.flashbuilder.internal.configurators.SignAirConfigurator$AirDescriptor" );

    private final MavenProject mavenProject;

    private final IAIRBasedProject project;

    protected AirDescriptor( IAIRBasedProject project, MavenProject mavenProject )
    {
        this.project = project;
        this.mavenProject = mavenProject;
    }

    public void create( File template, IProgressMonitor monitor )
    {
        if ( template == null )
        {
            if ( log.isErrorEnabled() )
                log.error( "AIR descriptor template file must not be null." );
        }
        else if ( !template.exists() )
        {
            if ( log.isErrorEnabled() )
                log.error( "AIR descriptor template file doesn't exist. " + template.getAbsolutePath() );
        }
        else
        {
            IFile mainApp = project.getMainApplication();
            IPath targetDir = project.getProject().getLocation().append( project.getProjectSettings().getMainSourceFolder() );
            
            if ( mainApp == null )
            {
                if ( log.isErrorEnabled() )
                    log.error( "Main application not found on AIR project config." );
            }
            else
            {
                final Map<String, String> props = new HashMap<String, String>();
                props.put( "output", getContent() );
                props.put( "version", getVersion() );

                if ( log.isDebugEnabled() )
                    log.debug( "Writing AIR descriptor template to direcotry " + targetDir.toString() + " with params "
                        + props );
                
                String mainFileName = mainApp.getName();
                mainFileName = mainFileName.substring( 0, mainFileName.lastIndexOf( "." ) );
                
                final File dest = new File( targetDir.toFile(), mainFileName +"-app.xml" );
                try
                {
                    FileInterpolationUtil.copyFile( template, dest, props );
                    
                    // Refresh project so that it picks up the new descriptor file.
                    project.getProject().refreshLocal( IResource.DEPTH_INFINITE, monitor );
                }
                catch ( Exception e )
                {
                    if ( log.isErrorEnabled() )
                        log.error( "Error creating AIR descriptor file. ", e );
                }
            }
        }
    }

    /**
     * Descriptor content value.
     * 
     * @return
     */
    private String getContent()
    {
        String s = project.getMainApplication().getName();
        if( s.indexOf( "mxml" ) > -1 )
            s = s.replaceAll( "mxml", "swf" );
        else if( s.indexOf( "as" ) > -1 )
            s = s.replaceAll( "as", "swf" );
        return s;
    }

    private String getVersion()
    {
        String version;
        if ( mavenProject.getArtifact().isSnapshot() )
        {
            version =
                mavenProject.getVersion().replace( "-SNAPSHOT",
                                                   "");//new SimpleDateFormat( "yyyyMMdd.HHmmss" ).format( new Date() ) );
        }
        else
        {
            version = mavenProject.getVersion();
        }

        return version;
    }
}
