package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.flexconfig;

import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.sonatype.flexmojos.compiler.ICommandLineConfiguration;
import org.sonatype.flexmojos.compiler.ICompilerConfiguration;
import org.sonatype.flexmojos.compiler.IDefine;
import org.sonatype.flexmojos.compiler.IFontsConfiguration;
import org.sonatype.flexmojos.compiler.ILanguageRange;
import org.sonatype.flexmojos.compiler.ILanguages;
import org.sonatype.flexmojos.compiler.ILocalizedDescription;
import org.sonatype.flexmojos.compiler.ILocalizedTitle;
import org.sonatype.flexmojos.compiler.IMetadataConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DefaultFlexConfigWritter
    implements IFlexConfigWritter
{
    // XML element names
    private static final String FLEX_CONFIG = "flex-config";

    private static final String COMPILER = "compiler";

    private static final String THEME = "theme";

    private static final String FILENAME = "filename";

    private static final String KEEP_ALL_TYPE_SELECTORS = "keep-all-type-selectors";

    private static final String KEEP_AS3_METADATA = "keep-as3-metadata";

    private static final String NAME = "name";

    private static final String DEFINE = "define";

    private static final String VALUE = "value";

    private static final String FONTS = "fonts";

    private static final String ADVANCED_ANTI_ALIASING = "advanced-anti-aliasing";

    private static final String FLASH_TYPE = "flash-type";

    private static final String LANGUAGES = "languages";

    private static final String LANGUAGE_RANGE = "language-range";

    private static final String LANG = "lang";

    private static final String RANGE = "range";

    private static final String LOCAL_FONT_PATHS = "local-font-paths";

    private static final String PATH_ELEMENT = "path-element";

    private static final String LOCAL_FONTS_SNAPSHOT = "local-fonts-snapshot";

    private static final String MANAGERS = "managers";

    private static final String MANAGER_CLASS = "manager-class";

    private static final String MAX_CACHED_FONTS = "max-cached-fonts";

    private static final String MAX_GLYPHS_PER_FACE = "max-glyphs-per-face";

    private static final String SWF_VERSION = "swf-version";

    private static final String METADATA = "metadata";

    private static final String METADATA_CONTRIBUTOR = "contributor";

    private static final String METADATA_CREATOR = "creator";

    private static final String METADATA_DATE = "date";

    private static final String METADATA_DESCRIPTION = "description";

    private static final String METADATA_LANGUAGE = "language";

    private static final String METADATA_LOCALIZED_DESCRIPTION = "localized-description";

    private static final String METADATA_LANG = "lang";

    private static final String METADATA_TEXT = "text";

    private static final String METADATA_LOCALIZED_TITLE = "localized-title";

    private static final String METADATA_TITLE = "title";

    private static final String METADATA_PUBLISHER = "publisher";

    private Object adapter;

    public void setMojoAdapter( Object adapter )
    {
        this.adapter = adapter;
    }

    public void writeConfig( OutputStream out )
        throws FlexConfigWritterException
    {
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        try
        {
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.newDocument();

            Element config = createFlexConfig( doc );

            doc.appendChild( config );

            DOMSource source = new DOMSource( doc );

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            t.setOutputProperty( OutputKeys.INDENT, "yes" );
            t.setOutputProperty( "{http://xml.apache.org/xslt}indent-amount", "2" );

            t.transform( source, new StreamResult( out ) );

        }
        catch ( ParserConfigurationException e )
        {
            throw new FlexConfigWritterException( e );
        }
        catch ( TransformerConfigurationException e )
        {
            throw new FlexConfigWritterException( e );
        }
        catch ( TransformerException e )
        {
            throw new FlexConfigWritterException( e );
        }
    }

    private Element createFlexConfig( Document doc )
    {
        final Element config = doc.createElement( FLEX_CONFIG );

        // -------------------------
        // Base Options
        // -------------------------
        if ( adapter instanceof ICommandLineConfiguration )
        {
            addBaseConfig( doc, config );
        }

        // -----------------
        // Compiler
        // -----------------
        if ( adapter instanceof ICompilerConfiguration )
            addCompilerConfig( doc, config );

        return config;
    }

    private void addBaseConfig( Document doc, Element config )
    {
        final ICommandLineConfiguration conf = (ICommandLineConfiguration) adapter;

        /*
         * <metadata> <title>Adobe Flex 2 Application</title> <description>http://www.adobe.com/flex</description>
         * <publisher>unknown</publisher> <creator>unknown</creator> <language>EN</language> </metadata>
         */
        // ---------------------------------
        // metadata
        // ---------------------------------
        final IMetadataConfiguration metadata = conf.getMetadataConfiguration();
        if ( metadata != null )
        {
            final Element md = doc.createElement( METADATA );

            // contributor
            final String contributors[] = metadata.getContributor();
            if ( contributors != null )
            {
                for ( int i = 0; i < contributors.length; i++ )
                {
                    final Element contributor = doc.createElement( METADATA_CONTRIBUTOR );
                    contributor.setTextContent( contributors[i] );
                    md.appendChild( contributor );
                }
            }

            // creator
            final String creators[] = metadata.getCreator();
            if ( creators != null )
            {
                for ( int i = 0; i < creators.length; i++ )
                {
                    final Element creator = doc.createElement( METADATA_CREATOR );
                    creator.setTextContent( contributors[i] );
                    md.appendChild( creator );
                }
            }

            // date
            final String date = metadata.getDate();
            if ( date != null )
            {
                final Element de = doc.createElement( METADATA_DATE );
                de.setTextContent( date );
                md.appendChild( de );
            }

            // description
            final String description = metadata.getDescription();
            if ( description != null )
            {
                final Element de = doc.createElement( METADATA_DESCRIPTION );
                de.setTextContent( description );
                md.appendChild( de );
            }

            // language
            final String[] languages = metadata.getLanguage();
            if ( languages != null )
            {
                for ( int i = 0; i < languages.length; i++ )
                {
                    final Element language = doc.createElement( METADATA_LANGUAGE );
                    language.setTextContent( languages[i] );
                    md.appendChild( language );
                }
            }

            // localized-description
            final ILocalizedDescription[] localDescriptions = metadata.getLocalizedDescription();
            if ( localDescriptions != null )
            {
                for ( int i = 0; i < localDescriptions.length; i++ )
                {
                    final Element localDescription = doc.createElement( METADATA_LOCALIZED_DESCRIPTION );
                    final Element lang = doc.createElement( METADATA_LANG );
                    lang.setTextContent( localDescriptions[i].lang() );
                    final Element text = doc.createElement( METADATA_TEXT );
                    text.setTextContent( localDescriptions[i].text() );
                    localDescription.appendChild( lang );
                    localDescription.appendChild( text );

                    md.appendChild( localDescription );
                }
            }

            // localized-title
            final ILocalizedTitle[] localizedTitles = metadata.getLocalizedTitle();
            if ( localizedTitles != null )
            {
                for ( int i = 0; i < localizedTitles.length; i++ )
                {
                    final Element localTitle = doc.createElement( METADATA_LOCALIZED_TITLE );
                    final Element lang = doc.createElement( METADATA_LANG );
                    lang.setTextContent( localizedTitles[i].lang() );
                    final Element text = doc.createElement( METADATA_TITLE );
                    text.setTextContent( localizedTitles[i].title() );
                    localTitle.appendChild( lang );
                    localTitle.appendChild( text );

                    md.appendChild( localTitle );
                }
            }

            // publisher
            final String[] publishers = metadata.getPublisher();
            if ( publishers != null )
            {
                for ( int i = 0; i < publishers.length; i++ )
                {
                    final Element publisher = doc.createElement( METADATA_PUBLISHER );
                    publisher.setTextContent( publishers[i] );
                    md.appendChild( publisher );
                }
            }

            // title
            final String title = metadata.getTitle();
            if ( title != null )
            {
                final Element te = doc.createElement( METADATA_TITLE );
                te.setTextContent( title );
                md.appendChild( te );
            }

            config.appendChild( md );
        }

        // -----------------
        // swf-version
        // -----------------
        final Integer swfVersion = conf.getSwfVersion();
        if ( swfVersion != null )
        {
            final Element version = doc.createElement( SWF_VERSION );
            version.setTextContent( String.valueOf( swfVersion ) );
            config.appendChild( version );
        }
    }

    private void addCompilerConfig( Document doc, Element config )
    {
        final Element compiler = doc.createElement( COMPILER );

        final ICompilerConfiguration conf = (ICompilerConfiguration) adapter;

        /*
         * <fonts> <advancedAntiAliasing>true</advancedAntiAliasing> <flashType>true</flashType> <languages>
         * <englishRange>U+0020-U+007E</englishRange> </languages>
         * <localFontsSnapshot>${baseDir}/src/main/resources/fonts.ser</localFontsSnapshot> <managers>
         * <manager>flash.fonts.BatikFontManager</manager> </managers> <maxCachedFonts>20</maxCachedFonts>
         * <maxGlyphsPerFace>1000</maxGlyphsPerFace> </fonts>
         */
        // --------------------------------
        // compiler.fonts
        // --------------------------------
        final IFontsConfiguration fontsConf = conf.getFontsConfiguration();
        if ( fontsConf != null )
        {
            final Element fonts = doc.createElement( FONTS );
            // advanced-anti-aliasing
            if ( fontsConf.getAdvancedAntiAliasing() != null )
            {
                final Element aaa = doc.createElement( ADVANCED_ANTI_ALIASING );
                aaa.setTextContent( fontsConf.getAdvancedAntiAliasing().toString() );
                fonts.appendChild( aaa );
            }
            // flat-type
            if ( fontsConf.getFlashType() != null )
            {
                final Element ft = doc.createElement( FLASH_TYPE );
                ft.setTextContent( fontsConf.getFlashType().toString() );
                fonts.appendChild( ft );
            }
            // languages
            if ( fontsConf.getLanguagesConfiguration() != null )
            {
                final ILanguages langConf = fontsConf.getLanguagesConfiguration();

                final ILanguageRange[] ranges = langConf.getLanguageRange();
                if ( ranges != null )
                {
                    final Element languages = doc.createElement( LANGUAGES );

                    for ( int i = 0; i < ranges.length; i++ )
                    {
                        final Element langRange = doc.createElement( LANGUAGE_RANGE );

                        final Element lang = doc.createElement( LANG );
                        lang.setTextContent( ranges[i].lang() );
                        langRange.appendChild( lang );

                        final Element range = doc.createElement( RANGE );
                        range.setTextContent( ranges[i].range() );
                        langRange.appendChild( range );

                        languages.appendChild( langRange );
                    }

                    fonts.appendChild( languages );
                }
            }
            // local-font-paths
            if ( fontsConf.getLocalFontPaths() != null && fontsConf.getLocalFontPaths().size() > 0 )
            {
                @SuppressWarnings( "unchecked" )
                final List<String> lfp = fontsConf.getLocalFontPaths();

                final Element paths = doc.createElement( LOCAL_FONT_PATHS );

                final Iterator<String> itr = lfp.iterator();
                while ( itr.hasNext() )
                {
                    final Element path = doc.createElement( PATH_ELEMENT );
                    path.setTextContent( itr.next() );
                    paths.appendChild( path );
                }

                fonts.appendChild( paths );
            }
            // local-fonts-snapshot
            if ( fontsConf.getLocalFontsSnapshot() != null )
            {
                final Element snap = doc.createElement( LOCAL_FONTS_SNAPSHOT );
                snap.setTextContent( fontsConf.getLocalFontsSnapshot() );
                fonts.appendChild( snap );
            }
            // managers
            if ( fontsConf.getManagers() != null && fontsConf.getManagers().size() > 0 )
            {
                final Element managers = doc.createElement( MANAGERS );

                @SuppressWarnings( "unchecked" )
                final Iterator<String> clazz = fontsConf.getManagers().iterator();
                while ( clazz.hasNext() )
                {
                    final Element c = doc.createElement( MANAGER_CLASS );
                    c.setTextContent( clazz.next() );
                    managers.appendChild( c );
                }

                fonts.appendChild( managers );
            }
            // max-cached-fonts
            if ( fontsConf.getMaxCachedFonts() != null )
            {
                final Element mcf = doc.createElement( MAX_CACHED_FONTS );
                mcf.setTextContent( fontsConf.getMaxCachedFonts() );
                fonts.appendChild( mcf );
            }
            // max-glyphs-per-face
            if ( fontsConf.getMaxGlyphsPerFace() != null )
            {
                final Element mgpf = doc.createElement( MAX_GLYPHS_PER_FACE );
                mgpf.setTextContent( fontsConf.getMaxGlyphsPerFace() );
                fonts.appendChild( mgpf );
            }

            compiler.appendChild( fonts );
        }

        // ---------------------------------
        // compiler.define
        // ---------------------------------
        final IDefine[] defines = conf.getDefine();
        if ( defines != null )
        {
            for ( IDefine define : defines )
            {
                final String name = define.name();
                final String value = define.value();
                final Element def = doc.createElement( DEFINE );
                final Element defName = doc.createElement( NAME );
                defName.setTextContent( name );
                final Element defValue = doc.createElement( VALUE );
                defValue.setTextContent( value );
                def.appendChild( defName );
                def.appendChild( defValue );

                compiler.appendChild( def );
            }
        }

        // ---------------------------------
        // compiler.keep-all-type-selectors
        // ---------------------------------
        final Boolean keepAllTypeSelectors = conf.getKeepAllTypeSelectors();
        if ( keepAllTypeSelectors != null )
        {
            final Element kats = doc.createElement( KEEP_ALL_TYPE_SELECTORS );
            kats.setTextContent( String.valueOf( keepAllTypeSelectors ) );
            compiler.appendChild( kats );
        }

        // ---------------------------------
        // compiler.keep-as3-type-metadata
        // ---------------------------------
        final String[] keepAs3Metadata = conf.getKeepAs3Metadata();
        if ( keepAs3Metadata != null )
        {
            final Element keepAs3MetadataElm = doc.createElement( KEEP_AS3_METADATA );
            for ( String name : keepAs3Metadata )
            {
                final Element metadataNameElm = doc.createElement( NAME );
                metadataNameElm.setTextContent( name );
                keepAs3MetadataElm.appendChild( metadataNameElm );
            }
            compiler.appendChild( keepAs3MetadataElm );
        }

        // -----------------
        // compiler.themes
        // -----------------
        @SuppressWarnings( "unchecked" )
        final List<String> themes = conf.getTheme();
        if ( themes != null && themes.size() > 0 )
        {
            final Element themeElm = doc.createElement( THEME );
            int l = themes.size();
            for ( int i = 0; i < l; i++ )
            {
                final String theme = themes.get( i );
                final Element filename = doc.createElement( FILENAME );
                filename.setTextContent( theme );
                themeElm.appendChild( filename );
            }
            compiler.appendChild( themeElm );
        }

        config.appendChild( compiler );
    }

}
