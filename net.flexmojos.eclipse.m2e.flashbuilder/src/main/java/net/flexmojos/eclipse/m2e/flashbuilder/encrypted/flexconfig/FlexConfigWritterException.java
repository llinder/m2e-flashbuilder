package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.flexconfig;

public class FlexConfigWritterException
    extends Exception
{

    private static final long serialVersionUID = 1L;

    public FlexConfigWritterException()
    {
    }

    public FlexConfigWritterException( String message )
    {
        super( message );
    }

    public FlexConfigWritterException( Throwable e )
    {
        super( e );
    }

    public FlexConfigWritterException( String message, Throwable e )
    {
        super( message, e );
    }

}
