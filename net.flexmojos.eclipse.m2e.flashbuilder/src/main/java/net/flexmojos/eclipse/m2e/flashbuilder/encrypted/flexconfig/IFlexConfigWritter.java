package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.flexconfig;

import java.io.OutputStream;

public interface IFlexConfigWritter
{
	public void setMojoAdapter( Object adapter );
	public void writeConfig( OutputStream out ) throws FlexConfigWritterException;
}
