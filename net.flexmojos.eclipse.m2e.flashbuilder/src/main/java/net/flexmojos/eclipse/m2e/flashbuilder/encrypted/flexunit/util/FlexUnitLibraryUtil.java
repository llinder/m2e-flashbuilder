package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.flexunit.util;

import static org.hamcrest.Matchers.allOf;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.osgi.framework.Version;

import com.adobe.flexbuilder.project.FlexProjectManager;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProject;
import com.adobe.flexbuilder.project.actionscript.IMutableActionScriptProjectSettings;
import com.adobe.flexbuilder.project.sdks.IFlexSDK;

public class FlexUnitLibraryUtil
{
	
	
	/**
	 * Updates the Flashbuilder build path with test artifacts from the POM when possible.
	 * If test artifacts don't exists on the POM then they will be referenced from the SDK
	 * the same way Flashbuilder does.
	 * 
	 * If the artifacts already exist then the build path will not be updated.
	 * 
	 * The return value indicates if the build path was updated (true updated and false if not).
	 * 
	 * @param project
	 * @param mavenProject
	 * @return 
	 */
	public static boolean updateBuildPath( IProject project, MavenProject mavenProject )
	{
		boolean exists = false;
		final IActionScriptProject asProject = FlexProjectManager.getActionScriptOrFlexProject( project );
		final IMutableActionScriptProjectSettings settings = asProject.getProjectSettingsClone();
		final IClassPathEntry[] classPath = settings.getLibraryPath();
		final List<IClassPathEntry> pathEntries = new ArrayList<IClassPathEntry>( classPath.length );
		final List<IClassPathEntry> matchedPathEntries = new ArrayList<IClassPathEntry>();
		
		final Set<Artifact> artifacts = mavenProject.getArtifacts();
		
		final FlexUnit flexUnit = new FlexUnit();
		final FlexUnitVersion4 flexUnitVersion4 = new FlexUnitVersion4();
		final FlexUnitVersion0_9 flexUnitVersion0_9 = new FlexUnitVersion0_9();
		
		@SuppressWarnings( "unchecked" )
        final Matcher<Artifact> flexUnitMatcher = allOf( flexUnit, flexUnitVersion4, flexUnitVersion0_9 );
		
		boolean hasFlexUnitArtifact = false;
		
		// get test artifacts (flexunit 0.9, hamcrest 1.0.2, flexunit4-beta2, flexunitextended, FlexUnitTestRunner_rb)
		for( Artifact artifact : artifacts )
		{
			if( flexUnitMatcher.matches( artifact ) );
				hasFlexUnitArtifact = true;
		}
		
		// If Flex Unit 4 or .9 is not on the POM then add it from the local SDK
		if( !hasFlexUnitArtifact )
		{
			try
			{
				final String libPath = getFlexunitSwcPathForTheCurrentSdkVersion( 1, asProject );
				final String libPath_pathString = new Path( libPath ).toString();
				final String baseLibPath = getFlexunitSwcPath();
				final String baseLibPath_pathString = new Path( baseLibPath ).toString();
				final String baseLocalePath = getFlexunitRbSwcPath();
				final String baseLocalePath_pathString = new Path( baseLocalePath ).toString();
				
				// Check if local SDK flex unit paths already exist on the class path
				for( int i=0; i<classPath.length; i++ )
				{
					IClassPathEntry entry = classPath[i];
					
					if( new Path( entry.getValue() ).toString().indexOf( libPath_pathString ) == 0 )
						exists = true;
					
					if( new Path( entry.getValue() ).toString().indexOf( baseLibPath_pathString ) == 0 )
					{
						matchedPathEntries.add( entry );
					}
					else if( new Path( entry.getValue() ).toString().indexOf( baseLocalePath_pathString ) == 0 )
					{
						matchedPathEntries.add( entry );
					}
					
					pathEntries.add( entry );
				}
			}
			catch( CoreException e )
			{
				// TODO log message
			}
		}
		else
		{
			// artifacts exist on POM
			exists = true;
		}
		
		return !exists;
	}
	
	private static String getFlexunitSwcPathForTheCurrentSdkVersion(int type, IActionScriptProject projectIActionScriptProject)
		throws CoreException
	{
		String requiredLocation = null;
		String tpeIndicatorString = null;
		if (type == 1)
		{
			requiredLocation = getFlexunitSwcPath();
			tpeIndicatorString = "libs";
		}
		else if (type == 2)
		{
			requiredLocation = getFlexunitRbSwcPath();
			tpeIndicatorString = "locale";
		}
	
		IFlexSDK sdk = projectIActionScriptProject.getProjectSettings().getFlexSDK();
		Version version = sdk.getVersion();
		int majorVersion = version.getMajor();
		if (majorVersion <= 3)
			majorVersion = 3;
		if (majorVersion >= 4)
			majorVersion = 4;
		requiredLocation = requiredLocation + "/version" + majorVersion + tpeIndicatorString + "/";
	
		return requiredLocation;
	}
	
	private static String getFlexunitSwcPath()
		throws CoreException
	{
		return "${FLEXUNIT_LIB_LOCATION}";
	}
	
	private static String getFlexunitRbSwcPath()
		throws CoreException
	{
		return "${FLEXUNIT_LOCALE_LOCATION}";
	}

	
	private static class FlexUnit extends TypeSafeMatcher<Artifact>
	{
		private static final String FLEXUNIT_GROUP_ID = "flexunit";
		private static final String FLEXUNIT_GROUP_ID2 = "com.adobe.flexunit";
		private static final String FLEXUNIT_ARTIFACT_ID = "flexunit";
			
		@Override
		public boolean matchesSafely(Artifact item)
		{
			if( FLEXUNIT_GROUP_ID.equals( item.getGroupId() ) || FLEXUNIT_GROUP_ID2.equals( item.getGroupId() ) )
				return ( FLEXUNIT_ARTIFACT_ID.equals( FLEXUNIT_ARTIFACT_ID ) );
					
			return false;
		}
		public void describeTo(Description description)
		{
			description.appendText( "not Flex Unit" );
			
		}
	}
	
	private static class FlexUnitVersion0_9 extends TypeSafeMatcher<Artifact>
	{

		@Override
		public boolean matchesSafely(Artifact item)
		{
			// Check flex unit 0.90
			if( "0.90".equals( item.getVersion() ) )
				return true;
			
			return false;
		}

		public void describeTo(Description description)
		{
			description.appendText( "not Flex Unit 0.9" );			
		}
		
	}
	
	private static class FlexUnitVersion4 extends TypeSafeMatcher<Artifact>
	{		
		private static final String VERSION_4_beta_2 = "4.0-beta-2";
		private static final String VERSION_4_rc1 = "4.0-rc1";
		private static final String VERSION_4 = "4.0";
		
		@Override
		public boolean matchesSafely(Artifact item)
		{
			// Check flex unit 4
			if( VERSION_4_beta_2.equals( item.getVersion() ) || VERSION_4_rc1.equals( item.getVersion() ) || VERSION_4.equals( item.getVersion() ) )
				return true;
			
			return false;
		}

		public void describeTo(Description description)
		{
			description.appendText( "not Flex Unit 4" );
		}
	}
}
