package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.markers;

public interface IFlashbuilderMarkers
{
    public static final String MISSING_FLEX_SDK = "net.flexmojos.eclipse.m2e.flashbuilder.markers.missingFlexSDK";
}
