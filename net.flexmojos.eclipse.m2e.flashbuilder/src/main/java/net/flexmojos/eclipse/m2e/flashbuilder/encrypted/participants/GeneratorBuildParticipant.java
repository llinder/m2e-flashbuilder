package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.participants;

import java.io.File;
import java.util.Set;

import net.flexmojos.eclipse.m2e.encrypted.adapters.GeneratorMojoAdapter;

import org.apache.maven.plugin.MojoExecution;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.MojoExecutionBuildParticipant;
import org.sonatype.flexmojos.compiler.extra.IGeneratorConfiguration;

public class GeneratorBuildParticipant
    extends MojoExecutionBuildParticipant
{

    public GeneratorBuildParticipant( MojoExecution execution )
    {
        super( execution, false );
    }

    @Override
    public Set<IProject> build( int kind, IProgressMonitor monitor )
        throws Exception
    {
        final IGeneratorConfiguration config = new GeneratorMojoAdapter( monitor, getSession(), getMojoExecution() );
        
        // Execute generator mojo
        final Set<IProject> result = super.build( kind, monitor );
        
        // Tell m2e builder to refresh generated files.
        final File generated = config.getOutputDirectory();
        if( generated != null )
            getBuildContext().refresh( generated );

        return result;
    }
}
