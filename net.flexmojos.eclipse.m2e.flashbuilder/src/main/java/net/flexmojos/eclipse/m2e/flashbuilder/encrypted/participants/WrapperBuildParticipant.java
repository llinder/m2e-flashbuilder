package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.participants;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import net.flexmojos.eclipse.m2e.encrypted.adapters.WrapperMojoAdapter;

import org.apache.maven.plugin.MojoExecution;
import org.codehaus.plexus.util.FileUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.project.configurator.MojoExecutionBuildParticipant;
import org.sonatype.flexmojos.compiler.extra.IHtmlWrapperConfiguration;

public class WrapperBuildParticipant
    extends MojoExecutionBuildParticipant
{

    public WrapperBuildParticipant( MojoExecution execution )
    {
        super( execution, false );
    }

    @Override
    public Set<IProject> build( int kind, IProgressMonitor monitor )
        throws Exception
    {
        final IHtmlWrapperConfiguration wrapperConfig =
            new WrapperMojoAdapter( monitor, getSession(), getMojoExecution() );

        // Execute wrapper mojo
        final Set<IProject> result = super.build( kind, monitor );

        // tell m2e builder to refresh generated files.
        final File generated = wrapperConfig.getTemplateOutputDirectory();
        if ( generated != null )
        {
            // Copy files from flexmojos wrapper output to Flashbuilder template dir
            copyTemplate( generated );
        }

        return result;
    }

    private File copyTemplate( File generated ) throws IOException
    {
        final File baseDir = getMavenProjectFacade().getMavenProject().getBasedir();

        // Template target directory
        final File outputDir = new File( baseDir, "/html-template" );
        
        // Clear old template
        if ( outputDir.exists() )
            outputDir.delete();
        
        // Build the template target dir
        outputDir.mkdirs();
        
        // Copy files
        FileUtils.copyDirectoryStructure( generated, outputDir );
        
        // If this doesn't happen as desired then another way must be found.
        getBuildContext().refresh( outputDir );
        
        return outputDir;
    }
}
