package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util;

import java.util.Arrays;

public class ArrayUtil
{
	public static <T> T[] concat(T[] first, T[] second)
	{
		if( second == null )
		{
			return first;
		}
		else
		{
			T[] result = Arrays.copyOf(first, first.length + second.length);
			System.arraycopy(second, 0, result, first.length, second.length);
			return result;
		}
	}
}
