package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.sonatype.flexmojos.util.PathUtil;

import com.adobe.flexbuilder.project.ClassPathEntryFactory;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProjectSettings;

public class ClassPathUtils
{
    /**
     * Utility function to prune the problematic libs path.
     * 
     * @param entries
     * @return
     */
    public static IClassPathEntry[] purgeLibsPath( IClassPathEntry[] entries )
    {
        int libIndex = -1;
        for( int i=0; i<entries.length; i++ )
        {
            if( entries[i].getKind() == IClassPathEntry.KIND_PATH )
            {
                libIndex = i;
                break;
            }
        }
        if( libIndex > -1 )
            entries = (IClassPathEntry[])ArrayUtils.remove( entries, libIndex );
        
        return entries;
    }
    
    public static IClassPathEntry[] getSourcePaths( IActionScriptProjectSettings settings, File[] paths )
    {
        final File home = settings.getProjectLocation().toFile();
        final String mainPath = settings.getMainSourceFolder().toFile().toString();
        
        final Set<IClassPathEntry> entries = new LinkedHashSet<IClassPathEntry>();
        
        // Add existing entries
        final IClassPathEntry[] existing = settings.getSourcePath();
        if( existing != null )
        {
            for( int i=0; i<existing.length; i++ )
                entries.add( existing[i] );
        }

        // Add new entries
        for ( int i = 0; i < paths.length; i++ )
        {
            final File f = paths[i];

            // make path relative
            final String path = PathUtil.relativePath( home, f );
            
            // ignore paths that match main source path
            if ( path.equals( mainPath ) )
                continue;

            final IClassPathEntry entry = ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_PATH, path, settings );

            entries.add( entry );
        }

        return entries.toArray( new IClassPathEntry[0] );
    }
}
