package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Version;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import com.adobe.flexbuilder.project.sdks.IFlexSDK;
import com.adobe.flexbuilder.util.FlashPlayerVersion;

public class FlexUtil
{
	private static String FLEX_SDK_DESC = "flex-sdk-description.xml";
	
	public static FlashPlayerVersion getTargetPlayer( final String version )
	{
		FlashPlayerVersion playerVersion = null;
		
        if ( version != null )
        {
            int[] parts = new int[4];
            String[] str = version.split( "\\." );
            for ( int i = 0; i < 4; i++ )
            {
                if ( str.length > i )
                    parts[i] = Integer.parseInt( str[i] );
                else
                    parts[i] = 0;
            }

            playerVersion = new FlashPlayerVersion( parts[0], parts[1], parts[2], parts[3] );
        }

        return playerVersion;
	}
	
	public static IFlexSDK getFlexSdkByVersion( IFlexSDK[] sdks, Version version )
		throws CoreException
	{
		IFlexSDK sdk = null;
		
		// Prepare SAX parser
		final SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating( false );
		SAXParser parser;
		try
		{
			parser = factory.newSAXParser();
			
			for( int i=0; i<sdks.length; i++ )
			{
				IFlexSDK s = sdks[i];
				
				IPath sdkPath = s.getLocation();
				
				// Read sdk description to get version. This is necessary because the IFlexSDK instance
				// doesn't have full version information in it.
				IPath sdkDescription = sdkPath.append( FLEX_SDK_DESC );
				
				InputStream iStream = new FileInputStream( sdkDescription.toFile() );
					
				InputSource input = new InputSource( iStream );
				FlexSdkDesc desc = new FlexSdkDesc();
				parser.parse( input, desc );
					
				if( desc.version.equals( version ) )
				{
					sdk = s;
					break;
				}
			}
		}
		catch ( Exception e)
		{
			throw new CoreException( new Status( Status.ERROR, FlexMojosPlugin.PLUGIN_ID, e.getMessage() ) );
		}
		
		
		
		return sdk;
	}
	
	
	public static class FlexSdkDesc extends DefaultHandler
	{
		private static final String NAME = "name";
		private static final String VERSION = "version";
		private static final String BUILD = "build";
		
		public String name;
		public Integer[] vParts;
		public String build;
		public Version version;
		
		private String lastEntity;
		
		public void startElement( String uri, String localName, String qName, Attributes attributes )
		{
			if( qName != null )
				lastEntity = qName.toLowerCase();
		}
		
		public void characters( char[] ch, int start, int length )
		{
			String value = new String( ch, start, length );
			value = value.trim();
			
			if( !value.equals("") )
			{
			
				if( NAME.equals( lastEntity ) )
				{
					this.name = value;
				}
				else if( VERSION.equals( lastEntity ) )
				{
					String[] parts = value.split("\\.");
					vParts = new Integer[3];
					for( int i=0; i<parts.length; i++ )
					{
						vParts[i] = Integer.parseInt( parts[i] );
					}
					
					if( build != null )
						makeVersion();
				}
				else if( BUILD.equals( lastEntity ) )
				{
					this.build = value;
					
					if( vParts != null )
						makeVersion();
				}
			}
		}
		
		private void makeVersion()
		{
			version = new Version( vParts[0], vParts[1], vParts[2], build );
		}
	}
}
