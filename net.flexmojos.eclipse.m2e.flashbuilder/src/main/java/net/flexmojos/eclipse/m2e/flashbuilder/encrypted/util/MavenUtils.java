package net.flexmojos.eclipse.m2e.flashbuilder.encrypted.util;

import static ch.lambdaj.Lambda.filter;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.allOf;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import net.flexmojos.eclipse.m2e.FlexMojosPlugin;
import net.flexmojos.eclipse.m2e.common.FlexConstants;
import net.flexmojos.eclipse.m2e.encrypted.util.PathUtil;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.ConfigurationContainer;
import org.apache.maven.model.Plugin;
import org.apache.maven.project.MavenProject;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.embedder.IMaven;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.hamcrest.Matcher;
import org.osgi.framework.Version;
import org.sonatype.flexmojos.plugin.common.FlexScopes;

import com.adobe.flexbuilder.project.ClassPathEntryFactory;
import com.adobe.flexbuilder.project.IClassPathEntry;
import com.adobe.flexbuilder.project.IFlexLibraryProject;
import com.adobe.flexbuilder.project.actionscript.IActionScriptProjectSettings;
import com.adobe.flexbuilder.project.common.CrossDomainRslEntry;
import com.adobe.flexbuilder.project.internal.FlexLibraryCore;

public class MavenUtils
{
    /*
	public static Set<Artifact> getDependencies( MavenProject project )
    {
        return Collections.unmodifiableSet( project.getArtifacts() );
    }
    */

    /*
    public static Set<Artifact> getDependencies( MavenProject project, Matcher<? extends Artifact>... matchers )
    {
        Set<Artifact> dependencies = getDependencies( project );

        return new LinkedHashSet<Artifact>( filter( allOf( matchers ), dependencies ) );
    }
    */

    /*
    public static Artifact getDependency( MavenProject project, Matcher<? extends Artifact>... matchers )
    {
        return selectFirst( getDependencies( project ), allOf( matchers ) );
    }
    */
	
    
	/**
	 * Utility function that retrieves typed parameters from a
	 * Maven plugin configuration section on the POM.
	 */
    /*
	public static <MPT> MPT getMojoParameterValue( String parameter, Class<MPT> type, MavenSession session, Plugin plugin, ConfigurationContainer configuration, String goal )
	{
		final IMaven maven = MavenPlugin.getMaven();
		
		try
		{
			return maven.getMojoParameterValue( parameter, type, session, plugin, configuration, goal );
		}
		catch( CoreException e ) {}
		
		return null;
	}
	*/
	
	/*
	public static IClassPathEntry convertToClassPath( String resource, IActionScriptProjectSettings settings, IProject project )
	{
		IPath relativePath = PathUtil.getRelativeProjectPath( project, resource );
		return ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_PATH, relativePath.toString(), settings );
	}
	
	public static IClassPathEntry convertToClassPath( IMavenProjectFacade refProject, Artifact a, IActionScriptProjectSettings settings, String[] rslUrls, String[] policyFileUrls, String contextRoot, String packaging )
		throws CoreException
	{
		IFlexLibraryProject p = FlexLibraryCore.getProject( refProject.getProject() );
		if( p != null )
		{
			IFile swc = p.getOutputDebugSWCFile();
			
			final IPath refPath = refProject.getFullPath();
			String refSourceDir = refProject.getMavenProject().getBuild().getSourceDirectory();
			IPath refSourcePath = PathUtil.getRelativeProjectPath( refProject.getProject(), refSourceDir.toString() );
			refSourceDir = refPath.append( refSourcePath ).toString();
			
			IClassPathEntry entry = ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_LIBRARY_FILE, swc.getFullPath().toString(), settings );
			entry.setSourceValue( refSourceDir );
			
			return updateClassPath( entry, a, settings, rslUrls, policyFileUrls, contextRoot, packaging);
		}
		else
		{
			return convertToClassPath( a, settings, rslUrls, policyFileUrls, contextRoot, packaging );
		}
	}
	
	public static IClassPathEntry convertToClassPath( Artifact a, IActionScriptProjectSettings settings, String[] rslUrls, String[] policyFileUrls, String contextRoot, String packaging )
		throws CoreException
	{
		final File file = a.getFile();
		
		try
		{
			String path = file.getCanonicalPath();
			IClassPathEntry entry = ClassPathEntryFactory.newEntry( IClassPathEntry.KIND_LIBRARY_FILE, path, settings );
			entry.setUseDefaultLinkType( false );
			return updateClassPath( entry, a, settings, rslUrls, policyFileUrls, contextRoot, packaging);
		}
		catch ( IOException e )
		{
			throw new CoreException( new Status( Status.ERROR, FlexMojosPlugin.PLUGIN_ID, String.format( "Unable to resolve Maven artifact %s", a.toString() ) ) );
		}
	}
	
	public static IClassPathEntry updateClassPath( IClassPathEntry entry, Artifact a, IActionScriptProjectSettings settings, String[] rslUrls, String[] policyFileUrls, String contextRoot, String packaging )
		throws CoreException
	{
			int linkType = MavenUtils.getLinkTypeFromScope( a.getArtifactId(), a.getScope(), packaging );
				
			if( linkType == -1 )
			{
				// Link type -1 is INTERNAL which is not supported
				// by Flashbuilder. This dependency must be added to
				// the config XML instead.
				return null;
			}
			else if( FlexConstants.SWF.equals( packaging ) &&
				( IClassPathEntry.LINK_TYPE_RSL == linkType || IClassPathEntry.LINK_TYPE_CROSS_DOMAIN_RSL == linkType) )
			{
				setRslInfo( entry, a, rslUrls, policyFileUrls, contextRoot );	
			}
			else
			{
				entry.setLinkType( linkType );
			}
			return entry;
				
	}
	
	public static void setRslInfo( IClassPathEntry entry, final Artifact artifact, final String[] rslUrls, final String[] policyFileUrls, final String contextRoot )
		throws CoreException
	{
		
		String scope = artifact.getScope();
		final String extension;
		if ( FlexScopes.CACHING.equals( scope ) )
		{
			extension = FlexConstants.SWZ;
		}
		else
		{
			extension = FlexConstants.SWF;
		}
		
		// Cross Domain RSL
		if( policyFileUrls != null && policyFileUrls.length > 0 )
		{
			final CrossDomainRslEntry[] cdrEntries = new CrossDomainRslEntry[policyFileUrls.length];
						
			for( int i=0; i<policyFileUrls.length; i++ )
			{
				final String policyFileUrl =
					MavenUtils.interpolateRslUrl( policyFileUrls[i], artifact, extension, contextRoot );
				
				final String rslUrl = 
					MavenUtils.interpolateRslUrl( policyFileUrls[i], artifact, extension, contextRoot );
				
				cdrEntries[i] = new CrossDomainRslEntry( rslUrl, policyFileUrl, true );
			}
			
			entry.setCrossDomainRsls( cdrEntries );
			entry.setLinkType( IClassPathEntry.LINK_TYPE_CROSS_DOMAIN_RSL );
		}
		// Regular RSL (only one URL is supported)
		else if( rslUrls != null && rslUrls.length > 0 )
		{
			final String rslUrl = rslUrls[0];
			entry.setRslUrl( rslUrl );
			entry.setAutoExtractSwf( true );
			entry.setLinkType( IClassPathEntry.LINK_TYPE_RSL );
		}
		else
		{
			// Unexpected Maven configuration.
			throw new CoreException( new Status( Status.ERROR, FlexMojosPlugin.PLUGIN_ID, "Unexpected RSL/Policy URL configuration." ) );
		}
	}
	*/
	
	/*
	public static String interpolateRslUrl( String baseUrl, Artifact artifact, String extension, String contextRoot )
    {
        if ( baseUrl == null )
        {
            return null;
        }

        if ( contextRoot == null || "".equals( contextRoot ) )
        {
            baseUrl = baseUrl.replace( "{contextRoot}/", "" );
        }
        else
        {
            baseUrl = baseUrl.replace( "{contextRoot}", contextRoot );
        }

        baseUrl = replaceArtifactCoordinatesTokens( baseUrl, artifact );

        if ( extension != null )
        {
            baseUrl = baseUrl.replace( "{extension}", extension );
        }

        return baseUrl;
    }
    */
	
    /*
	public static String replaceArtifactCoordinatesTokens( String sample, Artifact artifact )
    {
        sample = sample.replace( "{groupId}", artifact.getGroupId() );
        sample = sample.replace( "{artifactId}", artifact.getArtifactId() );
        sample = sample.replace( "{version}", artifact.getBaseVersion() );
        if ( artifact.getClassifier() != null )
        {
            sample = sample.replace( "{classifier}", artifact.getClassifier() );
        }
        sample = sample.replace( "{hard-version}", artifact.getVersion() );

        return sample;
    }
    */
	
	public static Version getVersionFromArtifact( Artifact a )
	{
		String[] parts = a.getVersion().split("\\.");
		Integer[] vparts = new Integer[3];
		String qualifier = "";
		for( int i=0; i<4; i++ )
		{
			if( i < parts.length )
			{
				if( i < 3 )
					vparts[i] = Integer.parseInt( parts[i] );
				else
					qualifier = parts[i];
			}
			else
			{
				if( i < 3 )
					vparts[i] = 0;
				else
					qualifier = "";
			}
		}
		
		return new Version( vparts[0], vparts[1], vparts[2], qualifier );
	}

	/*
	public static int getLinkTypeFromScope( String artifactId, String scope, String packaging )
	{
		if( "playerglobal".equals( artifactId ) )
		{
			return IClassPathEntry.LINK_TYPE_EXTERNAL;
		}
		else if( "airglobal".equals( artifactId ) )
		{
			return IClassPathEntry.LINK_TYPE_EXTERNAL;
		}
		else if( FlexConstants.SWC.equals( packaging ) )
		{
			if( FlexScopes.MERGED.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_INTERNAL;
			}
			else if( FlexScopes.EXTERNAL.equals( scope ) ||
					 FlexScopes.CACHING.equals( scope ) ||
					 FlexScopes.RSL.equals( scope ) ||
					 FlexScopes.COMPILE.equals( scope ) ||
					 FlexScopes.TEST.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_EXTERNAL;
			}
			else if( FlexScopes.INTERNAL.equals( scope ) )
			{
				// TODO figure out how to do true "INTERNAL" linkage
				// where everything gets compiled in.
				return -1;
			}
			else
			{
				return IClassPathEntry.LINK_TYPE_INTERNAL;
			}
		}
		else if( FlexConstants.SWF.equals( packaging ) )
		{
			if( FlexScopes.MERGED.equals( scope ) ||
				FlexScopes.COMPILE.equals( scope ) ||
				FlexScopes.TEST.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_INTERNAL;
			}
			else if( FlexScopes.EXTERNAL.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_EXTERNAL;
			}
			else if( FlexScopes.INTERNAL.equals( scope ) )
			{
				// TODO figure out how to do true "INTERNAL" linkage
				// where everything gets compiled in.
				return -1;
			}
			else if( FlexScopes.RSL.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_RSL;
			}
			else if( FlexScopes.CACHING.equals( scope ) )
			{
				return IClassPathEntry.LINK_TYPE_CROSS_DOMAIN_RSL;
			}
			else
			{
				return IClassPathEntry.LINK_TYPE_INTERNAL;
			}
		}
		
		return IClassPathEntry.LINK_TYPE_INTERNAL;
	}
	*/
	
	
}
