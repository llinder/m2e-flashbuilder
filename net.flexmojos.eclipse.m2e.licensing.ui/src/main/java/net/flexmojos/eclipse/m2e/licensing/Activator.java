package net.flexmojos.eclipse.m2e.licensing;
import net.flexmojos.eclipse.m2e.licensing.internal.ManageLicenseDialog;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


public class Activator
    extends Plugin implements IStartup
{

    public void earlyStartup()
    {
        // Validate license and present user with the opportunity to get a license.
        ILicensing licensing = LicensingPlugin.getDefault().getLicensing();
        if ( !licensing.validate() )
        {
            final IWorkbench workbench = PlatformUI.getWorkbench();
            workbench.getDisplay().asyncExec( new Runnable()
            {
                public void run()
                {
                    IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
                    if ( window != null )
                    {
                        if ( MessageDialog.openQuestion( window.getShell(), "Invalid License",
                                                         "A valid license is required to use FlexMojos m2e. Would you like to open the license management window?" ) )
                        {
                            ManageLicenseDialog d = new ManageLicenseDialog( window.getShell() );
                            d.create();
                            d.open();
                        }
                    }
                }
            } );

        }
    }
    
}
