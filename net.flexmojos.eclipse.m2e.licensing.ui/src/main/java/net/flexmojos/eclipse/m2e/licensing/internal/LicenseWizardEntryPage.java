package net.flexmojos.eclipse.m2e.licensing.internal;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class LicenseWizardEntryPage
    extends WizardPage
{
    private Text emailInput;

    private Text passwordInput;

    private Text companyIdInput;

    public LicenseWizardEntryPage()
    {
        super( "EntryPage" );
        setTitle( "FlexMojos M2E Licensing & Registration" );
        setPageComplete( false );
        setDescription( "Please enter your NitroLM account information.\nIf you are not an existing NitroLM user then click the register button to create a new account." );
        // TODO Auto-generated constructor stub
    }

    public void createControl( Composite parent )
    {
        Composite container = new Composite( parent, SWT.NULL );
        setControl( container );
        container.setLayout( new FormLayout() );

        Label emailLabel = new Label( container, SWT.RIGHT );
        FormData fd_emailLabel = new FormData();
        fd_emailLabel.top = new FormAttachment(0, 13);
        fd_emailLabel.left = new FormAttachment( 0, 10 );
        emailLabel.setLayoutData( fd_emailLabel );
        emailLabel.setText( "Email Address:" );

        emailInput = new Text( container, SWT.BORDER );
        FormData fd_emailInput = new FormData();
        fd_emailInput.left = new FormAttachment( 0, 99 );
        fd_emailInput.right = new FormAttachment( 100, -10 );
        fd_emailInput.top = new FormAttachment( 0, 10 );
        emailInput.setLayoutData( fd_emailInput );

        passwordInput = new Text( container, SWT.BORDER | SWT.PASSWORD );
        FormData fd_passwordInput = new FormData();
        fd_passwordInput.top = new FormAttachment(emailInput, 6);
        fd_passwordInput.left = new FormAttachment(0, 99);
        fd_passwordInput.right = new FormAttachment(100, -10);
        passwordInput.setLayoutData( fd_passwordInput );

        companyIdInput = new Text( container, SWT.BORDER );
        FormData fd_companyIdInput = new FormData();
        fd_companyIdInput.left = new FormAttachment(emailInput, 0, SWT.LEFT);
        fd_companyIdInput.right = new FormAttachment(100, -10);
        fd_companyIdInput.top = new FormAttachment(passwordInput, 6);
        companyIdInput.setLayoutData( fd_companyIdInput );

        Label lblPassword = new Label( container, SWT.RIGHT );
        FormData fd_lblPassword = new FormData();
        fd_lblPassword.right = new FormAttachment(emailLabel, 83);
        fd_lblPassword.top = new FormAttachment(emailInput, 9);
        fd_lblPassword.left = new FormAttachment( emailLabel, 0, SWT.LEFT );
        lblPassword.setLayoutData( fd_lblPassword );
        lblPassword.setText( "Password:" );

        Label lblCompanyId = new Label( container, SWT.RIGHT );
        FormData fd_lblCompanyId = new FormData();
        fd_lblCompanyId.right = new FormAttachment(emailLabel, 83);
        fd_lblCompanyId.top = new FormAttachment(passwordInput, 9);
        fd_lblCompanyId.left = new FormAttachment( emailLabel, 0, SWT.LEFT );
        lblCompanyId.setLayoutData( fd_lblCompanyId );
        lblCompanyId.setText( "NitroLM Code:" );

        Button getButton = new Button( container, SWT.NONE );
        getButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
            }
        } );
        FormData fd_getButton = new FormData();
        fd_getButton.top = new FormAttachment(companyIdInput, 21);
        fd_getButton.left = new FormAttachment(emailInput, 0, SWT.LEFT);
        getButton.setLayoutData( fd_getButton );
        getButton.setText( "Get License" );

        Label label_1 = new Label( container, SWT.SEPARATOR | SWT.HORIZONTAL );
        FormData fd_label_1 = new FormData();
        fd_label_1.top = new FormAttachment(getButton, 26);
        fd_label_1.left = new FormAttachment(emailLabel, 0, SWT.LEFT);
        fd_label_1.right = new FormAttachment(100, -10);
        label_1.setLayoutData( fd_label_1 );

        Label lblNewUser = new Label( container, SWT.RIGHT );
        FormData fd_lblNewUser = new FormData();
        fd_lblNewUser.top = new FormAttachment(0, 186);
        fd_lblNewUser.right = new FormAttachment(emailLabel, 0, SWT.RIGHT);
        fd_lblNewUser.left = new FormAttachment(emailLabel, 0, SWT.LEFT);
        lblNewUser.setLayoutData( fd_lblNewUser );
        lblNewUser.setText( "New user?:" );

        Button registerButton = new Button( container, SWT.NONE );
        registerButton.addSelectionListener( new SelectionAdapter()
        {
            public void widgetSelected( SelectionEvent e )
            {
                
            }
        } );
        fd_label_1.bottom = new FormAttachment(100, -144);
        FormData fd_registerButton = new FormData();
        fd_registerButton.top = new FormAttachment(lblNewUser, -7, SWT.TOP);
        fd_registerButton.left = new FormAttachment(emailInput, 0, SWT.LEFT);
        registerButton.setLayoutData( fd_registerButton );
        registerButton.setText( "Register" );

        Button releaseButton = new Button( container, SWT.NONE );
        fd_registerButton.right = new FormAttachment(releaseButton, 0, SWT.RIGHT);
        releaseButton.setEnabled( false );
        releaseButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
            }
        } );
        FormData fd_releaseButton = new FormData();
        fd_releaseButton.top = new FormAttachment(getButton, 0, SWT.TOP);
        fd_releaseButton.left = new FormAttachment(getButton, 8);
        releaseButton.setLayoutData( fd_releaseButton );
        releaseButton.setText( "Release License" );

    }
}
