package net.flexmojos.eclipse.m2e.licensing.internal;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;

public class LicenseWizardRegisterPage
    extends WizardPage
{
    private Text emailInput;
    private Text passwordInput;
    private Text firstNameInput;
    private Text lastNameInput;
    private Text companyInput;
    private Text address1Input;
    private Text address2Input;
    private Text cityInput;
    private Text stateInput;
    private Label lblPassword;
    private Label lblFirstName;
    private Label lblLastName;
    private Label lblCompany;
    private Label lblAddress;
    private Label lblAddress_1;
    private Label lblCity;
    private Label lblStateprovince;
    private Text zipInput;
    private Label lblZipCode;
    private Text phoneInput;
    private Label lblPhone;

    protected LicenseWizardRegisterPage( String pageName )
    {
        super( pageName );
        setDescription("Please complete the new user registration form below.");
        setTitle("FlexMojos User Registration");
        // TODO Auto-generated constructor stub
    }

    public void createControl( Composite parent )
    {
        Composite container = new Composite(parent, SWT.NULL);
        setControl(container);
        
        emailInput = new Text(container, SWT.BORDER);
        emailInput.setBounds(123, 10, 457, 19);
        
        passwordInput = new Text(container, SWT.BORDER | SWT.PASSWORD);
        passwordInput.setBounds(123, 35, 457, 19);
        
        firstNameInput = new Text(container, SWT.BORDER);
        firstNameInput.setBounds(123, 60, 457, 19);
        
        lastNameInput = new Text(container, SWT.BORDER);
        lastNameInput.setBounds(123, 85, 457, 19);
        
        companyInput = new Text(container, SWT.BORDER);
        companyInput.setBounds(123, 110, 457, 19);
        
        address1Input = new Text(container, SWT.BORDER);
        address1Input.setBounds(123, 135, 457, 19);
        
        address2Input = new Text(container, SWT.BORDER);
        address2Input.setBounds(123, 160, 457, 19);
        
        cityInput = new Text(container, SWT.BORDER);
        cityInput.setBounds(123, 185, 457, 19);
        
        stateInput = new Text(container, SWT.BORDER);
        stateInput.setBounds(123, 210, 457, 19);
        
        Label lblEmailAddress = new Label(container, SWT.NONE);
        lblEmailAddress.setAlignment(SWT.RIGHT);
        lblEmailAddress.setBounds(10, 13, 107, 14);
        lblEmailAddress.setText("Email Address:");
        
        lblPassword = new Label(container, SWT.NONE);
        lblPassword.setText("Password:");
        lblPassword.setAlignment(SWT.RIGHT);
        lblPassword.setBounds(10, 38, 107, 14);
        
        lblFirstName = new Label(container, SWT.NONE);
        lblFirstName.setText("First Name:");
        lblFirstName.setAlignment(SWT.RIGHT);
        lblFirstName.setBounds(10, 63, 107, 14);
        
        lblLastName = new Label(container, SWT.NONE);
        lblLastName.setText("Last Name:");
        lblLastName.setAlignment(SWT.RIGHT);
        lblLastName.setBounds(10, 88, 107, 14);
        
        lblCompany = new Label(container, SWT.NONE);
        lblCompany.setText("Company:");
        lblCompany.setAlignment(SWT.RIGHT);
        lblCompany.setBounds(10, 113, 107, 14);
        
        lblAddress = new Label(container, SWT.NONE);
        lblAddress.setText("Address1:");
        lblAddress.setAlignment(SWT.RIGHT);
        lblAddress.setBounds(10, 138, 107, 14);
        
        lblAddress_1 = new Label(container, SWT.NONE);
        lblAddress_1.setText("Address2:");
        lblAddress_1.setAlignment(SWT.RIGHT);
        lblAddress_1.setBounds(10, 163, 107, 14);
        
        lblCity = new Label(container, SWT.NONE);
        lblCity.setText("City:");
        lblCity.setAlignment(SWT.RIGHT);
        lblCity.setBounds(10, 188, 107, 14);
        
        lblStateprovince = new Label(container, SWT.NONE);
        lblStateprovince.setText("State/Province");
        lblStateprovince.setAlignment(SWT.RIGHT);
        lblStateprovince.setBounds(10, 213, 107, 14);
        
        zipInput = new Text(container, SWT.BORDER);
        zipInput.setBounds(123, 235, 457, 19);
        
        lblZipCode = new Label(container, SWT.NONE);
        lblZipCode.setText("Zip Code:");
        lblZipCode.setAlignment(SWT.RIGHT);
        lblZipCode.setBounds(10, 238, 107, 14);
        
        phoneInput = new Text(container, SWT.BORDER);
        phoneInput.setBounds(123, 260, 457, 19);
        
        lblPhone = new Label(container, SWT.NONE);
        lblPhone.setText("Phone:");
        lblPhone.setAlignment(SWT.RIGHT);
        lblPhone.setBounds(10, 263, 107, 14);
    }

}
