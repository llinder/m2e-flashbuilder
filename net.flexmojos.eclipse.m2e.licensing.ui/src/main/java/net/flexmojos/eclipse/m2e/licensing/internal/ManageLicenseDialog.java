package net.flexmojos.eclipse.m2e.licensing.internal;

import java.io.IOException;

import net.flexmojos.eclipse.m2e.licensing.ILicensePreferences;
import net.flexmojos.eclipse.m2e.licensing.ILicensing;
import net.flexmojos.eclipse.m2e.licensing.LicenseException;
import net.flexmojos.eclipse.m2e.licensing.LicensingPlugin;
import net.flexmojos.eclipse.m2e.validators.EmailValidator;
import net.flexmojos.eclipse.m2e.validators.IValidator;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ManageLicenseDialog
    extends TitleAreaDialog
{
    private Text emailInput;

    private Text passwordInput;

    private Text codeInput;

    private Button getLicenseButton;

    private Button releaseLicenseButton;

    private Button registerButton;

    private Button demoCheckBox;

    private final ILicensing licensing;

    private IValidator[] validators;

    /**
     * Create the dialog.
     * 
     * @param parentShell
     */
    public ManageLicenseDialog( Shell parentShell )
    {
        super( parentShell );
        setHelpAvailable( false );

        licensing = LicensingPlugin.getDefault().getLicensing();
    }

    /**
     * Create contents of the dialog.
     * 
     * @param parent
     */
    @Override
    protected Control createDialogArea( Composite parent )
    {
        setMessage( "Please enter your NitroLM account information. If you are not an existing NitroLM user then click the register button to create a new account." );
        setTitle( "FlexMojos Licensing and Regisgtration" );

        Composite area = (Composite) super.createDialogArea( parent );
        Composite container = new Composite( area, SWT.NONE );
        container.setLayout( new FormLayout() );
        container.setLayoutData( new GridData( GridData.FILL_BOTH ) );

        Label emailLabel = new Label( container, SWT.RIGHT );
        FormData fd_emailLabel = new FormData();
        fd_emailLabel.left = new FormAttachment( 0, 10 );
        fd_emailLabel.top = new FormAttachment( 0, 13 );
        emailLabel.setLayoutData( fd_emailLabel );
        emailLabel.setText( "Email Address:" );

        emailInput = new Text( container, SWT.BORDER );
        fd_emailLabel.right = new FormAttachment( emailInput, -6 );
        FormData fd_emailInput = new FormData();
        fd_emailInput.left = new FormAttachment( 0, 123 );
        fd_emailInput.right = new FormAttachment( 100, -10 );
        fd_emailInput.top = new FormAttachment( 0, 10 );
        emailInput.setLayoutData( fd_emailInput );
        IValidator emailValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( EmailValidator.isValid( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "A valid NitroLM email address is required." );
            }

            public IStatus validate()
            {
                return validate( emailInput.getText() );
            }
        };

        Label passwordLabel = new Label( container, SWT.RIGHT );
        passwordLabel.setText( "Password:" );
        FormData fd_passwordLabel = new FormData();
        fd_passwordLabel.right = new FormAttachment( emailLabel, 0, SWT.RIGHT );
        fd_passwordLabel.left = new FormAttachment( 0, 10 );
        fd_passwordLabel.top = new FormAttachment( emailLabel, 11 );
        passwordLabel.setLayoutData( fd_passwordLabel );

        passwordInput = new Text( container, SWT.BORDER | SWT.PASSWORD );
        FormData fd_passwordInput = new FormData();
        fd_passwordInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        fd_passwordInput.top = new FormAttachment( emailInput, 6 );
        fd_passwordInput.right = new FormAttachment( 100, -10 );
        passwordInput.setLayoutData( fd_passwordInput );
        IValidator passwordValidator = new IValidator()
        {

            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your NitroLM password is required." );

            }

            public IStatus validate()
            {
                return validate( passwordInput.getText() );
            }
        };

        Label lblCode = new Label( container, SWT.RIGHT );
        lblCode.setText( "Company Code:" );
        FormData fd_lblCode = new FormData();
        fd_lblCode.right = new FormAttachment( emailLabel, 0, SWT.RIGHT );
        fd_lblCode.left = new FormAttachment( 0, 10 );
        lblCode.setLayoutData( fd_lblCode );

        codeInput = new Text( container, SWT.BORDER );
        fd_lblCode.top = new FormAttachment( codeInput, 3, SWT.TOP );
        FormData fd_codeInput = new FormData();
        fd_codeInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_codeInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        codeInput.setLayoutData( fd_codeInput );
        IValidator companyValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                if( !demoCheckBox.getSelection() )
                {
                    return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                    : ValidationStatus.error( "Your NitroLM company code is required." );
                }
                else
                {
                    return Status.OK_STATUS;
                }
            }

            public IStatus validate()
            {
                return validate( codeInput.getText() );
            }
        };

        validators = new IValidator[] { emailValidator, passwordValidator, companyValidator };

        Composite composite = new Composite( container, SWT.NONE );
        FormData fd_composite = new FormData();
        fd_composite.top = new FormAttachment( codeInput, 6 );
        fd_composite.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_composite.left = new FormAttachment( 100, -316 );
        composite.setLayoutData( fd_composite );

        releaseLicenseButton = new Button( composite, SWT.NONE );
        releaseLicenseButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
                Runnable runnable = new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            if ( licensing.releaseLicense() )
                            {
                                ILicensePreferences prefs = LicensingPlugin.getDefault().getLicensePreferences();
                                prefs.setEmail( "" );
                                prefs.setPassword( "" );
                                prefs.setCompany( "" );
                                prefs.save();

                                updateState();
                                
                                setReturnCode( OK );
                                close();
                            }
                        }
                        catch ( LicenseException e )
                        {
                            MessageDialog.openWarning( getShell(), "Release Error", e.getMessage() );
                        }
                        catch ( IOException e )
                        {
                            MessageDialog.openWarning( getShell(), "Save Error", e.getMessage() );
                        }
                    }
                };
                BusyIndicator.showWhile( getShell().getDisplay(), runnable );
            }
        } );
        releaseLicenseButton.setEnabled( licensing.hasLicense() );
        releaseLicenseButton.setBounds( 155, 21, 141, 28 );
        releaseLicenseButton.setText( "Release License" );

        getLicenseButton = new Button( composite, SWT.NONE );
        getLicenseButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
                final String email = emailInput.getText();
                final String password = passwordInput.getText();
                final String company = codeInput.getText();

                ManageLicenseDialog.this.setErrorMessage( null );

                // Validate inputs
                for ( int i = 0; i < validators.length; i++ )
                {
                    final IStatus validationStatus = validators[i].validate();
                    if ( !Status.OK_STATUS.equals( validationStatus ) )
                    {
                        ManageLicenseDialog.this.setErrorMessage( validationStatus.getMessage() );
                        return;
                    }
                }

                // Form validation checked OK... get license.
                Runnable runnable = new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            licensing.getLicense( email, password, company );

                            // Save user preferences.
                            ILicensePreferences prefs = LicensingPlugin.getDefault().getLicensePreferences();
                            prefs.setEmail( email );
                            prefs.setPassword( password );
                            prefs.setCompany( company );
                            prefs.save();

                            MessageDialog.openInformation( getShell(), "Complete",
                                                           "FlexMojos M2E license verification complete. Thanks you!" );

                            // Set return code and close!
                            setReturnCode( OK );
                            close();
                        }
                        catch ( LicenseException e )
                        {
                            MessageDialog.openWarning( getShell(), "License Error", e.getMessage() );
                        }
                        catch ( IOException e )
                        {
                            MessageDialog.openWarning( getShell(), "Save Error", e.getMessage() );
                        }
                    }
                };
                BusyIndicator.showWhile( getShell().getDisplay(), runnable );
            }
        } );
        getLicenseButton.setBounds( 22, 21, 127, 28 );
        getLicenseButton.setText( "Get License" );
        getLicenseButton.setEnabled( !licensing.hasLicense() );

        Label lblNewUser = new Label( container, SWT.RIGHT );
        lblNewUser.setText( "New User?:" );
        FormData fd_lblNewUser = new FormData();
        fd_lblNewUser.right = new FormAttachment( emailLabel, 0, SWT.RIGHT );
        fd_lblNewUser.bottom = new FormAttachment( 100, -35 );
        fd_lblNewUser.left = new FormAttachment( emailLabel, 0, SWT.LEFT );
        lblNewUser.setLayoutData( fd_lblNewUser );

        registerButton = new Button( container, SWT.NONE );
        fd_composite.bottom = new FormAttachment( registerButton, -39 );
        registerButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
                RegisterDialog register = new RegisterDialog( getShell() );
                register.setBlockOnOpen( true );
                register.create();
                if ( register.open() == Window.OK ) 
                {
                    // Set return code and close!
                    setReturnCode( OK );
                    close();
                }
            }
        } );
        FormData fd_registerButton = new FormData();
        fd_registerButton.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        fd_registerButton.bottom = new FormAttachment( 100, -28 );
        fd_registerButton.right = new FormAttachment( 100, -289 );
        registerButton.setLayoutData( fd_registerButton );
        registerButton.setText( "Register" );

        demoCheckBox = new Button( container, SWT.CHECK );
        demoCheckBox.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
                codeInput.setEnabled( !demoCheckBox.getSelection() );
            }
        } );
        fd_codeInput.top = new FormAttachment( demoCheckBox, 6 );
        FormData fd_btnDemoLicense = new FormData();
        fd_btnDemoLicense.top = new FormAttachment( passwordInput, 6 );
        fd_btnDemoLicense.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        demoCheckBox.setLayoutData( fd_btnDemoLicense );
        demoCheckBox.setText( "Demo License?" );

        updateState();

        return area;
    }

    /**
     * Create contents of the button bar.
     * 
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar( Composite parent )
    {
        final Button closeButton = createButton( parent, IDialogConstants.OK_ID, "Close", true );
        closeButton.addSelectionListener( new SelectionAdapter()
        {
            public void widgetSelected( SelectionEvent e )
            {
                // Set return code and close!
                setReturnCode( CANCEL );
                close();
            }
        } );
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override
    protected Point getInitialSize()
    {
        return new Point( 575, 415 );
    }

    private void updateState()
    {
        boolean hasLicense = licensing.hasLicense();
        boolean hasDemoLicense = licensing.hasDemoLicense();

        if ( hasLicense )
        {
            ILicensePreferences prefs = LicensingPlugin.getDefault().getLicensePreferences();

            emailInput.setText( prefs.getEmail() );
            passwordInput.setText( prefs.getPassword() );
            codeInput.setText( prefs.getCompany() );
        }
        else
        {
            emailInput.setText( "" );
            passwordInput.setText( "" );
            codeInput.setText( "" );
        }
        
        demoCheckBox.setSelection( hasDemoLicense );
        demoCheckBox.setEnabled( !hasLicense || hasDemoLicense );
        getLicenseButton.setEnabled( !hasLicense || hasDemoLicense );
        // registerButton.setEnabled( !hasLicense ); always allow user to register.
        releaseLicenseButton.setEnabled( hasLicense && !hasDemoLicense );

        emailInput.setEnabled( !hasLicense || hasDemoLicense );
        passwordInput.setEnabled( !hasLicense || hasDemoLicense );
        codeInput.setEnabled( !hasLicense || !hasDemoLicense );
    }
}
