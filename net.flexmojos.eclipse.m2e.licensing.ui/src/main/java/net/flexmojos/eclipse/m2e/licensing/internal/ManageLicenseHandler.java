package net.flexmojos.eclipse.m2e.licensing.internal;



import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;

public class ManageLicenseHandler
    extends AbstractHandler
{
    public Object execute( ExecutionEvent event )
        throws ExecutionException
    {
        
        ManageLicenseDialog dialog = new ManageLicenseDialog( HandlerUtil.getActiveShell( event ) );
        dialog.setBlockOnOpen( true );
        dialog.create();
        
        return dialog.open();
    }

}
