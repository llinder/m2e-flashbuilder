package net.flexmojos.eclipse.m2e.licensing.internal;

import java.io.IOException;

import net.flexmojos.eclipse.m2e.licensing.ILicensePreferences;
import net.flexmojos.eclipse.m2e.licensing.ILicensing;
import net.flexmojos.eclipse.m2e.licensing.LicenseException;
import net.flexmojos.eclipse.m2e.licensing.LicensingPlugin;
import net.flexmojos.eclipse.m2e.validators.EmailValidator;
import net.flexmojos.eclipse.m2e.validators.IValidator;
import net.flexmojos.eclipse.m2e.validators.PhoneValidator;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Text;

public class RegisterDialog
    extends TitleAreaDialog
{

    private Text emailInput;

    private Text passwordInput;

    private Text confirmPasswordInput;

    private Text firstNameInput;

    private Text lastNameInput;

    private Text companyInput;

    private Text phoneInput;

    private Text address1Input;

    private Text address2Input;

    private Text cityInput;

    private Text stateInput;

    private Text zipInput;

    private Label lblFirstName;

    private Label lblLastName;

    private Label lblCompany;

    private Label lblPhoneNumber;

    private Label lblAddress;

    private Label label;

    private Label lblCity;

    private Label lblStateprovince;

    private Label lblZipCode;

    private Text countryInput;

    private Label lblCountry;

    private Button demoButton;

    private Label companyCodeMessage;

    private Text companyCodeInput;

    private Label lblCompanyCode;

    private final ILicensing licensing;

    private IValidator[] validators;

    /**
     * Create the dialog.
     * 
     * @param parentShell
     */
    public RegisterDialog( Shell parentShell )
    {
        super( parentShell );
        setHelpAvailable( false );

        licensing = LicensingPlugin.getDefault().getLicensing();
    }

    /**
     * Create contents of the dialog.
     * 
     * @param parent
     */
    @Override
    protected Control createDialogArea( Composite parent )
    {
        setMessage( "Please complete the Nitro-LM registration form. All fields are manditory." );
        setTitle( "Nitro-LM Registration" );
        Composite area = (Composite) super.createDialogArea( parent );
        Composite container = new Composite( area, SWT.NONE );
        container.setLayout( new FormLayout() );
        container.setLayoutData( new GridData( GridData.FILL_BOTH ) );

        // Form inputs
        emailInput = new Text( container, SWT.BORDER );
        FormData fd_emailInput = new FormData();
        fd_emailInput.left = new FormAttachment( 100, -316 );
        fd_emailInput.top = new FormAttachment( 0, 10 );
        fd_emailInput.right = new FormAttachment( 100, -10 );
        emailInput.setLayoutData( fd_emailInput );
        IValidator emailValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( EmailValidator.isValid( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "A valid email address is required." );
            }

            public IStatus validate()
            {
                return validate( emailInput.getText() );
            }
        };

        passwordInput = new Text( container, SWT.BORDER | SWT.PASSWORD );
        FormData fd_passwordInput = new FormData();
        fd_passwordInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_passwordInput.top = new FormAttachment( emailInput, 6 );
        fd_passwordInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        passwordInput.setLayoutData( fd_passwordInput );
        IValidator passwordValidator = new IValidator()
        {

            public IStatus validate( Object value )
            {
                final String s = (String) value;
                if ( StringUtils.isNotBlank( s ) )
                    if ( s.length() < 8 )
                        return ValidationStatus.error( "Password must be at least 8 chacters." );
                    else
                        return Status.OK_STATUS;
                else
                    return ValidationStatus.error( "A password is required." );
            }

            public IStatus validate()
            {
                return validate( passwordInput.getText() );
            }
        };

        confirmPasswordInput = new Text( container, SWT.BORDER | SWT.PASSWORD );
        FormData fd_confirmPasswordInput = new FormData();
        fd_confirmPasswordInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_confirmPasswordInput.top = new FormAttachment( passwordInput, 6 );
        fd_confirmPasswordInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        confirmPasswordInput.setLayoutData( fd_confirmPasswordInput );
        IValidator confirmPasswordValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                final String s = (String) value;
                final String s2 = passwordInput.getText();
                if ( StringUtils.isNotBlank( s ) )
                    if ( !s.equals( s2 ) )
                        return ValidationStatus.error( "Passwords don't match. Please re-enter your password." );
                    else
                        return Status.OK_STATUS;
                else
                    return ValidationStatus.error( "Password confirmation is required." );
            }

            public IStatus validate()
            {
                return validate( confirmPasswordInput.getText() );
            }
        };

        firstNameInput = new Text( container, SWT.BORDER );
        FormData fd_firstNameInput = new FormData();
        fd_firstNameInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_firstNameInput.top = new FormAttachment( confirmPasswordInput, 6 );
        fd_firstNameInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        firstNameInput.setLayoutData( fd_firstNameInput );
        IValidator firstNameValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your first name is required." );
            }

            public IStatus validate()
            {
                return validate( firstNameInput.getText() );
            }
        };

        lastNameInput = new Text( container, SWT.BORDER );
        FormData fd_lastNameInput = new FormData();
        fd_lastNameInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_lastNameInput.top = new FormAttachment( firstNameInput, 6 );
        fd_lastNameInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        lastNameInput.setLayoutData( fd_lastNameInput );
        IValidator lastNameValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your last name is required." );
            }

            public IStatus validate()
            {
                return validate( lastNameInput.getText() );
            }
        };

        companyInput = new Text( container, SWT.BORDER );
        FormData fd_companyInput = new FormData();
        fd_companyInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_companyInput.top = new FormAttachment( lastNameInput, 6 );
        fd_companyInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        companyInput.setLayoutData( fd_companyInput );
        IValidator companyValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your company name is required." );
            }

            public IStatus validate()
            {
                return validate( companyInput.getText() );
            }
        };

        phoneInput = new Text( container, SWT.BORDER );
        FormData fd_phoneInput = new FormData();
        fd_phoneInput.top = new FormAttachment( companyInput, 6 );
        fd_phoneInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_phoneInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        phoneInput.setLayoutData( fd_phoneInput );
        IValidator phoneValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( PhoneValidator.isValid( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "A valid phone number is required." );
            }

            public IStatus validate()
            {
                return validate( phoneInput.getText() );
            }
        };

        address1Input = new Text( container, SWT.BORDER );
        FormData fd_address1Input = new FormData();
        fd_address1Input.top = new FormAttachment( phoneInput, 5 );
        fd_address1Input.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_address1Input.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        address1Input.setLayoutData( fd_address1Input );
        IValidator addressValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your address is required." );
            }

            public IStatus validate()
            {
                return validate( address1Input.getText() );
            }
        };

        address2Input = new Text( container, SWT.BORDER );
        FormData fd_address2Input = new FormData();
        fd_address2Input.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_address2Input.top = new FormAttachment( address1Input, 6 );
        fd_address2Input.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        address2Input.setLayoutData( fd_address2Input );

        cityInput = new Text( container, SWT.BORDER );
        FormData fd_cityInput = new FormData();
        fd_cityInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_cityInput.top = new FormAttachment( address2Input, 6 );
        fd_cityInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        cityInput.setLayoutData( fd_cityInput );
        IValidator cityValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your city is required." );
            }

            public IStatus validate()
            {
                return validate( cityInput.getText() );
            }
        };

        stateInput = new Text( container, SWT.BORDER );
        FormData fd_stateInput = new FormData();
        fd_stateInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        fd_stateInput.top = new FormAttachment( cityInput, 6 );
        fd_stateInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        stateInput.setLayoutData( fd_stateInput );
        IValidator stateValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your state or province is required." );
            }

            public IStatus validate()
            {
                return validate( stateInput.getText() );
            }
        };

        zipInput = new Text( container, SWT.BORDER );
        FormData fd_zipInput = new FormData();
        fd_zipInput.top = new FormAttachment( stateInput, 6 );
        fd_zipInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_zipInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        zipInput.setLayoutData( fd_zipInput );
        IValidator zipValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your zip code is required." );
            }

            public IStatus validate()
            {
                return validate( zipInput.getText() );
            }
        };

        countryInput = new Text( container, SWT.BORDER );
        FormData fd_countryInput = new FormData();
        fd_countryInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_countryInput.top = new FormAttachment( zipInput, 6 );
        fd_countryInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        countryInput.setLayoutData( fd_countryInput );
        IValidator countryValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                return ( StringUtils.isNotBlank( (String) value ) ) ? Status.OK_STATUS
                                : ValidationStatus.error( "Your country is required." );
            }

            public IStatus validate()
            {
                return validate( countryInput.getText() );
            }
        };

        // Form labels
        Label lblNewLabel = new Label( container, SWT.NONE );
        lblNewLabel.setAlignment( SWT.RIGHT );
        FormData fd_lblNewLabel = new FormData();
        fd_lblNewLabel.left = new FormAttachment( 0, 10 );
        fd_lblNewLabel.right = new FormAttachment( emailInput, -6 );
        fd_lblNewLabel.top = new FormAttachment( emailInput, 3, SWT.TOP );
        lblNewLabel.setLayoutData( fd_lblNewLabel );
        lblNewLabel.setText( "Email:" );

        Label lblPassword = new Label( container, SWT.NONE );
        lblPassword.setAlignment( SWT.RIGHT );
        lblPassword.setText( "Password:" );
        FormData fd_lblPassword = new FormData();
        fd_lblPassword.left = new FormAttachment( 0, 10 );
        fd_lblPassword.top = new FormAttachment( passwordInput, 3, SWT.TOP );
        fd_lblPassword.right = new FormAttachment( passwordInput, -6 );
        lblPassword.setLayoutData( fd_lblPassword );

        Label lblConfirmPassword = new Label( container, SWT.NONE );
        lblConfirmPassword.setAlignment( SWT.RIGHT );
        lblConfirmPassword.setText( "Confirm Password:" );
        FormData fd_lblConfirmPassword = new FormData();
        fd_lblConfirmPassword.left = new FormAttachment( 0, 10 );
        fd_lblConfirmPassword.top = new FormAttachment( confirmPasswordInput, 3, SWT.TOP );
        fd_lblConfirmPassword.right = new FormAttachment( confirmPasswordInput, -6 );
        lblConfirmPassword.setLayoutData( fd_lblConfirmPassword );

        lblFirstName = new Label( container, SWT.NONE );
        lblFirstName.setText( "First Name:" );
        lblFirstName.setAlignment( SWT.RIGHT );
        FormData fd_lblFirstName = new FormData();
        fd_lblFirstName.right = new FormAttachment( firstNameInput, -6 );
        fd_lblFirstName.left = new FormAttachment( 0, 10 );
        fd_lblFirstName.top = new FormAttachment( firstNameInput, 3, SWT.TOP );
        lblFirstName.setLayoutData( fd_lblFirstName );

        lblLastName = new Label( container, SWT.NONE );
        lblLastName.setText( "Last Name:" );
        lblLastName.setAlignment( SWT.RIGHT );
        FormData fd_lblLastName = new FormData();
        fd_lblLastName.right = new FormAttachment( lastNameInput, -6 );
        fd_lblLastName.left = new FormAttachment( 0, 10 );
        fd_lblLastName.top = new FormAttachment( lastNameInput, 3, SWT.TOP );
        lblLastName.setLayoutData( fd_lblLastName );

        lblCompany = new Label( container, SWT.NONE );
        lblCompany.setText( "Company:" );
        lblCompany.setAlignment( SWT.RIGHT );
        FormData fd_lblCompany = new FormData();
        fd_lblCompany.right = new FormAttachment( companyInput, -6 );
        fd_lblCompany.left = new FormAttachment( 0, 10 );
        fd_lblCompany.top = new FormAttachment( companyInput, 3, SWT.TOP );
        lblCompany.setLayoutData( fd_lblCompany );

        lblPhoneNumber = new Label( container, SWT.NONE );
        lblPhoneNumber.setText( "Phone Number:" );
        lblPhoneNumber.setAlignment( SWT.RIGHT );
        FormData fd_lblPhoneNumber = new FormData();
        fd_lblPhoneNumber.right = new FormAttachment( phoneInput, -6 );
        fd_lblPhoneNumber.left = new FormAttachment( 0, 10 );
        fd_lblPhoneNumber.top = new FormAttachment( phoneInput, 3, SWT.TOP );
        lblPhoneNumber.setLayoutData( fd_lblPhoneNumber );

        lblAddress = new Label( container, SWT.NONE );
        lblAddress.setText( "Address:" );
        lblAddress.setAlignment( SWT.RIGHT );
        FormData fd_lblAddress = new FormData();
        fd_lblAddress.right = new FormAttachment( address1Input, -6 );
        fd_lblAddress.left = new FormAttachment( 0, 10 );
        fd_lblAddress.top = new FormAttachment( address1Input, 3, SWT.TOP );
        lblAddress.setLayoutData( fd_lblAddress );

        label = new Label( container, SWT.NONE );
        label.setText( "Address:" );
        label.setAlignment( SWT.RIGHT );
        FormData fd_label = new FormData();
        fd_label.right = new FormAttachment( address2Input, -6 );
        fd_label.left = new FormAttachment( 0, 10 );
        fd_label.top = new FormAttachment( address2Input, 3, SWT.TOP );
        label.setLayoutData( fd_label );

        lblCity = new Label( container, SWT.NONE );
        lblCity.setText( "City:" );
        lblCity.setAlignment( SWT.RIGHT );
        FormData fd_lblCity = new FormData();
        fd_lblCity.right = new FormAttachment( cityInput, -6 );
        fd_lblCity.left = new FormAttachment( 0, 10 );
        fd_lblCity.top = new FormAttachment( cityInput, 3, SWT.TOP );
        lblCity.setLayoutData( fd_lblCity );

        lblStateprovince = new Label( container, SWT.NONE );
        lblStateprovince.setText( "State/Province:" );
        lblStateprovince.setAlignment( SWT.RIGHT );
        FormData fd_lblStateprovince = new FormData();
        fd_lblStateprovince.right = new FormAttachment( stateInput, -6 );
        fd_lblStateprovince.top = new FormAttachment( lblCity, 11 );
        fd_lblStateprovince.left = new FormAttachment( 0, 10 );
        lblStateprovince.setLayoutData( fd_lblStateprovince );

        lblZipCode = new Label( container, SWT.NONE );
        lblZipCode.setText( "Zip Code:" );
        lblZipCode.setAlignment( SWT.RIGHT );
        FormData fd_lblZipCode = new FormData();
        fd_lblZipCode.right = new FormAttachment( zipInput, -6 );
        fd_lblZipCode.left = new FormAttachment( 0, 10 );
        fd_lblZipCode.top = new FormAttachment( zipInput, 3, SWT.TOP );
        lblZipCode.setLayoutData( fd_lblZipCode );

        lblCountry = new Label( container, SWT.NONE );
        lblCountry.setText( "Country:" );
        lblCountry.setAlignment( SWT.RIGHT );
        FormData fd_lblCountry = new FormData();
        fd_lblCountry.right = new FormAttachment( countryInput, -6 );
        fd_lblCountry.left = new FormAttachment( 0, 10 );
        fd_lblCountry.top = new FormAttachment( countryInput, 3, SWT.TOP );
        lblCountry.setLayoutData( fd_lblCountry );

        companyCodeMessage = new Label( container, SWT.WRAP | SWT.CENTER );
        FormData fd_companyCodeMessage = new FormData();
        fd_companyCodeMessage.left = new FormAttachment( lblNewLabel, 0, SWT.LEFT );
        fd_companyCodeMessage.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_companyCodeMessage.top = new FormAttachment( countryInput, 6 );
        companyCodeMessage.setLayoutData( fd_companyCodeMessage );
        companyCodeMessage.setText( "If you have a Nitro-LM company code enter it here.\nOtherwise select the \"Request Demo License\" for a free 30 day trail." );

        demoButton = new Button( container, SWT.CHECK );
        demoButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected( SelectionEvent e )
            {
                companyCodeInput.setEnabled( !demoButton.getSelection() );
                if( demoButton.getSelection() )
                    companyCodeInput.setText( "" );
            }
        } );
        demoButton.setText( "Request Demo License" );
        FormData fd_demoButton = new FormData();
        fd_demoButton.top = new FormAttachment( companyCodeMessage, 6 );
        fd_demoButton.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_demoButton.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        demoButton.setLayoutData( fd_demoButton );

        companyCodeInput = new Text( container, SWT.BORDER );
        FormData fd_companyCodeInput = new FormData();
        fd_companyCodeInput.right = new FormAttachment( emailInput, 0, SWT.RIGHT );
        fd_companyCodeInput.top = new FormAttachment( demoButton, 6 );
        fd_companyCodeInput.left = new FormAttachment( emailInput, 0, SWT.LEFT );
        companyCodeInput.setLayoutData( fd_companyCodeInput );
        IValidator companyCodeValidator = new IValidator()
        {
            public IStatus validate( Object value )
            {
                if( !demoButton.getSelection() )
                {
                    String s = (String)value;
                    return ( StringUtils.isBlank( s ) ) ? ValidationStatus.error( "Your company code is required or select \"Request Demo License\" for a trial." ) :
                        Status.OK_STATUS;
                }
                else
                {
                    return Status.OK_STATUS;
                }
            }
            
            public IStatus validate()
            {
                return validate( companyCodeInput.getText() );
            }
        };

        lblCompanyCode = new Label( container, SWT.NONE );
        lblCompanyCode.setText( "Company Code:" );
        lblCompanyCode.setAlignment( SWT.RIGHT );
        FormData fd_lblCompanyCode = new FormData();
        fd_lblCompanyCode.right = new FormAttachment( companyCodeInput, -6 );
        fd_lblCompanyCode.top = new FormAttachment( companyCodeInput, 3, SWT.TOP );
        fd_lblCompanyCode.left = new FormAttachment( 0, 10 );
        lblCompanyCode.setLayoutData( fd_lblCompanyCode );
        
        validators =
                        new IValidator[] { emailValidator, passwordValidator, confirmPasswordValidator, firstNameValidator,
                            lastNameValidator, companyValidator, phoneValidator, addressValidator, cityValidator, stateValidator,
                            zipValidator, countryValidator, companyCodeValidator };

        return area;
    }

    @Override
    protected void okPressed()
    {
        // Validate inputs
        for ( int i = 0; i < validators.length; i++ )
        {
            final IStatus validationStatus = validators[i].validate();
            if ( !Status.OK_STATUS.equals( validationStatus ) )
            {
                RegisterDialog.this.setErrorMessage( validationStatus.getMessage() );
                return;
            }
        }
        final String email = emailInput.getText();
        final String password = passwordInput.getText();
        final String firstName = firstNameInput.getText();
        final String lastName = lastNameInput.getText();
        final String company = companyInput.getText();
        final String phone = phoneInput.getText();
        final String address1 = address1Input.getText();
        final String address2 = address2Input.getText();
        final String city = cityInput.getText();
        final String state = stateInput.getText();
        final String zip = zipInput.getText();
        final String country = countryInput.getText();
        // Set company code to null for demo license.
        final String companyCode = !demoButton.getSelection() ? companyCodeInput.getText() : null;

        // Form validation checked OK... perform registration.
        Runnable runnable = new Runnable()
        {
            public void run()
            {
                try
                {
                    licensing.register( email, password, firstName, lastName, company, phone, address1, address2, city,
                                        state, zip, country, companyCode );

                    // Save user preferences.
                    ILicensePreferences prefs = LicensingPlugin.getDefault().getLicensePreferences();
                    prefs.setEmail( email );
                    prefs.setPassword( password );
                    prefs.setCompany( companyCode == null ? "" : companyCode );
                    prefs.save();

                    final String message =
                        ( demoButton.getSelection() ) ? "Thank you for registering!\n\nTo complete the process please check your email and click the confirmation link to enable your account. After confirming your email address you will be granted a 30 day trial and can start using FlexMojos M2E immediatly."
                                        : "Thank you for registering!\n\nTo complete the process please check your email and click the confirmation link to enable your account. After confirming your email address you can begin using FlexMojos M2E immediatly.";

                    MessageDialog.openInformation( getShell(), "Success", message );

                    // Set return code and close!
                    setReturnCode( OK );
                    close();
                }
                catch ( LicenseException e )
                {
                    MessageDialog.openWarning( getShell(), "Registration Error", e.getMessage() );
                }
                catch ( IOException e )
                {
                    MessageDialog.openWarning( getShell(), "Save Error", e.getMessage() );
                }
            }
        };
        BusyIndicator.showWhile( getShell().getDisplay(), runnable );
    }

    /**
     * Create contents of the button bar.
     * 
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar( Composite parent )
    {
        createButton( parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true );
        createButton( parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false );
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override
    protected Point getInitialSize()
    {
        return new Point( 511, 621 );
    }
}
