package net.flexmojos.eclipse.m2e.licensing.internal.markers;

import java.awt.Window;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.flexmojos.eclipse.m2e.licensing.LicensingPlugin;
import net.flexmojos.eclipse.m2e.licensing.internal.ManageLicenseDialog;
import net.flexmojos.eclipse.m2e.licensing.markers.ILicenseMarkers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.m2e.core.ui.internal.Messages;
import org.eclipse.m2e.core.ui.internal.UpdateConfigurationJob;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator2;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.markers.WorkbenchMarkerResolution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings( "restriction" )
public class LicenseRequiredMarkerResolutionGenerator
    implements IMarkerResolutionGenerator2
{

    private static final Logger log = LoggerFactory.getLogger( LicenseRequiredMarkerResolutionGenerator.class );

    public IMarkerResolution[] getResolutions( IMarker marker )
    {
        final boolean userHasLicense = LicensingPlugin.getDefault().getLicensing().hasLicense();
        
        if( !userHasLicense )
        {
            return new IMarkerResolution[] { new LaunchManagementWindow( marker ) };
        }
        else
        {
            return new IMarkerResolution[] { new RefreshResolution( marker ) };
        }
    }

    public boolean hasResolutions( IMarker marker )
    {
        try
        {
            return ( ILicenseMarkers.REQUIRED_ID.equals( marker.getType() ) );
        }
        catch ( CoreException e )
        {
            return false;
        }
    }
    
    private abstract class LicenseResolution extends WorkbenchMarkerResolution
    {
        protected final IMarker marker;
        
        public LicenseResolution( IMarker marker )
        {
            this.marker = marker;
        }
        
        /**
         * @param markers
         * @return
         */
        protected Set<IProject> getProjects( IMarker... markers )
        {
            Set<IProject> toRet = new HashSet<IProject>();
            for ( IMarker mark : markers )
            {
                IResource res = mark.getResource();
                IProject prj = res.getProject();
                if ( prj != null )
                {
                    toRet.add( prj );
                }
            }
            return toRet;
        }

        /*
         * (non-Javadoc)
         * @see
         * org.eclipse.ui.views.markers.WorkbenchMarkerResolution#findOtherMarkers(org.eclipse.core.resources.IMarker[])
         */
        public IMarker[] findOtherMarkers( IMarker[] markers )
        {
            List<IMarker> toRet = new ArrayList<IMarker>();
            for ( IMarker m : markers )
            {
                try
                {
                    if ( ILicenseMarkers.REQUIRED_ID.equals( m.getType() ) && m != marker )
                    {
                        // TODO is this the only condition for lifecycle markers
                        toRet.add( m );
                    }
                }
                catch ( CoreException ex )
                {
                    log.error( ex.getMessage(), ex );
                }
            }
            return toRet.toArray( new IMarker[0] );
        }
    }
    
    private class LaunchManagementWindow extends LicenseResolution
    {
        public LaunchManagementWindow( IMarker marker )
        {
            super( marker );
        }
        
        public void run( IMarker marker )
        {
            final IWorkbenchWindow workbench = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
            final Shell shell = workbench.getShell();

            ManageLicenseDialog dialog = new ManageLicenseDialog( shell );
            dialog.setBlockOnOpen( true );
            dialog.create();
            if( dialog.open() == 0 ) /* 0 == user is licensed. 1 == user canceled management */
            {
                // Update workbench projects
                final Set<IProject> projects = getProjects( marker );
                new UpdateConfigurationJob( projects.toArray( new IProject[projects.size()] ) ).schedule();
            }
            
        }
        
        public String getLabel()
        {
            return "Launch FlexMojos m2e license management window.";
        }

        public String getDescription()
        {
            return "Launches the FlexMojos m2e license management window where you can enter your license information or register for a trail license.";
        }

        public Image getImage()
        {
            // TODO Auto-generated method stub
            return null;
        }
        
    }

    private class RefreshResolution
        extends LicenseResolution
    {

        /**
         * @param marker
         */
        public RefreshResolution( IMarker marker )
        {
            super( marker );
        }

        /*
         * (non-Javadoc)
         * @see org.eclipse.ui.IMarkerResolution2#getDescription()
         */
        public String getDescription()
        {
            return Messages.MarkerResolutionGenerator_desc;
        }

        /*
         * (non-Javadoc)
         * @see org.eclipse.ui.IMarkerResolution2#getImage()
         */
        public Image getImage()
        {
            // TODO Auto-generated method getImage
            return null;
        }

        /*
         * (non-Javadoc)
         * @see org.eclipse.ui.IMarkerResolution#getLabel()
         */
        public String getLabel()
        {
            return Messages.MarkerResolutionGenerator_label;
        }

        /*
         * (non-Javadoc)
         * @see org.eclipse.ui.IMarkerResolution#run(org.eclipse.core.resources.IMarker)
         */
        public void run( IMarker marker )
        {
            final Set<IProject> projects = getProjects( marker );
            new UpdateConfigurationJob( projects.toArray( new IProject[projects.size()] ) ).schedule();
        }

        /*
         * (non-Javadoc)
         * @see org.eclipse.ui.views.markers.WorkbenchMarkerResolution#run(org.eclipse.core.resources.IMarker[],
         * org.eclipse.core.runtime.IProgressMonitor)
         */
        public void run( IMarker[] markers, IProgressMonitor monitor )
        {
            final Set<IProject> projects = getProjects( markers );
            new UpdateConfigurationJob( projects.toArray( new IProject[projects.size()] ) ).schedule();
        }
    }
}
