package net.flexmojos.ecilpse.m2e.licensing.internal;

import net.flexmojos.eclipse.m2e.licensing.ILicensePreferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.preferences.ScopedPreferenceStore;


public class LicensePreferences extends ScopedPreferenceStore implements ILicensePreferences
{    
    public LicensePreferences()
    {
        super( new InstanceScope(), "net.flexmojos.ecilpse.m2e.licensing" );
    }
    

    public String getEmail()
    {
        return getString( EMAIL );
    }
    public void setEmail( String email )
    {
        this.putValue( EMAIL, email );
    }

    public String getPassword()
    {
        return getString( PASSWORD );
    }
    public void setPassword( String password )
    {
        this.putValue( PASSWORD, password );
    }

    public String getCompany()
    {
        return getString( COMPANY );
    }
    public void setCompany( String company )
    {
        this.putValue( COMPANY, company );
    }
}
