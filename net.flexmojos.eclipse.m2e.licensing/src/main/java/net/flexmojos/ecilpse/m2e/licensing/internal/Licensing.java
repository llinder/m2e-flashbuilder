package net.flexmojos.ecilpse.m2e.licensing.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import net.flexmojos.eclipse.m2e.licensing.ILicensePreferences;
import net.flexmojos.eclipse.m2e.licensing.ILicensing;
import net.flexmojos.eclipse.m2e.licensing.LicenseException;
import net.flexmojos.eclipse.m2e.licensing.LicensingPlugin;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifiedlogic.nitro.client.license.LicenseClientObject;
import com.simplifiedlogic.nitro.client.license.NLMErrors;
import com.simplifiedlogic.nitro.loaders.LicenseClientFactory;

public class Licensing
    implements ILicensing
{
    private static final Logger log = LoggerFactory.getLogger( Licensing.class );

    private LicenseClientObject clientDelegate;

    private ILicensePreferences preferences;

    private boolean enabled;

    private String productId;

    private String version;

    private String market;

    private Boolean hasLicense;

    private Boolean hasDemoLicense;

    public Licensing()
    {

    }

    public void initialize( boolean enabled, String productId, String version, String market, String pluginId )
    {
        if ( log.isDebugEnabled() )
            log.debug( "Starting licensing." );

        this.enabled = enabled;
        this.productId = productId;
        this.version = version;
        this.market = market;

        preferences = new LicensePreferences();

        // Set up and check licensing if licensing is enabled.
        if ( isEnabled() )
        {
            clientDelegate = LicenseClientFactory.getClient();

            try
            {
                // Set up licensing
                setProductInfo( productId, version, pluginId );
            }
            catch ( LicenseException e )
            {
                log.error( "Unable to initialize licensing product information.", e );
            }

            // Update license state (get license or renew existing license).
            getOrRenewLicense();
        }

    }

    public boolean validate()
    {
        if ( isEnabled() )
        {
            if( hasLicense() )
            {
                return true;
            }
            else
            {
                return getOrRenewLicense();
            }
        }

        return true;
    }

    public ILicensePreferences getPreferences()
    {
        return preferences;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void register( String email, String password, String firstName, String lastName, String company,
                          String phone, String address1, String address2, String city, String state, String zip,
                          String country, String companyCode )
        throws LicenseException
    {
        invalidateCachedValues();

        // 1. Validate license information
        if ( StringUtils.isNotBlank( email ) && StringUtils.isNotBlank( password )
            && StringUtils.isNotBlank( firstName ) && StringUtils.isNotBlank( lastName )
            && StringUtils.isNotBlank( company ) && StringUtils.isNotBlank( phone )
            && StringUtils.isNotBlank( address1 ) && StringUtils.isNotBlank( city ) && StringUtils.isNotBlank( state )
            && StringUtils.isNotBlank( zip ) && StringUtils.isNotBlank( country ) )
        {
            // 2. Check for nitro server connectivity
            int ret = this.clientDelegate.testConnect( false );

            if ( log.isDebugEnabled() )
                log.debug( "Registration server connectivity test result: " + NLMErrors.responseToString( ret ) );

            if ( ret == NLMErrors.CONNECT_OK )
            {
                // 3. Create user
                @SuppressWarnings( "rawtypes" )
                Map extraData = new HashMap();
                ret =
                    clientDelegate.createUser( email, password, firstName, lastName, phone, address1, address2, city,
                                               state, zip, country, company, market, companyCode, version, "eclipse",
                                               NLMErrors.CONFIRM_MAIL_LINKONLY, extraData );

                if ( log.isDebugEnabled() )
                    log.debug( "Recieved registration result from the server: " + NLMErrors.responseToString( ret ) );

                if ( ret != NLMErrors.RESPONSE_CONFIRMATION_REQUIRED )
                {
                    throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                            "Unable to create user account, "
                                                                + NLMErrors.responseToString( ret ) ) );
                }
            }
        }
        else
        {
            throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                    "Invalid registration details." ) );
        }
    }

    public void getLicense( String email, String password, String company )
        throws LicenseException
    {
        if( isEnabled() )
        {
        
            invalidateCachedValues();
    
            // 1. Validate license information
            if ( StringUtils.isNotBlank( email ) && StringUtils.isNotBlank( password ) )
            {
                // Make company a blank string if not specified. This will trigger a trial license.
                if ( company == null )
                    company = "";
    
                if ( isConnected() )
                {
                    // 2. Get a license
                    int ret = clientDelegate.getLicense( email, password, company, productId, version, 30, market, false );
    
                    if ( log.isDebugEnabled() )
                        log.debug( "Recieved license result from the server: " + NLMErrors.responseToString( ret ) );
    
                    if ( ret != NLMErrors.RESPONSE_OK )
                    {
                        throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                                "Unable to get license, "
                                                                    + NLMErrors.responseToString( ret ) ) );
                    }
    
                }
                else
                {
                    throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                            "An internet connection is required to get a license to use this product." ) );
                }
            }
            else
            {
                throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                        "Invalid license details." ) );
            }
        }

    }

    public boolean releaseLicense()
        throws LicenseException
    {
        Assert.isNotNull( clientDelegate, "Client delegate must be initailized in order to release a license." );

        if( isEnabled() && isConnected() )
        {
        
            invalidateCachedValues();
    
            int ret = clientDelegate.releaseLicense( productId, version );
    
            if ( log.isDebugEnabled() )
                log.debug( "Release license: " + NLMErrors.responseToString( ret ) );
    
            if ( ret != NLMErrors.RESPONSE_RELEASED )
                throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                        "Unable to release license. " + NLMErrors.responseToString( ret ) ) );
    
            return ( ret == NLMErrors.RESPONSE_RELEASED );
        }
        else
        {
            throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                            "An internet connection is required to release a license." ) );
        }
    }

    /**
     * Delegates to NitroLM to check users license on their machine. Server is not contacted except for the
     * case of a floating license.
     * 
     * @return boolean
     */
    public boolean hasLicense()
    {
        Assert.isNotNull( clientDelegate, "Client delegate must be initailized in order to validate license." );

        if( isEnabled() )
        {
        
            if ( hasLicense != null )
                return hasLicense;
    
            int ret = this.clientDelegate.validate( version, productId, null );
    
            if ( log.isDebugEnabled() )
                log.debug( "Recieved license validation response: " + NLMErrors.responseToString( ret ) );
    
            hasLicense = ( ret == NLMErrors.RESPONSE_OK );
    
            return hasLicense;
        }
        
        return true;
    }

    /**
     * Delegates to NitroLM to check users license details on their machine. 
     * Server is not contacted except for the case of a floating license.
     * 
     * @return boolean
     */
    public boolean hasDemoLicense()
    {
        if( isEnabled() )
        {
        
            if ( hasDemoLicense != null )
                return hasDemoLicense;
    
            if ( hasLicense() )
            {
                Map<String, Object> outputValues = new HashMap<String, Object>();
                int ret = clientDelegate.getLicenseDisplayInfo( version, productId, outputValues );
    
                if ( ret == NLMErrors.RESPONSE_OK )
                    hasDemoLicense =
                        ( outputValues.containsKey( LicenseClientObject.LV_LOCKTYPE ) && NLMErrors.LOCK_DEMO.equals( outputValues.get( LicenseClientObject.LV_LOCKTYPE ) ) );
            }
            else
            {
                hasDemoLicense = false;
            }
    
            return hasDemoLicense;
        }
        
        return false;
    }

    /**
     * Utility function to rewnew a license.
     * 
     * Returns true if renewal successful other wise false.
     * @return
     */
    private boolean renewLicense()
    {
        if( isEnabled() && hasLicense() )
        {
        
            int ret = clientDelegate.renewLicense( productId, version, 2 );
    
            if ( log.isDebugEnabled() )
                log.debug( "Renewed license with result: " + NLMErrors.responseToString( ret ) );
            
            return (ret == NLMErrors.RESPONSE_OK );
        }
        
        return false;
    }

    /**
     * Configure licensing product information.
     * 
     * @param productId
     * @param version
     * @param pluginId
     * @throws LicenseException
     */
    private void setProductInfo( String productId, String version, String pluginId )
        throws LicenseException
    {
        if( isEnabled() )
        {
        
            if ( log.isDebugEnabled() )
                log.debug( "Setting product info, productId=" + productId + ", version=" + version + ", pluginId="
                    + pluginId );
    
            try
            {
                final InputStream productKey = getClass().getResourceAsStream( "/" + productId + ".ser" );
                final byte[] pubKey = IOUtils.toByteArray( productKey );
                clientDelegate.putPublicKey( productId, pubKey );
            }
            catch ( IOException e )
            {
                throw new LicenseException( new Status( IStatus.ERROR, LicensingPlugin.PLUGIN_ID,
                                                        "Unable to read product key." ) );
            }
        }
    }

    /**
     * Utility function to either get a new license or renew an existing license.
     */
    private boolean getOrRenewLicense()
    {
        if( isEnabled() && isConnected() )
        {
            if ( !hasLicense() )
            {
                if( StringUtils.isNotEmpty( preferences.getEmail() ) && StringUtils.isNotEmpty( preferences.getPassword() ) && StringUtils.isNotEmpty( preferences.getCompany() ) )
                {
                    try
                    {
                        getLicense( preferences.getEmail(), preferences.getPassword(), preferences.getCompany() );
                        
                        return true;
                    }
                    catch ( LicenseException e1 )
                    {
                        log.error( "Unable to retrieve client license.", e1 );
                    }
                    
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return renewLicense();
            }
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Check licensing server connectivity.
     * 
     * @return
     */
    private boolean isConnected()
    {
        int ret = this.clientDelegate.testConnect( false );

        if ( log.isDebugEnabled() )
            log.debug( "License server connectivity test with result, " + NLMErrors.responseToString( ret ) );

        return ( ret == NLMErrors.CONNECT_OK );
    }

    /**
     * Clears out cached values so next access will cause a full refresh.
     */
    private void invalidateCachedValues()
    {
        hasLicense = null;
        hasDemoLicense = null;
    }

}
