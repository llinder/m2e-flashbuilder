package net.flexmojos.eclipse.m2e.licensing;

import java.io.IOException;

import org.eclipse.jface.preference.IPreferenceStore;


public interface ILicensePreferences extends IPreferenceStore
{
    static final String EMAIL = "nitro_email";
    static final String PASSWORD = "nitro_password";
    static final String COMPANY = "nitro_company";
    
    public String getEmail();
    public void setEmail( String email );
    
    public String getPassword();
    public void setPassword( String password );
    
    public String getCompany();
    public void setCompany( String company );
    
    public void save() throws IOException;
}
