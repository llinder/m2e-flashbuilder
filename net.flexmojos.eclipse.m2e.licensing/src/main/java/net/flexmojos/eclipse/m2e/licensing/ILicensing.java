package net.flexmojos.eclipse.m2e.licensing;



public interface ILicensing
{
    public void initialize( boolean enabled, String productId, String version, String market, String pluginId );
    public boolean hasLicense();
    public boolean releaseLicense() throws LicenseException;
    public void getLicense( String email, String password, String company ) throws LicenseException;
    public void register( String email, String password, String firstName, String lastName, String company, String phone, String address1, String address2, String city, String state, String zip, String country, String companyCode ) throws LicenseException;
    public boolean isEnabled();
    public boolean hasDemoLicense();
    public boolean validate();
}
