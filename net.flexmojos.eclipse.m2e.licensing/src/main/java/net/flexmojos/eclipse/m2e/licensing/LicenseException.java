package net.flexmojos.eclipse.m2e.licensing;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

public class LicenseException
    extends CoreException
{
    private static final long serialVersionUID = 1L;

    public LicenseException( IStatus status )
    {
        super( status );
    }
}
