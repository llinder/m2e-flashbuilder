package net.flexmojos.eclipse.m2e.licensing;

import java.io.IOException;
import java.util.Properties;

import net.flexmojos.ecilpse.m2e.licensing.internal.LicensePreferences;
import net.flexmojos.ecilpse.m2e.licensing.internal.Licensing;
import net.flexmojos.eclipse.m2e.IFlexMojosPlugin;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.BundleContext;

public class LicensingPlugin
    extends Plugin implements IFlexMojosPlugin
{    
    public static final String PLUGIN_ID = "net.flexmojos.eclipse.m2e.licensing";

    public static final boolean LICENSING_ENABLED;

    public static final String PRODUCT_ID;

    public static final String PRODUCT_VERSION;

    public static final String MARKET_SOURCE;
    
    private static LicensingPlugin plugin;
    
    public static LicensingPlugin getDefault()
    {
        return plugin;
    }

    // Read and configure product licensing
    static
    {
        final Properties licenseProperties = new Properties();
        try
        {
            licenseProperties.load( LicensingPlugin.class.getResourceAsStream( "/license.properties" ) );
            String enabled = licenseProperties.getProperty( "enabled" );
            String productId = licenseProperties.getProperty( "product_id" );
            String version = licenseProperties.getProperty( "product_version" );
            String market = licenseProperties.getProperty( "market_source" );
            LICENSING_ENABLED = "true".equals( enabled );
            PRODUCT_ID = productId;
            PRODUCT_VERSION = version;
            MARKET_SOURCE = market;
        }
        catch ( IOException e )
        {
            throw new RuntimeException( "Unable to load flexmojos.properties", e );
        }
    }
    
    private ILicensing licensing;
    
    public ILicensing getLicensing()
    {
        return licensing;
    }
    
    private ILicensePreferences preferences;
    
    public ILicensePreferences getLicensePreferences()
    {
        return preferences;
    }

    public void start( BundleContext context )
        throws Exception
    {
        super.start( context );
        plugin = this;

        // Initailize licensing
        licensing = new Licensing();
        licensing.initialize( LICENSING_ENABLED, PRODUCT_ID, PRODUCT_VERSION, MARKET_SOURCE, PLUGIN_ID );
        
        preferences = new LicensePreferences();
    }

    public void stop( BundleContext context )
        throws Exception
    {
        super.stop( context );
        plugin = null;
    }

    public String getPluginId()
    {
        return PLUGIN_ID;
    }
}
