package net.flexmojos.eclipse.m2e.licensing.markers;

public interface ILicenseMarkers
{
    public static final String REQUIRED_ID = "net.flexmojos.eclipse.m2e.licensing.markers.licenseRequired";
}
