package net.flexmojos.eclipse.m2e.ui.exportWizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

public class ExportPomWizard
    extends Wizard
    implements IExportWizard
{
    ExportPomWizardPage mainPage;

    public void init( IWorkbench workbench, IStructuredSelection selection )
    {
        setWindowTitle( "FlexMojos Export Wizard" );
        setNeedsProgressMonitor( true );
        mainPage = new ExportPomWizardPage( "Export FlexMojos POM", selection );
    }

    public boolean performFinish()
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void addPages()
    {
        super.addPages();
        addPage( mainPage );
    }
}
