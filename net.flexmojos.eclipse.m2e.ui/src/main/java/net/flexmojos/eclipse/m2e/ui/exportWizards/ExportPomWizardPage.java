package net.flexmojos.eclipse.m2e.ui.exportWizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ExportPomWizardPage
    extends WizardPage
{
    private IStructuredSelection currentSelection;
    
    private Text groupId;
    private Text text;
    private Text text_1;
    private Text text_2;

    protected ExportPomWizardPage( String pageName, IStructuredSelection selection )
    {
        super( pageName );
        currentSelection = selection;
        setPageComplete( false );
    }

    public void createControl( Composite parent )
    {
        Composite container = new Composite(parent, SWT.NULL);
        setControl(container);
        container.setLayout(new FormLayout());
        
        groupId = new Text(container, SWT.BORDER);
        FormData fd_groupId = new FormData();
        fd_groupId.top = new FormAttachment(0, 10);
        fd_groupId.right = new FormAttachment(100, -10);
        groupId.setLayoutData(fd_groupId);
        
        Label groupIdLabel = new Label(container, SWT.NONE);
        FormData fd_groupIdLabel = new FormData();
        fd_groupIdLabel.top = new FormAttachment(groupId, 3, SWT.TOP);
        fd_groupIdLabel.right = new FormAttachment(groupId, -6);
        groupIdLabel.setLayoutData(fd_groupIdLabel);
        groupIdLabel.setText("Group ID:");
        
        text = new Text(container, SWT.BORDER);
        fd_groupId.left = new FormAttachment(text, 0, SWT.LEFT);
        FormData fd_text = new FormData();
        fd_text.right = new FormAttachment(100, -10);
        fd_text.top = new FormAttachment(groupId, 6);
        text.setLayoutData(fd_text);
        
        text_1 = new Text(container, SWT.BORDER);
        FormData fd_text_1 = new FormData();
        fd_text_1.right = new FormAttachment(100, -10);
        fd_text_1.top = new FormAttachment(text, 6);
        text_1.setLayoutData(fd_text_1);
        
        Label artifactIdLabel = new Label(container, SWT.NONE);
        fd_text.left = new FormAttachment(0, 122);
        FormData fd_artifactIdLabel = new FormData();
        fd_artifactIdLabel.top = new FormAttachment(text, 3, SWT.TOP);
        fd_artifactIdLabel.right = new FormAttachment(text, -6);
        artifactIdLabel.setLayoutData(fd_artifactIdLabel);
        artifactIdLabel.setText("Artifact ID:");
        
        Label versionLabel = new Label(container, SWT.NONE);
        fd_text_1.left = new FormAttachment(0, 122);
        FormData fd_versionLabel = new FormData();
        fd_versionLabel.top = new FormAttachment(text_1, 3, SWT.TOP);
        fd_versionLabel.right = new FormAttachment(text_1, -6);
        versionLabel.setLayoutData(fd_versionLabel);
        versionLabel.setText("Version:");
        
        Button btnFlexmojosEnterprise = new Button(container, SWT.CHECK);
        FormData fd_btnFlexmojosEnterprise = new FormData();
        fd_btnFlexmojosEnterprise.left = new FormAttachment(groupId, 0, SWT.LEFT);
        btnFlexmojosEnterprise.setLayoutData(fd_btnFlexmojosEnterprise);
        btnFlexmojosEnterprise.setText("FlexMojos Enterprise?");
        
        text_2 = new Text(container, SWT.BORDER);
        fd_btnFlexmojosEnterprise.bottom = new FormAttachment(text_2, -6);
        FormData fd_text_2 = new FormData();
        fd_text_2.top = new FormAttachment(0, 109);
        fd_text_2.right = new FormAttachment(100, -10);
        text_2.setLayoutData(fd_text_2);
        
        Label lblFlexmojosVersion = new Label(container, SWT.NONE);
        fd_text_2.left = new FormAttachment(lblFlexmojosVersion, 6);
        FormData fd_lblFlexmojosVersion = new FormData();
        fd_lblFlexmojosVersion.top = new FormAttachment(text_2, 3, SWT.TOP);
        fd_lblFlexmojosVersion.right = new FormAttachment(groupIdLabel, 0, SWT.RIGHT);
        lblFlexmojosVersion.setLayoutData(fd_lblFlexmojosVersion);
        lblFlexmojosVersion.setText("FlexMojos Version:");
    }
}
